# C Development Tools (Assembler, Compiler, Linker) for a Turing Machine

This project's main goal is to create tools similar to a typical C development environment, which would build an executable representing a state machine for a single-track Turing Machine architecture. The purpose is to prove that Turing Machines truly are just as capable as a modern computer, through a real **working** and **fast** example. *(The 'BinaryCounter' example program, including dependencies, builds into a state machine in about 4 seconds).* This project features my very first compiler, and for that matter, context-free parser. A few additional goals existed, mostly for the sake of education/understanding:
 - Learn first-hand the complexity and intricacies of compilers and parsers,
 - To develop a deeper understanding of the needs and requirements between the three programs listed above, and the file specifications of object files and assembly files, and why those specific needs exist,
 - To understand the low-level abstractions made by a typical C compiler, as it allocates variables on the stack (including recycling space as variables fall out of scope), as it processes variable references, "rvalues", "lvalues", function declarations, function calls, header files, control structures, or even how it manages variables of complex types, such as `structs`, arrays, `typedefs`, or even more complicated variations, like a `struct` with fields of other `structs`, with fields of arrays of `typedef`'d `structs`, with arrays in them!

A latex IEEE-Formatted paper can be found in `paper/`, which goes into much more detail on the topics covered here.

## Language Design

The language is quite simple in order to easier to implement, and just complicated enough to make the point this project is trying to make. It supports functions (with recursion), `structs`, `typedefs`, arrays of constant length, `if`/`else`, labels, `gotos`, constants, literals, etc. Arrays can be indexed with a dynamic index, effectively implementing pointers. Although not necessary for this project, there's also an I/O or external interface library for sending and receiving bytes from the user.

### Pointers Not Supported

The purpose of the language was to simply demonstrate a C-like language, nothing close to satisfying any standards or anything. For instance, this language does not offer support for directly manipulating pointers, in fact, pointers do not explicitly exist in the language at this time. However, arrays can be indexed by a variable index, meaning that one could use array indices *as* pointers, instead. The importance difference here is the expectations of the compiler and architecture: With pointers, the program is effectively expected to perform fetches to and from any location in memory, randomly, or at least arbitrarily. The only kind of memory for a proper Turing Machine is ticker tape, where accesses can only be made one to the left or right of the previous memory fetch. The only solution I can see for any chance of future pointer implementation, is where somehow each stack frame can determine it's absolute address on the tape and then move relative from there to the location referred to by the pointer. Clearly, any implementation of pointers would be significantly more complicated than array indexing, where the entire array is copied by value between function calls, guaranteeing that any dynamic memory fetch is within the stack frame doing the fetch.

### External Interface Library

In the latex paper which goes into even further detail on this project, this library is called the "Basic Utilities." This library does not extend the ability of the platform, nor does it offload any computation, it is merely used to read or write letters to the terminal the emulator is running in, or to signal to the emulator that the program/state machine has reached an 'accepting' state or 'rejecting' state. See paper or source code for more details.

### Examples

9 Example programs can be found in `examples/programs`, if left wanting more, many test programs can be found in `examples/tests`

#### "HelloWorld!"

Of course we start with this example, where the program calls the `write()` system call repeatedly for each letter of the string the program eventually prints. Simple. Programs/state machines whose `main()` return nonzero, enters the 'reject' state, while programs whose `main()` return zero, enter the 'accept' state.

```c
#include "deps/defines/c.h" // typedefs 'char' type
#include "deps/syscalls/IO.h" // needed for 'write()' syscall

byte main() {
	write('H');
	write('e');
	write('l');
	write('l');
	write('o');
	write(',');
	write(' ');
	write('W');
	write('o');
	write('r');
	write('l');
	write('d');
	write('!');
	write('\n');
	return 0;
}
```

### Decimal to Binary Converter

This example is much more complicated example.
The alphabet size of the low-level state machine is 256. `bytes` are therefore the primitive type of the language, `shorts` are a `struct` of two `bytes`, and `ints` are a `struct` of two `shorts`. The `bitwise_byte`, `bitwise_short`, and `bitwise_int` types have been created to represent value of a `byte`, `short`, or `int` in it's binary form, as an array of booleans. These `bitwise_*` `structs` simplify the implementations of arithmetic operations by letting the implementation of for instance, addition, use typical boolean operations on the binary numbers, instead of somehow operating on each whole `byte` at a time.
For loops and while loops are macros, which the preprocessor replaces with an `if` and a `goto`

```c
#include "deps/defines/c.h" // defines 'char' type
#include "deps/defines/bitwise.h" // defines bitwise structs
#include "deps/add/bitwise_byte.c"
#include "deps/subtract/bitwise_byte.c"
#include "deps/multiply/bitwise_byte.c"
#include "deps/subtract/byte.c"
#include "deps/conversion/from_byte.h"
#include "deps/misc/print_byte.c"
#include "deps/syscalls/IO.h" // declares 'write()' syscall
#include "deps/defines/loops.h" // declares the `while` macro

// Reads in a base 10 number and writes out the number converted to base 2
// given number must be less than 256

byte main() {
	struct bitwise_byte current;
	assign_bitwise_byte(current, 0, 0, 0, 0, 0, 0, 0, 0);
	struct bitwise_byte ten;
	// although the variable 'ten' is just a constant,
	// it saves work to not have to initialise another for each use
	assign_bitwise_byte(ten, 0, 0, 0, 0, 1, 0, 1, 0);
	struct bitwise_byte char_zero;
	char_zero = conversion_from_byte('0'); // converts byte to bitwise_byte equivalent
	char buffer;
	while(byte_subtract(buffer = read(), '\n'), // the 'byte_subtract' function is used as a '!=' operator
	{
		current = bitwise_byte_multiply(current, ten);
		// C equivalent for the above line would be: current *= 10;
		current = bitwise_byte_add(current, bitwise_byte_subtract(conversion_from_byte(buffer), char_zero), 0);
		// C equivalent for the above line would be: current += buffer - '0';
	})
	print_byte(current); // prints either a '1' or '0', for each boolean in the given 'bitwise_byte'
	write('\n');
	return 0;
}
```

### Compiling Development Tools and Example Programs

Packages you will need to build:
 - bison >= 3.0.4
 - flex >= 2.6.4
 - gcc >= 7.3.1
 - make >= 4.2.1

Do not take the version numbers seriously, those are just the versions of the packages that I used for development.
To build the development tools, just run the `build_dev_tools.sh` script.
If it is successful, the assembler, compiler, linker, and emulator will all have been created in your `/tmp/` directory! You can now go into `examples/programs` and build example programs with their respective scripts!














