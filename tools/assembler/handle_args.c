/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/macros.h",
		"~/deps/memory/clone.c",
		"~/deps/array/struct.h",
		"~/deps/array/new.c",
		"~/deps/array/push_n.c",
		"~/deps/array/delete.c",
		"~/deps/exec.c",
		"~/deps/fileio/does_path_exist.c",
		"~/deps/misc/assert.h",

		"handle_args.c",
	}
/*/
#ifndef handle_args_c
#define handle_args_c
struct parsed_args {
	char output_file[max_pathname];
};
int handle_args(struct parsed_args* output, int n_args, char** args);
#else
#include <string.h>
#include <unistd.h>
int handle_args(struct parsed_args* output, int n_args, char** args) {
	ENTER;
	bool output_given = false;
	char c;
	struct array cpp_command = array_new(char*);
	array_push_n(&cpp_command, varptr((char*) "cpp"));
	array_push_n(&cpp_command, varptr((char*) "-w"));
	array_push_n(&cpp_command, varptr((char*) "-undef"));
	while((c = getopt(n_args, args, "vo:D:")) != -1) {
		switch(c) {
			case 'v': {
				set_verbose(true);
				break;
			}
			case 'o': {
				output_given = true;
				strcpy(output->output_file, optarg);
				break;
			}
			case 'D': {
				verprintf("this is a cpp flag,\n");
				array_push_n(&cpp_command, varptr((char*) "-D"));
				array_push_n(&cpp_command,
							 varptr(memclone(optarg, strlen(optarg) + 1)));
				break;
			}
			default: { assert(0, "unsupported flag!"); }
		}
	}
	HERE;
	assert(output_given, "Output file not given!");
	if(optind == n_args) {
		EXIT;
		return STD_IN;
	} else {
		size_t strlen_file;
		char file[max_pathname];
		HERE;
		verpv(optind);
		verpv(n_args);
		strlen_file = strlen(args[optind]);
		verpv(strlen_file);
		memcpy(file, args[optind], strlen_file + 1);
		verpvs(file);
		assert(does_path_exist(file, strlen_file),
			   "Assembly file does not exist! (\"%s\")", file);
		HERE;
		int pipe_fds[2];
		pipe(pipe_fds);
		array_push_n(&cpp_command, varptr((char*) file));
		array_push_n(&cpp_command, varptr((char*) NULL));
		exec(cpp_command.datav, false, ({
				 void callback() {
					 dup2(pipe_fds[PIPE_WRITE], STD_OUT);
					 close(pipe_fds[PIPE_READ]);
				 }
				 callback;
			 }));
		HERE;
		verpv(pipe_fds[PIPE_READ]);
		close(pipe_fds[PIPE_WRITE]);
		array_delete(&cpp_command);
		EXIT;
		return pipe_fds[PIPE_READ];
	}
}
#endif
