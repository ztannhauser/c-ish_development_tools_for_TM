/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/bsearch/struct.h",
		"~/deps/array/bsearch/singular.c",
		"~/deps/misc/assert.h",
		"../structs/name.h",
		"../structs/func_write.h",
		"structs/obj.h",
		"structs/func_write.h",
		"compares/state_and_name.c",
		"convert_func_writes.c",
	}
/*/
#ifndef convert_func_writes_c
#define convert_func_writes_c
void convert_func_writes(struct array* obj_func_writes, struct array* states,
						 struct func_write* linker_writes);
#else
void convert_func_writes(struct array* obj_func_writes, struct array* states,
						 struct func_write* linker_writes) {
	ENTER;
	size_t i, n = obj_func_writes->n;
	for(i = 0; i < n; i++) {
		verpv(i);
		struct obj_func_write* rlr = arrayp_index(*obj_func_writes, i);
		struct func_write* ele = &(linker_writes[i]);
		struct array_search_returned abr = array_bsearch_singular(
			states, &(rlr->instruct), compare_state_and_name);
		assert(abr.exists, "issue!");
		ele->instruct_index = abr.index;
		ele->begin = rlr->begin;
		ele->end = rlr->end;
		ele->func = rlr->func;
		HERE;
	}
	EXIT;
}
#endif
