/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"~/deps/memory/copy.c",
		"~/deps/array/struct.h",
		"~/deps/array/bsearch/struct.h",
		"~/deps/array/bsearch/singular.c",
		"~/deps/array/push.c",
		"~/deps/string/struct.h",
		"~/deps/misc/assert.h",
		"../structs/name.h",
		"structs/obj.h",
		"../compares/names.c",
		"compares/state_and_name.c",
		"get_new_state_by_name.c",
	}
/*/
#ifndef get_new_state_by_name_c
#define get_new_state_by_name_c
struct obj_turing_state* get_new_state_by_name(struct array* external_states,
											   struct array* states,
											   struct name* tempname,
											   bool external);
#else
struct obj_turing_state* get_new_state_by_name(struct array* external_states,
											   struct array* states,
											   struct name* tempname,
											   bool external) {
	ENTER;
	struct array_search_returned abr =
		array_bsearch_singular(states, tempname, compare_state_and_name);
	verpv(abr.exists);
	verpv(abr.index);
	assert(!(abr.exists), "redefinition of same state! (\"%s\")",
		   tempname->data);
	struct obj_turing_state* state = malloc(sizeof(struct obj_turing_state));
	HERE;
	array_push(states, abr.index, &state);
	state->name = *tempname;
	state->external = external;
	HERE;
	if(external) {
		HERE;
		struct array_search_returned abr =
			array_bsearch_singular(external_states, tempname, compare_names);
		HERE;
		verpv(abr.exists);
		verpv(abr.index);
		HERE;
		array_push(external_states, abr.index, tempname);
		HERE;
	}
	EXIT;
	return state;
}
#endif
