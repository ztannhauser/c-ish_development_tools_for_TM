/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/string/char_popper/struct.h",
		"~/deps/string/char_popper/assert_syntax.c",
		"~/deps/string/find/blackspace_skip_shell_comments.c",
		"~/deps/misc/assert.h",
		"../structs/name.h",
		"structs/obj.h",
		"read_name.c",
		"init_all_branches.c",
		"get_new_state_by_name.c",
		"parse_branch_modifcation.c",
		"construct_and_add_state.c",
	}
/*/
#ifndef construct_and_add_state_c
#define construct_and_add_state_c
void construct_and_add_state(struct array* func_writes,
							 struct array* external_states,
							 struct array* states, struct char_popper* cp);
#else

#include <string.h>

void construct_and_add_state(struct array* func_writes,
							 struct array* external_states,
							 struct array* states, struct char_popper* cp) {
	ENTER;
	struct obj_turing_state* state;
	{
		bool external = false;
	top : {}
		char c = current_char(cp);
		verpvc(c);
		verpv(external);
		verpvc(current_char(cp));
		struct name tempname;
		read_name(cp, &tempname);
		verpvs(tempname.data);
		verpv(tempname.strlen);
		if(strcmp(tempname.data, "extern") == 0) {
			external = true;
			cpassert(find_blackspace_skip_shell_comments(cp));
			goto top;
		}
		state =
			get_new_state_by_name(external_states, states, &tempname, external);
		cpassert(find_blackspace_skip_shell_comments(cp));
		assert(current_char(cp) == ':',
			   "There must be a colon after state title!");
		cpassert(move_forward(cp));
	}
	init_all_branches(state, '?');
	cpassert(find_blackspace_skip_shell_comments(cp));
	while(current_char(cp) != ';') {
		parse_branch_modifcation(func_writes, states, state, cp);
		cpassert(find_blackspace_skip_shell_comments(cp));
	}
	cpassert(move_forward(cp));
	EXIT;
}
#endif
