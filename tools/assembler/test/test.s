/*/
/*/

extern start: * => *, R, test_0;
test_0: * => '1', R, test_1;
test_1: * => '2', R, test_2;
test_2: * => '3', R, test_3;
test_3: * => test_4, R, foo;
extern test_4: * => '4', N, halt;
