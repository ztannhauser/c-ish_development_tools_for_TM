/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"../../structs/name.h",
		"../../structs/undef_sym.h",
		"../../compares/names.c",
		"undef_ref_and_name.c",
	}
/*/
#ifndef compares_undef_ref_and_name_c
#define compares_undef_ref_and_name_c
int compares_undef_ref_and_name(struct undef_sym* a, struct name* b);
#else
int compares_undef_ref_and_name(struct undef_sym* a, struct name* b) {
	return compare_names(&(a->name), b);
}
#endif
