/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"../../structs/name.h",
		"../../compares/names.c",
		"../structs/func_write.h",
		"func_writes.c",
	}
/*/
#ifndef compare_func_writes_c
#define compare_func_writes_c
int compare_func_writes(struct obj_func_write* a, struct obj_func_write* b);
#else
int compare_func_writes(struct obj_func_write* a, struct obj_func_write* b) {
	return compare_names(&(a->func), &(b->func));
}
#endif
