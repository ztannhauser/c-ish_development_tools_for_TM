/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/macros.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/string/struct.h",
		"../../structs/name.h",
		"../../compares/names.c",
		"../structs/obj.h",
		"state_and_name.c",
	}
/*/
#ifndef compare_state_and_name_c
#define compare_state_and_name_c
int compare_state_and_name(struct obj_turing_state** state, struct name* name);
#else
#include <string.h>
int compare_state_and_name(struct obj_turing_state** state, struct name* name) {
	return compare_names(&((*state)->name), name);
}
#endif
