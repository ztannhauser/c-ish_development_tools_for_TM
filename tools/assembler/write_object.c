/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"~/deps/defines.h",
		"~/deps/array/struct.h",
		"~/deps/misc/assert.h",
		"../structs/name.h",
		"../structs/state.h",
		"../structs/branch_index.h",
		"../structs/func_write.h",
		"../structs/undef_sym.h",
		"../structs/ext_state.h",
		"write_object.c",
	}
/*/
#ifndef write_object_c
#define write_object_c
void write_object(struct func_write* linker_fw, size_t n1,
				  struct ext_state* linker_ext, size_t n2,
				  struct turing_state* linker_states, size_t n3,
				  struct array* undefs, char* output_file);
#else
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
void write_object(struct func_write* linker_fw, size_t n1,
				  struct ext_state* linker_ext, size_t n2,
				  struct turing_state* linker_states, size_t n3,
				  struct array* undefs, char* output_file) {
	ENTER;
	verpvs(output_file);
	int fd =
		open(output_file, O_WRONLY | O_CREAT | O_TRUNC, READ_WRITE_FOR_ALL);
	verpv(fd);
	assert(fd >= 0, "failed to open output!");
	assert(write(fd, &n1, sizeof(size_t)) == sizeof(size_t), "!!");
	verpv(n1);
	{
		size_t product = sizeof(struct func_write) * n1;
		verpv(product);
		assert(write(fd, linker_fw, product) == product, "!!");
	}
	{
		write(fd, &n2, sizeof(size_t));
		verpv(n2);
		write(fd, linker_ext, sizeof(struct ext_state) * n2);
	}
	verpv(undefs->n);
	write(fd, &(undefs->n), sizeof(size_t));
	size_t i, n = undefs->n;
	for(i = 0; i < n; i++) {
		verpv(i);
		struct undef_sym* ref = arrayp_index(*undefs, i);
		verpv(ref);
		verpvs(ref->name.data);
		write(fd, &(ref->name), sizeof(struct name));
		write(fd, &(ref->indexes.n), sizeof(size_t));
		size_t j, m = ref->indexes.n;
		verpv(m);
		for(j = 0; j < m; j++) {
			struct branch_index* ele = arrayp_index(ref->indexes, j);
			write(fd, ele, sizeof(struct branch_index));
		}
	}
	write(fd, &n3, sizeof(n3));
	verpv(n3);
	write(fd, linker_states, sizeof(struct turing_state) * n3);
	close(fd);
	EXIT;
}
#endif
