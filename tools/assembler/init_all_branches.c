/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/memory/set.c",
		"../structs/name.h",
		"structs/obj.h",
		"init_all_branches.c",
	}
/*/
#ifndef init_all_branches_c
#define init_all_branches_c
void init_all_branches(struct obj_turing_state* state, char c);
#else
#include <string.h>
void init_all_branches(struct obj_turing_state* state, char c) {
	ENTER;
	verpvc(c);
	struct obj_turing_branch default_case = {
		.write = c, .ticker_movement = 0, .next_instruct.strlen = 4};
	memcpy(default_case.next_instruct.data, "reject", 7);
	memory_set(state->branches, &default_case, sizeof(struct obj_turing_branch),
			   256);
	EXIT;
}
#endif
