/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/new.c",
		"~/deps/array/delete.c",
		"~/deps/string/char_popper/struct.h",
		"~/deps/string/char_popper/new.c",
		"~/deps/string/char_popper/implementations/uread.c",
		"~/deps/string/char_popper/delete.c",
		"~/deps/string/find/blackspace_skip_shell_comments.c",
		"~/deps/misc/assert.c",
		"../structs/name.h",
		"../structs/state.h",
		"../structs/func_write.h",
		"../structs/ext_state.h",
		"../structs/undef_sym.h",
		"structs/obj.h",
		"structs/func_write.h",
		"handle_args.c",
		"convert_func_writes.c",
		"convert_ext_states.c",
		"resolve_symbols.c",
		"write_object.c",
		"construct_and_add_state.c",
	}
/*/
#include <unistd.h>
int main(int n_args, char** args) {
	set_verbose(false);
	ENTER;
	#ifndef NO_DEBUG
	verbose_prefix = "A";
	#endif
	verprintf("Hello, World!\n");
	struct parsed_args pa;
	int fd = handle_args(&pa, n_args, args);
	struct char_popper cp = char_popper_new(fd, char_popper_uread, close);
	struct array func_writes = array_new(struct obj_func_write);
	struct array external_states = array_new(struct name);
	struct array states = array_new(struct obj_turing_state*);
	while(find_blackspace_skip_shell_comments(&cp)) {
		#ifndef NO_DEBUG
		if(states.n % 256 == 0) {
			verprintf("# Progress: %luth instruction,\n", states.n);
		}
		#endif
		construct_and_add_state(&func_writes, &external_states, &states, &cp);
	}
	verprintf("# Loaded all %lu instructions.\n", states.n);
	char_popper_delete(&cp);
	struct func_write* linker_fw =
		malloc(func_writes.n * sizeof(struct func_write));
	convert_func_writes(&func_writes, &states, linker_fw);
	struct ext_state* linker_ext =
		malloc(external_states.n * sizeof(struct ext_state));
	convert_ext_states(&external_states, &states, linker_ext);
	struct turing_state* linker_states =
		malloc(states.n * sizeof(struct turing_state));
	struct array undefs = array_new(struct undef_sym);
	resolve_symbols(&states, &undefs, linker_states);
	verpv(external_states.n);
	write_object(linker_fw, func_writes.n, linker_ext, external_states.n,
				 linker_states, states.n, &undefs, pa.output_file);
	free(linker_fw);
	free(linker_ext);
	free(linker_states);
	array_delete(&undefs);
	array_delete(&func_writes);
	array_delete(&external_states);
	array_delete(&states);
	EXIT;
}
