/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"~/deps/string/struct.h",
		"~/deps/string/string/push.c",
		"~/deps/string/char_popper/struct.h",
		"~/deps/string/char_popper/while_do_then.c",
		"~/deps/string/char_popper/conds/while_is_keyword_letter.c",
		"~/deps/misc/assert.h",
		"../structs/name.h",
		"read_name.c",
	}
/*/
#ifndef read_name_c
#define read_name_c
void read_name(struct char_popper* cp, struct name* temp);
#else
void read_name(struct char_popper* cp, struct name* temp) {
	struct string name = {.data = temp->data, .strlen = 0};
	char_popper_while_do_then(cp, char_popper_cond_while_is_keyword_letter,
							  NULL, string_push, &name);
	name.data[0] = '\0';
	temp->strlen = name.strlen;
}
#endif
