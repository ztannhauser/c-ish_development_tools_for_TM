/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/new.c",
		"~/deps/array/bsearch/struct.h",
		"~/deps/array/bsearch/singular.c",
		"~/deps/array/push_n.c",
		"~/deps/array/push.c",
		"~/deps/misc/assert.h",
		"../structs/name.h",
		"../structs/state.h",
		"../structs/undef_sym.h",
		"../structs/branch_index.h",
		"structs/obj.h",
		"compares/undef_ref_and_name.c",
		"compares/state_and_name.c",
		"resolve_symbols.c",
	}
	compiler_flags =
	{
		"-DNO_DEBUG"
	}
/*/
#ifndef resolve_symbols_c
#define resolve_symbols_c
void resolve_symbols(struct array* states, struct array* undefs,
					 struct turing_state* linker_states);
#else
void resolve_symbols(struct array* states, struct array* undefs,
					 struct turing_state* linker_states) {
	ENTER;
	size_t i, n = states->n;
	for(i = 0; i < n; i++) {
		verpv(i);
		struct obj_turing_state* rlr =
			array_index(*states, i, struct obj_turing_state*);
		verpvs(rlr->name.data);
		struct turing_state* ele = &(linker_states[i]);
		unsigned short k;
		for(k = 0; k < 256; k++) {
			struct obj_turing_branch* brlr = &(rlr->branches[k]);
			struct turing_branch* bele = &(ele->branches[k]);
			bele->write = brlr->write;
			bele->ticker_movement = brlr->ticker_movement;
			struct array_search_returned abr = array_bsearch_singular(
				states, &(brlr->next_instruct), compare_state_and_name);
			if(abr.exists) {
				bele->next_instruct = abr.index;
			} else {
				verpv(k);
				verpvs(brlr->next_instruct.data);
				struct array_search_returned abr =
					array_bsearch_singular(undefs, &(brlr->next_instruct),
										   compares_undef_ref_and_name);
				struct branch_index new_bi = {.instruct = i, .branch = k};
				if(abr.exists) {
					verpv(abr.index);
					struct undef_sym* current =
						arrayp_index(*undefs, abr.index);
					array_push_n(&(current->indexes), &new_bi);
				} else {
					struct undef_sym new;
					new.name = brlr->next_instruct;
					verpvs(new.name.data);
					new.indexes = array_new(struct branch_index);
					array_push_n(&(new.indexes), &new_bi);
					array_push(undefs, abr.index, &new);
				}
			}
		}
	}
	EXIT;
}
#endif
