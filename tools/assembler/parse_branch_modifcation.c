/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/memory/copy.c",
		"~/deps/array/struct.h",
		"~/deps/array/bsearch/struct.h",
		"~/deps/array/bsearch/singular.c",
		"~/deps/array/push.c",
		"~/deps/string/char_popper/struct.h",
		"~/deps/string/char_popper/assert_syntax.c",
		"~/deps/string/find/blackspace_skip_shell_comments.c",
		"~/deps/string/conversion/unflatten/c_char.c",
		"~/deps/misc/assert.h",
		"../structs/name.h",
		"structs/obj.h",
		"structs/func_write.h",
		"compares/state_and_name.c",
		"compares/func_writes.c",
		"read_name.c",
		"parse_branch_modifcation.c",
	}
	compiler_flags =
	{
		//"-DNO_DEBUG"
	}
/*/
#ifndef parse_branch_modifcation_c
#define parse_branch_modifcation_c
void parse_branch_modifcation(struct array* func_writes, struct array* states,
							  struct obj_turing_state* state,
							  struct char_popper* cp);
#else
void parse_branch_modifcation(struct array* func_writes, struct array* states,
							  struct obj_turing_state* state,
							  struct char_popper* cp) {
	ENTER;
	byte begin, end;
	char c = current_char(cp);
	verpvc(c);
	switch(c) {
		case '*': {
			begin = 0;
			end = 255;
			cpassert(move_forward(cp));
			break;
		}
		case '[': {
			cpassert(move_forward(cp));
			cpassert(find_blackspace_skip_shell_comments(cp));
			begin = string_unflatten_c_char(cp, false);
			cpassert(find_blackspace_skip_shell_comments(cp));
			char_popper_assert_syntax(cp, "...", 3,
									  "Must have '...' bewteen range!");
			cpassert(find_blackspace_skip_shell_comments(cp));
			end = string_unflatten_c_char(cp, false);
			cpassert(find_blackspace_skip_shell_comments(cp));
			break;
		}
		case '\'': {
			begin = end = string_unflatten_c_char(cp, false);
			break;
		}
		default: {
			assert(0, "Only *, [, or \' Allowed here! (recv. '%c', 0x%X)", c,
				   c);
		}
	}
	verpv(begin);
	verpv(end);
	cpassert(find_blackspace_skip_shell_comments(cp));
	char_popper_assert_syntax(cp, "=>", 2, "must have a =>!");
	cpassert(find_blackspace_skip_shell_comments(cp));
	unsigned short rb, re = end;
	c = current_char(cp);
	if(c == '*') {
		cpassert(move_forward(cp));
		for(rb = begin; rb <= re; rb++) {
			state->branches[rb].write = rb;
		}
	} else if(c == '\'') {
		byte write = string_unflatten_c_char(cp, false);
		for(rb = begin; rb <= re; rb++) {
			state->branches[rb].write = write;
		}
	} else {
		struct obj_func_write new;
		new.instruct = state->name;
		verpvs(state->name.data);
		new.begin = begin;
		new.end = end;
		read_name(cp, &new.func);
		verpvs(new.func.data);
		verpv(new.func.strlen);
		{
			struct array_search_returned abr =
				array_bsearch_singular(func_writes, &new, compare_func_writes);
			assert(!abr.exists, "issue!");
			array_push(func_writes, abr.index, &new);
		}
		struct name secondary;
		{
			char* here = secondary.data;
			(here++)[0] = '\x80';
			here = memory_copy(here, new.instruct.data, new.instruct.strlen);
			(here++)[0] = '_';
			here = memory_copy(here, new.func.data, new.func.strlen);
			here[0] = '\0';
			secondary.strlen = here - ((char*) secondary.data);
		}
		verpvs(secondary.data);
		verpv(secondary.strlen);
		struct array_search_returned abr =
			array_bsearch_singular(states, &secondary, compare_state_and_name);
		verpv(abr.exists);
		verpv(abr.index);
		assert(!abr.exists, "issue!");
		struct obj_turing_state* sec = malloc(sizeof(struct obj_turing_state));
		array_push(states, abr.index, &sec);
		sec->name = secondary;
		sec->external = false;
		for(rb = begin; rb <= re; rb++) {
			state->branches[rb].ticker_movement = 1;
			state->branches[rb].next_instruct = secondary;
		}
		state = sec;
		begin = 0;
		re = end = 255;
	}
	cpassert(find_blackspace_skip_shell_comments(cp));
	char_popper_assert_syntax(cp, ",", 1, "must have a comma!");
	cpassert(find_blackspace_skip_shell_comments(cp));
	{
		c = current_char(cp);
		signed char ticker_move;
		switch(c) {
			case 'L': {
				ticker_move = -1;
				break;
			}
			case 'N': {
				ticker_move = 0;
				break;
			}
			case 'R': {
				ticker_move = +1;
				break;
			}
			default: {
				assert(0, "Read letter is an invaild ticker direction!");
			}
		}
		for(rb = begin; rb <= re; rb++) {
			state->branches[rb].ticker_movement = ticker_move;
		}
		cpassert(move_forward(cp));
	}
	cpassert(find_blackspace_skip_shell_comments(cp));
	char_popper_assert_syntax(cp, ",", 1, "must have a comma!");
	cpassert(find_blackspace_skip_shell_comments(cp));
	{
		struct name tempname;
		read_name(cp, &tempname);
		for(rb = begin; rb <= re; rb++) {
			state->branches[rb].next_instruct = tempname;
		}
	}
	EXIT;
}
#endif
