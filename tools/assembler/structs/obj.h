/*/
/*/
#include <stdlib.h>
struct obj_turing_branch {
	byte write;
	signed char ticker_movement;
	struct name next_instruct;
};
struct obj_turing_state {
	struct name name;
	bool external;
	struct obj_turing_branch branches[256];
};
