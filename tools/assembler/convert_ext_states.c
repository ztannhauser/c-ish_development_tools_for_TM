/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/bsearch/struct.h",
		"~/deps/array/bsearch/singular.c",
		"~/deps/misc/assert.h",
		"../structs/name.h",
		"../structs/ext_state.h",
		"compares/state_and_name.c",
		"convert_ext_states.c",
	}
/*/
#ifndef convert_ext_states_c
#define convert_ext_states_c
void convert_ext_states(struct array* ext_states, struct array* states,
						struct ext_state* linker_ext);
#else
void convert_ext_states(struct array* ext_states, struct array* states,
						struct ext_state* linker_ext) {
	ENTER;
	size_t i, n = ext_states->n;
	verpv(n);
	for(i = 0; i < n; i++) {
		verpv(i);
		struct name* n = arrayp_index(*ext_states, i);
		struct ext_state* ele = &(linker_ext[i]);
		struct array_search_returned abr =
			array_bsearch_singular(states, n, compare_state_and_name);
		assert(abr.exists, "issue!");
		ele->name = *n;
		ele->instruct_index = abr.index;
		verpv(abr.index);
		HERE;
	}
	EXIT;
}
#endif
