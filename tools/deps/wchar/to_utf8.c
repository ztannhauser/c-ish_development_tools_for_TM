/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"~/deps/misc/assert.h",

		"to_utf8.c",
	}
/*/
#ifndef wchar_to_utf8_c
#define wchar_to_utf8_c
unsigned int wchar_to_utf8(unsigned int value, char* output);
#else
unsigned int wchar_to_utf8(unsigned int value, char* output) {
	if(value <= 0x7F) {
		*output = value;
		return 1;
	} else if(value <= 0x7FF) {
		*(output++) = 0b11000000 | (0b00011111 & (value >> 6));
		*(output++) = 0b10000000 | (0b00111111 & (value));
		return 2;
	} else if(value <= 0xFFFF) {
		*(output++) = 0b11100000 | ((0b00001111 & (value >> 12)));
		*(output++) = 0b10000000 | ((0b00111111 & (value >> 6)));
		*(output++) = 0b10000000 | ((0b00111111 & (value)));
		return 3;
	} else if(value <= 0x1FFFFF) {
		*(output++) = 0b11110000 | ((0b00000111 & (value >> 18)));
		*(output++) = 0b10000000 | ((0b00111111 & (value >> 12)));
		*(output++) = 0b10000000 | ((0b00111111 & (value >> 6)));
		*(output++) = 0b10000000 | ((0b00111111 & (value)));
		return 4;
	} else if(value <= 0x3FFFFFF) {
		*(output++) = 0b11111000 | ((0b00000011 & (value >> 24)));
		*(output++) = 0b10000000 | ((0b00111111 & (value >> 18)));
		*(output++) = 0b10000000 | ((0b00111111 & (value >> 12)));
		*(output++) = 0b10000000 | ((0b00111111 & (value >> 6)));
		*(output++) = 0b10000000 | ((0b00111111 & (value)));
		return 5;
	} else if(value <= 0x7FFFFFFF) {
		*(output++) = 0b11111100 | ((0b00000001 & (value >> 30)));
		*(output++) = 0b10000000 | ((0b00111111 & (value >> 24)));
		*(output++) = 0b10000000 | ((0b00111111 & (value >> 18)));
		*(output++) = 0b10000000 | ((0b00111111 & (value >> 12)));
		*(output++) = 0b10000000 | ((0b00111111 & (value >> 6)));
		*(output++) = 0b10000000 | ((0b00111111 & (value)));
		return 6;
	} else {
		assert(0, "Invaild Unicode value!");
	}
}
#endif
