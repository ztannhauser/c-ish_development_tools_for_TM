/*/
	deps =
	{
		"~/deps/typedefs.h",

		"get_utf8_width.c",
	}
/*/
#ifndef get_utf8_width_c
#define get_utf8_width_c
unsigned int get_utf8_width(char c);
#else
unsigned int get_utf8_width(char c) {
	return (0b1 & (c >> 5))
			   ? ((0b1 & (c >> 4))
					  ? ((0b1 & (c >> 3)) ? ((0b1 & (c >> 2)) ? (6) : (5))
										  : (4))
					  : (3))
			   : (2);
}
#endif
