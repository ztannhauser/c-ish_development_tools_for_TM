/*/
/*/
#ifndef typedefs_h
#define typedefs_h
#include <stdint.h>
#include <stdlib.h>
#if __GNUG__
#else
typedef uint8_t bool;
#endif
typedef uint8_t byte;
typedef int Socket;
typedef struct sockaddr_in SocketAddress;
typedef int (*comparator)(const void*, const void*);
#endif
