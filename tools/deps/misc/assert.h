/*//*/
#define assert(conditional, ...)                                              \
	if((conditional) == 0) {                                                  \
		assert_implementation(__FILE__, __LINE__, #conditional, __VA_ARGS__); \
	}

#define sysassert(name, args, cond) \
	assert((name args) cond, #name ": " print_error)

#define print_error "\"%s\"", strerror(errno)

void assert_implementation(const char *file, unsigned int line,
						   const char *conditional,
						   const char *message_format_str, ...);
