/*/
		deps =
		{
				"assert.c",
		}
/*/
#ifndef assert_c
#define assert_c
void assert_implementation(const char *file, unsigned int line,
						   const char *conditional,
						   const char *message_format_str, ...);
#else
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
void assert_implementation(const char *file, unsigned int line,
						   const char *conditional,
						   const char *message_format_str, ...) {
	fprintf(stderr,
			"!!assert() failed in '%s' on line %u!!\n"
			"\tConditional: \"%s\"\n"
			"\t",
			file, line, conditional);
	va_list args;
	va_start(args, message_format_str);
	vfprintf(stderr, message_format_str, args);
	va_end(args);
	fprintf(stderr, "\n");
	exit(13);
}
#endif
