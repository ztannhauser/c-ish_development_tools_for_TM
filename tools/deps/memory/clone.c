/*/
		deps =
		{
				"clone.c",
		}
/*/
#ifndef memclone_c
#define memclone_c
#define memclone2(thing) memclone(&(thing), sizeof(thing))
#define strclone(string) memclone(string, strlen(string) + 1)
#include <stdlib.h>
void *memclone(const void *src, size_t n);
#else
#include <string.h>
void *memclone(const void *src, size_t n) {
	void *returned = malloc(n);
	memcpy(returned, src, n);
	return returned;
}
#endif
