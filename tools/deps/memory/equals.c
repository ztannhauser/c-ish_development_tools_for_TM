/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",

		"equals.c",
	}
/*/
#ifndef memequals_c
#define memequals_c
bool memequals(const void *a, const void *b, size_t n);
#else
bool memequals(const void *a, const void *b, size_t n) {
#define A ((byte *) a)
#define B ((byte *) b)
	size_t i;
	for(i = 0; i < n; i++) {
		if(A[i] != B[i]) {
			return false;
		}
	}
	return true;
}
#endif
