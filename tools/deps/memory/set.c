/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"copy.c",
				"set.c",
		}
/*/
#ifndef memory_set_c
#define memory_set_c
void memory_set(void *dest, const void *src, size_t sizeof_src, size_t n);
#else
void memory_set(void *dest, const void *src, size_t sizeof_src, size_t n) {
	size_t i;
	for(i = 0; i < n; i++) {
		dest = memory_copy(dest, src, sizeof_src);
	}
}
#endif
