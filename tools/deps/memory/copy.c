/*/
		deps =
		{
				"copy.c",
		}
/*/
#ifndef memory_copy_c
#define memory_copy_c
#include <stdlib.h>
unsigned char *memory_copy(unsigned char *dest, const unsigned char *src,
						   size_t n);
#else
unsigned char *memory_copy(unsigned char *dest, const unsigned char *src,
						   size_t n) {
	while(n--) {
		*(dest++) = *(src++);
	}
	return dest;
}
#endif
