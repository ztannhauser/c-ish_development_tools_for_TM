/*//*/
#define sign(val) basic_compare(val, 0)
#define typecast(newtype, data) (*((newtype *) (&(data))))
#define basic_compare(a, b) (((a) == (b)) ? 0 : (((a) > (b)) ? 1 : -1))
#define max(a, b) (((a) > (b)) ? (a) : (b))
#define min(a, b) (((a) < (b)) ? (a) : (b))
#define within_inc(min, val, max) ((min <= val) && (val <= max))
#define contain(min, val, max) \
	((val > max) ? (max) : ((min > val) ? (min) : (val)))
#define swap(a, b)          \
	{                       \
		typeof(a) temp = a; \
		a = b;              \
		b = temp;           \
	}
#define swap2(a, b) \
	{               \
		a += b;     \
		b = a - b;  \
		a = a - b;  \
	}
#define autodec(name, value) typeof(value) name = value;
#define varptr(value)                 \
	({                                \
		typeof(value) temp = (value); \
		&(temp);                      \
	})
#define inline_array(type, value...) \
	({                               \
		type temp[] = value;         \
		temp;                        \
	})
#define tempvar(val, code)         \
	({                             \
		typeof(val) clone = (val); \
		code;                      \
	})
#define eval(mac, a)        \
	({                      \
		typeof(a) _a = (a); \
		mac(_a);            \
	})
#define eval2(mac, a, b)    \
	({                      \
		typeof(a) _a = (a); \
		typeof(b) _b = (b); \
		mac(_a, _b);        \
	})
#define eval3(mac, a, b, c) \
	({                      \
		typeof(a) _a = (a); \
		typeof(b) _b = (b); \
		typeof(c) _c = (c); \
		mac(_a, _b, _c);    \
	})
#define lambda(return_type, params, code)             \
	({                                                \
		return_type temporary_lambda_function params{ \
			code} temporary_lambda_function;          \
	})
