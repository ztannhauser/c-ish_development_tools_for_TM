/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",

				"is_keyword_letter.c",
		}
/*/
#ifndef is_keyword_letter_c
#define is_keyword_letter_c
bool char_is_keyword_letter(char c);
#else
bool char_is_keyword_letter(char c) {
	return ('0' <= c && c <= '9') || ('a' <= c && c <= 'z') ||
		   ('A' <= c && c <= 'Z') || (c == '_');
}
#endif
