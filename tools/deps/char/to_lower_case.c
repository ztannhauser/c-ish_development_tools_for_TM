/*/
		deps =
		{
				"to_lower_case.c",
		}
/*/
#ifndef to_lower_case_c
#define to_lower_case_c
char char_to_lower_case(char input);
#else
char char_to_lower_case(char input) {
	if('A' <= input && input <= 'Z') {
		input += 'a' - 'A';
	}
	return input;
}
#endif
