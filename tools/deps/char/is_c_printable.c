/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",

				"is_c_printable.c",
		}
/*/
#ifndef is_c_printable_c
#define is_c_printable_c
bool is_c_printable(unsigned char c);
#else
bool is_c_printable(unsigned char c) {
	return (' ' <= c && c <= '~') && (c != '\\') && (c != '\'') && (c != '\"');
}
#endif
