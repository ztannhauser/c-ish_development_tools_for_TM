/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"to_lower_case.c",
				"is_hex_digit.c",
		}
/*/
#ifndef char_is_hex_digit_c
#define char_is_hex_digit_c
int char_is_hex_digit(char c);
#else
int char_is_hex_digit(char c) {
	if('0' <= c && c <= '9') {
		return (c - '0') + 1;
	}
	c = char_to_lower_case(c);
	if('a' <= c && c <= 'f') {
		return (c - 'a') + 11;
	}
	return 0;
}
#endif
