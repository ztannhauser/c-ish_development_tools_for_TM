/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",

				"is_blackspace.c",
		}
/*/
#ifndef char_is_blackspace_c
#define char_is_blackspace_c
bool char_is_blackspace(char c);
#else
bool char_is_blackspace(char c) {
	return (c != ' ') && (c != '\n') && (c != '\t') && (c != '\r');
}
#endif
