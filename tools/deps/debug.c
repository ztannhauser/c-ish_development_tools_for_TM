/*/
	deps =
	{
		"~/deps/typedefs.h",
		"debug.c",
	}
/*/
#include <stdio.h>
#ifdef NO_DEBUG
#define ENTER
#define EXIT
#define set_verbose(whatever)
#define get_verbose()
#define verpv(whatver)
#define verpvs(whatever)
#define verpvfs(a, b)
#define verpvc(c)
#define HERE
#define verror(command) command
#define verprintf(...)
#define TODO abort()
#define CHECK abort()
#define NOPE abort()
#else
#define getforstr(val)               \
	(_Generic(val, signed char       \
			  : "%i", unsigned char  \
			  : "%u", signed short   \
			  : "%i", unsigned short \
			  : "%u", signed int     \
			  : "%i", unsigned int   \
			  : "%u", signed long    \
			  : "%li", unsigned long \
			  : "%lu", float         \
			  : "%f", double         \
			  : "%f", long double    \
			  : "%f", default        \
			  : "%p"))
#define verpvin(val)                      \
	({                                    \
		typeof(val) _val = (val);         \
		verprintf(#val " == ");           \
		if(get_verbose()) {               \
			printf(getforstr(val), _val); \
			printf("\n");                 \
		}                                 \
		_val;                             \
	})
#define verpv(val)                   \
	verprintf(#val " == ");          \
	if(get_verbose()) {              \
		printf(getforstr(val), val); \
		printf("\n");                \
	}
#define verpvs(val) verprintf(#val " == \"%s\"\n", val);
#define verpvc(val) verprintf(#val " == '%c'\n", val);
#define verpvfs(val, format_string) \
	verprintf(#val " (%s) == " format_string "\n", format_string, val);
#define ENTER                                 \
	verprintf("<%s>\n", __PRETTY_FUNCTION__); \
	stackdepth++;
#define EXIT      \
	stackdepth--; \
	verprintf("</%s>\n", __PRETTY_FUNCTION__);
#define NOPE                                                  \
	{                                                         \
		set_verbose(1);                                       \
		verprintf("%s: line %u: NOPE\n", __FILE__, __LINE__); \
		exit(42);                                             \
	}
#define CHECK                                                  \
	{                                                          \
		set_verbose(1);                                        \
		verprintf("%s: line %u: CHECK\n", __FILE__, __LINE__); \
		exit(42);                                              \
	}
#define TODO                                                  \
	{                                                         \
		set_verbose(1);                                       \
		verprintf("%s: line %u: TODO\n", __FILE__, __LINE__); \
		exit(42);                                             \
	}
#define HERE verprintf("%s: line %u\n", __FILE__, __LINE__);
extern char *verbose_prefix;
extern size_t stackdepth;
void set_verbose(bool new_val);
bool get_verbose();
void verprintf(const char *format, ...) __attribute__((format(printf, 1, 2)));
#endif
#ifndef debug_c
#define debug_c
#else
#ifndef NO_DEBUG
#include <stdarg.h>
#include <stdlib.h>
bool verbose = 0;
size_t stackdepth = 0;
char *verbose_prefix = "";
void verprintf(const char *format, ...) {
	if(!verbose) {
		return;
	}
	printf("%s", verbose_prefix);
	size_t i;
	for(i = 0; i < stackdepth; i++) {
		printf("  ");
	}
	va_list arg;
	va_start(arg, format);
	vprintf(format, arg);
	va_end(arg);
}
void set_verbose(bool new_val) {
	verbose = new_val;
}
bool get_verbose() {
	return verbose;
}
#endif
#endif
