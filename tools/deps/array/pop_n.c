/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/misc/assert.h",
				"struct.h",
				"pop_n.c",
		}
/*/

#ifndef pop_n_c
#define pop_n_c

void array_pop_n(struct array *this);

#else

void array_pop_n(struct array *this) {
	assert(this->n > 0, "Can't pop empty array!");
	this->n--;
}
#endif
