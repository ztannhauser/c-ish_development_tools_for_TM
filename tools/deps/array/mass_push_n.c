/*/
		deps =
		{
				"~/deps/typedefs.h",
				"struct.h",
				"mass_push_n.c",
		}
/*/
#ifndef array_mass_push_n_c
#define array_mass_push_n_c
#include <stdlib.h>
void array_mass_push_n(struct array *this, void *data_ptr, size_t n);
#else
#include <string.h>
void array_mass_push_n(struct array *this, void *data_ptr, size_t n) {
	size_t mem_m = n * this->sizeof_element;
	size_t mem_n = this->n * this->sizeof_element;
top : {}
	if(mem_m + mem_n <= this->mem_length) {
		if(data_ptr) {
			memcpy(this->data + mem_n, data_ptr, mem_m);
		}
		this->n += n;
	} else {
		this->mem_length = this->mem_length * 2 + 1;
		this->data = realloc(this->data, this->mem_length);
		goto top;
	}
}
#endif
