/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/misc/assert.h",
		"struct.h",
		"push.c",
	}
/*/
#ifndef array_push_c
#define array_push_c
#include <stdlib.h>
void array_push(struct array *this, size_t index, void *ele);
#else
#include <stdio.h>
#include <string.h>
void array_push(struct array *this, size_t index, void *ele) {
	assert(0 <= index && index <= this->n,
		   "Array insertion index not in bounds!");
	size_t mem_n = this->n * this->sizeof_element;
top : {}
	if(this->sizeof_element + mem_n <= this->mem_length) {
		size_t mem_index = index * this->sizeof_element;
		char *i_t = this->data + mem_index;
		memmove(i_t + this->sizeof_element, i_t, mem_n - mem_index);
		if(ele) {
			memcpy(i_t, ele, this->sizeof_element);
		}
		this->n++;
	} else {
		this->mem_length = this->mem_length * 2 + 1;
		this->data = realloc(this->data, this->mem_length);
		goto top;
	}
}
#endif
