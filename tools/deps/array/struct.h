/*/

/*/
#include <inttypes.h>
struct array {
	union {
		byte *data;
		void *datav;
		void **datap;
	};
	uintmax_t n;
	uintmax_t sizeof_element;
	uintmax_t mem_length;
};
#define array_index(array, index, datatype)                   \
	({                                                        \
		assert((0 <= index) && (index < (array).n),           \
			   "Array was indexed beyond vaild index range"); \
		((datatype *) ((array).data))[index];                 \
	})
#define array_rw_index(array, index, datatype) \
	((datatype *) ((array).data))[index]
#define arrayp_index(array, index) \
	((array).data + (array).sizeof_element * (index))
