/*/
	deps =
	{
		"~/deps/typedefs.h",
		"struct.h",
		"clear.c",
	}
/*/
#ifndef array_clear_c
#define array_clear_c
void array_clear(struct array *this);
#else
void array_clear(struct array *this) {
	(this)->n = 0;
}
#endif
