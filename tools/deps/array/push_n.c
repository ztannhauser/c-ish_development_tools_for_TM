/*/
		deps =
		{
				"~/deps/typedefs.h",
				"struct.h",
				"push.c",
				"push_n.c",
		}
/*/
#ifndef array_push_n_c
#define array_push_n_c
#define array_append_null(this) array_push_n(this, varptr('\0'))
void array_push_n(struct array *this, void *element);
#else
void array_push_n(struct array *this, void *element) {
	array_push(this, this->n, element);
}
#endif
