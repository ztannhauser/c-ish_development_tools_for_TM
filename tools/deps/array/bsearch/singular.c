/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/misc/assert.h",
		"../struct.h",
		"struct.h",
		"singular.c",
	}
	compiler_flags =
	{
		//"-DNO_DEBUG"
	}
/*/
#ifndef array_bsearch_c
#define array_bsearch_c
struct array_search_returned array_bsearch_singular(struct array *this,
													const void *ptr,
													comparator compare);
#else
struct array_search_returned array_bsearch_singular(struct array *this,
													const void *ptr,
													comparator compare) {
	ENTER;
#define data (this->data)
#define array_size (this->n)
#define sizeof_element (this->sizeof_element)
	size_t begin_index = 0;
	size_t end_index = array_size - 1;
	if(array_size == 0) {
		EXIT;
		return (struct array_search_returned){false, 0};
	}
	assert(data, "NonEmpty array has null data!");
	if(compare(&data[begin_index * sizeof_element], ptr) < 0) {
		verprintf("lower than the lowest!\n");
		EXIT;
		return (struct array_search_returned){false, 0};
	}
	if(compare(&data[end_index * sizeof_element], ptr) > 0) {
		verprintf("higher than the highest!\n");
		EXIT;
		return (struct array_search_returned){false, array_size};
	}
	while(1) {
		size_t diff_index = end_index - begin_index;
		if(diff_index == 0) {
			int e_c = compare(&data[end_index * sizeof_element], ptr);
			if(e_c > 0) {
				EXIT;
				return (struct array_search_returned){false, end_index + 1};
			}
			if(e_c < 0) {
				EXIT;
				return (struct array_search_returned){false, end_index};
			}
			if(e_c == 0) {
				EXIT;
				return (struct array_search_returned){true, end_index};
			}
		} else if(diff_index == 1) {
			int b_c = compare(&data[begin_index * sizeof_element], ptr);
			int e_c = compare(&data[end_index * sizeof_element], ptr);
			if(b_c < 0) {
				EXIT;
				return (struct array_search_returned){false, begin_index};
			}
			if(b_c == 0) {
				EXIT;
				return (struct array_search_returned){true, begin_index};
			}
			if((b_c > 0) && (e_c < 0)) {
				EXIT;
				return (struct array_search_returned){false, end_index};
			}
			if(e_c == 0) {
				EXIT;
				return (struct array_search_returned){true, end_index};
			}
			EXIT;
			return (struct array_search_returned){false, end_index + 1};
		}
		size_t middle_index = (begin_index + end_index) / 2;
		int c = compare(&data[middle_index * sizeof_element], ptr);
		if(c > 0) {
			begin_index = middle_index + 1;
		} else if(c < 0) {
			end_index = middle_index - 1;
		} else {
			EXIT;
			return (struct array_search_returned){true, middle_index};
		}
	}
}
#endif
