/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"~/deps/misc/assert.h",
				"struct.h",
				"mass_pop_n.c",
		}
/*/
#ifndef array_mass_pop_n_c
#define array_mass_pop_n_c
void array_mass_pop_n(struct array *this, size_t n);
#else
void array_mass_pop_n(struct array *this, size_t n) {
	assert(this->n >= n, "Can't pop empty array!");
	this->n -= n;
}
#endif
