/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/array/struct.h",
		"~/deps/array/delete.c",
	}
/*/
#ifndef array_delete_c
#define array_delete_c
void array_delete(struct array *this);
#else
#include <stdlib.h>
void array_delete(struct array *this) {
	if(this->data) {
		free(this->data);
	}
}
#endif
