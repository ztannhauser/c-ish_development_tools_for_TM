/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/array/struct.h",
		"~/deps/array/new.c",
	}
/*/
#ifndef array_new_c
#define array_new_c
#define array_new(type) array_new_with_inital_n(sizeof(type), 1)
#define arrayp_new() array_new(void *)
#include <stdlib.h>
struct array array_new_with_inital_n(size_t element_length, size_t n);
#else
#include <stdio.h>
struct array array_new_with_inital_n(size_t element_length, size_t n) {
	struct array this;
	this.n = 0;
	this.data = NULL;
	this.mem_length = 0;
	this.sizeof_element = element_length;
	return this;
}
#endif
