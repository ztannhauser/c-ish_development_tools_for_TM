/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"~/deps/misc/assert.h",
				"struct.h",
				"mass_push_n.c",
				"push_from_fd.c",
		}
/*/
#ifndef array_push_from_fd_c
#define array_push_from_fd_c
#include <stdlib.h>
void array_push_from_fd(struct array *this, int fd, size_t n);
#else
#include <unistd.h>
void array_push_from_fd(struct array *this, int fd, size_t n) {
	ENTER;
	size_t bytes_to_read = n * this->sizeof_element;
	verpv(bytes_to_read);
	char *tempbuffer = malloc(bytes_to_read);
	assert(read(fd, tempbuffer, bytes_to_read) == bytes_to_read,
		   "array.push_from_fd could not read enough bytes!");
	array_mass_push_n(this, tempbuffer, n);
	free(tempbuffer);
	EXIT;
}
#endif
