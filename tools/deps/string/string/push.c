/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"~/deps/string/struct.h",
				"push.c",
		}
/*/
#ifndef string_push_c
#define string_push_c
void string_push(struct string *this, char c);
#else
void string_push(struct string *this, char c) {
	this->data[0] = c;
	(this->data)++;
	(this->strlen)++;
}
#endif
