/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"~/deps/string/struct.h",
				"mass_push.c",
		}
/*/
#ifndef string_mass_push_c
#define string_mass_push_c
void string_mass_push(struct string *this, char *data, size_t n);
#else
#include <string.h>
void string_mass_push(struct string *this, char *data, size_t n) {
	memcpy(this->data, data, n);
	(this->data) += n;
	(this->strlen) += n;
}
#endif
