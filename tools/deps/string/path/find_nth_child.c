/*/
		deps =
		{
				"~/deps/string/struct.h",

				"find_nth_child.c",
		}
/*/
#ifndef find_nth_child_c
#define find_nth_child_c
#include <stdlib.h>
/*
		Example: input: {"aaa/bbb/ccc/ddd/eee/fff", strlen($1), 2}, output:
   "eee/fff"
*/
#define path_find_filename(path, strlen_path) \
	find_nth_child(path, strlen_path, 1)
struct string find_nth_child(char *path, size_t strlen_path, size_t depth);
#else
struct string find_nth_child(char *path, size_t strlen_path, size_t depth) {
	size_t i = strlen_path - 1;
	while(depth > 0) {
		if(i == 0) {
			return (struct string){.data = NULL, .strlen = -1};
		}
		i--;
		if(path[i] == '/') {
			depth--;
		}
	}
	i++;
	struct string returned = {.data = &(path[i]), .strlen = strlen_path - i};
	return returned;
}
#endif
