/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"hexchars.c",
	}
/*/
#ifndef hexchars_c
#define hexchars_c
extern const char hexchars[16];
#else
const char hexchars[16] = {'0', '1', '2', '3', '4', '5', '6', '7',
							'8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
#endif
