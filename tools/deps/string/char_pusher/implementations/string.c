/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"~/deps/string/struct.h",
				"~/deps/string/string/push.c",
				"~/deps/string/string/mass_push.c",
				"~/deps/string/char_pusher/struct.h",
				"string.c",
		}
/*/
#ifndef string_char_pusher_string_c
#define string_char_pusher_string_c
extern struct char_pusher string_char_pusher;
#else
void char_pusher_string_push_char(struct string *this, char c) {
	string_push(this, c);
}
void char_pusher_string_mass_push_char(struct string *this, char *data,
									   size_t n) {
	string_mass_push(this, data, n);
}
struct char_pusher string_char_pusher = {
	.my_pc = char_pusher_string_push_char,
	.my_mpc = char_pusher_string_mass_push_char};
#endif
