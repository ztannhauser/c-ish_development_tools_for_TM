/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"../struct.h",
				"uwrite.c",
		}
/*/
#ifndef string_char_pusher_uwrite_c
#define string_char_pusher_uwrite_c
extern struct char_pusher uwrite_char_pusher;
#else
#include <unistd.h>
void char_pusher_uwrite_push_char(void *fd, char c) {
	write(fd, &c, 1);
}
void char_pusher_uwrite_mass_push_char(void *fd, char *data, size_t n) {
	write(fd, data, n);
}
struct char_pusher uwrite_char_pusher = {
	.my_pc = char_pusher_uwrite_push_char,
	.my_mpc = char_pusher_uwrite_mass_push_char};
#endif
