/*/
/*/
#include <stdlib.h>
typedef void (*cp_push_char)(void *ptr, char c);
typedef void (*cp_mass_push_char)(void *ptr, char *data, size_t n);
struct char_pusher {
	cp_push_char my_pc;
	cp_mass_push_char my_mpc;
};
