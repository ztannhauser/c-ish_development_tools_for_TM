/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/defines.h",

				"equals.c",
		}
/*/
#ifndef equals_c
#define equals_c
bool equals(const char *, const char *);
#else
bool equals(const char *a, const char *b) {
	while(1) {
		char A = *(a++);
		char B = *(b++);
		if(A != B) {
			return false;
		}
		if(A == '\0') {
			return true;
		}
	}
}
#endif
