/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"struct.h",
				"inc.c",
		}
/*/
#ifndef file_cursor_inc_c
#define file_cursor_inc_c
#define file_cursor_skip(this, skip) \
	file_cursor_inc(this, skip(this->data, this->strlen))
void file_cursor_inc(struct file_cursor *this, size_t amount);
#else
void file_cursor_inc(struct file_cursor *this, size_t amount) {
	(this->data) += amount;
	(this->size) -= amount;
}
#endif
