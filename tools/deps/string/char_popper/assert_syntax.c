/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"~/deps/misc/assert.h",
		"struct.h",
		"assert_syntax.c",
	}
	compiler_flags =
	{
		"-DNO_DEBUG"
	}
/*/
#ifndef char_popper_assert_syntax_c
#define char_popper_assert_syntax_c
void char_popper_assert_syntax(struct char_popper *cp, char *str,
							   unsigned strlen_str, char *message);
#else
void char_popper_assert_syntax(struct char_popper *cp, char *str,
							   unsigned strlen_str, char *message) {
	ENTER;
	while(strlen_str--) {
		verpvc(*str);
		assert(current_char(cp) == *(str++), message);
		cpassert(move_forward(cp));
	}
	EXIT;
}
#endif
