/*/
/*/
struct char_popper;
typedef bool (*cp_char_pop)(struct char_popper *this);
typedef void (*cp_delete_ptr)(void *ptr);
#define current_char(this) ((this)->buffer[(this)->buffer_index])
#define move_forward(this) (this)->my_cp(this)
#define cpassert(cond) assert(cond, "Char Popper ended Unexpectedly!")
struct char_popper {
	cp_char_pop my_cp;
	cp_delete_ptr my_delete;
	char buffer[2];
	bool buffer_index;
	bool skip_next_read;
	void *object;
};
