/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"struct.h",
				"delete.c",
		}
/*/
#ifndef char_popper_delete_c
#define char_popper_delete_c
void char_popper_delete(struct char_popper *this);
#else
void char_popper_delete(struct char_popper *this) {
	if(this->my_delete) {
		this->my_delete(this->object);
	}
}
#endif
