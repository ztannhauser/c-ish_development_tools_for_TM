/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/string/char_popper/struct.h",
		"while_do_then.c",
	}
	compiler_flags =
	{
		"-DNO_DEBUG"
	}
/*/
#ifndef char_popper_while_do_then_c
#define char_popper_while_do_then_c
typedef bool (*wdt_conditional)(void *cptr, char c);
typedef void (*wdt_action)(void *aptr, char c);
bool char_popper_while_do_then(struct char_popper *this, wdt_conditional cond,
							   void *cont_ptr, wdt_action act, void *act_ptr)
	__attribute__((nonnull(1, 2, 3)));
#else

bool char_popper_while_do_then(struct char_popper *this, wdt_conditional cond,
							   void *cont_ptr, wdt_action act, void *act_ptr) {
	ENTER;
	char c;
	while(cond(cont_ptr, c = current_char(this))) {
		verpvc(c);
		if(act) {
			act(act_ptr, c);
		}
		if(!move_forward(this)) {
			EXIT;
			return false;
		}
	}
	EXIT;
	return true;
}
#endif
