/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"~/deps/quack/struct.h",
				"struct.h",
				"mass_pop.c",
		}
/*/
#ifndef char_popper_mass_pop_c
#define char_popper_mass_pop_c
void char_popper_mass_pop(struct char_popper *this, char *output, size_t n);
#else
void char_popper_mass_pop(struct char_popper *this, char *output, size_t n) {
	ENTER;
	while(n--) {
		*(output++) = this->buffer[this->buffer_index];
		this->my_cp(this);
	}
	EXIT;
}
#endif
