/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"~/deps/char/is_keyword_letter.c",
				"while_is_keyword_letter.c",
		}
/*/
#ifndef char_popper_cond_while_is_keyword_letter_c
#define char_popper_cond_while_is_keyword_letter_c
bool char_popper_cond_while_is_keyword_letter(void *unused, char c);
#else
bool char_popper_cond_while_is_keyword_letter(void *unused, char c) {
	return char_is_keyword_letter(c);
}
#endif
