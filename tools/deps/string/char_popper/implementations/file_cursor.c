/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/defines.h",
				"~/deps/debug.c",
				"~/deps/string/file_cursor/struct.h",
				"~/deps/string/file_cursor/inc.c",
				"../struct.h",
				"file_cursor.c",
		}
/*/
#ifndef file_cursor_pop_c
#define file_cursor_pop_c
bool file_cursor_pop(struct char_popper *this);
#else
#include <string.h>
bool file_cursor_pop(struct char_popper *this) {
	this->buffer_index = !(this->buffer_index);
	if(this->skip_next_read) {
		this->skip_next_read = false;
		return true;
	} else {
#define fc ((struct file_cursor *) (this->object))
		if(fc->size == 0) {
			return false;
		}
		this->buffer[this->buffer_index] = fc->data[0];
		file_cursor_inc(fc, 1);
		return true;
	}
}
#endif
