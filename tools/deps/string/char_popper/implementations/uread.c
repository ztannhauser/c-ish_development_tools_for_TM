/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/string/char_popper/struct.h",
		"uread.c",
	}
/*/
#ifndef char_popper_uread_c
#define char_popper_uread_c
bool char_popper_uread(struct char_popper *this);
#else
#include <unistd.h>
bool char_popper_uread(struct char_popper *this) {
	this->buffer_index = !(this->buffer_index);
	if(this->skip_next_read) {
		this->skip_next_read = false;
		return true;
	} else {
#define fd ((int) (this->object))
		return (read(fd, &(current_char(this)), 1));
	}
}
#endif
