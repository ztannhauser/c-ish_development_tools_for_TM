/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/defines.h",
				"~/deps/debug.c",
				"struct.h",
				"new.c",
		}
		compiler_flags =
		{
				"-DNO_DEBUG"
		}
/*/
#ifndef char_popper_new_c
#define char_popper_new_c
struct char_popper char_popper_new(void *object, cp_char_pop callback,
								   cp_delete_ptr on_delete);
#else
struct char_popper char_popper_new(void *object, cp_char_pop callback,
								   cp_delete_ptr on_delete) {
	ENTER;
	struct char_popper this = {.skip_next_read = false,
							   .my_cp = callback,
							   .my_delete = on_delete,
							   .object = object};
	bool ret = callback(&this);
	verpv(ret);
	EXIT;
	return this;
}
#endif
