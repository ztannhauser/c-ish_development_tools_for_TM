/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/string/char_pusher/struct.h",

				"bool.c",
		}
/*/
#ifndef string_flatten_bool_c
#define string_flatten_bool_c
void string_flatten_bool(void *ptr, struct char_pusher *cp, bool value);
#else
#include <string.h>
void string_flatten_bool(void *ptr, struct char_pusher *cp, bool value) {
	if(value) {
		cp->my_mpc(ptr, "true", 4);
	} else {
		cp->my_mpc(ptr, "false", 5);
	}
}
#endif
