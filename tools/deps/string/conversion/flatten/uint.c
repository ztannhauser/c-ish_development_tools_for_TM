/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"~/deps/math/uint_pow.c",
				"~/deps/string/char_pusher/struct.h",
				"private/prepadding.c",
				"uint.c",
		}
		linker_flags =
		{
				"-lm"
		}
/*/
#ifndef string_flatten_uint_options_c
#define string_flatten_uint_options_c
#include <stdlib.h>
void string_flatten_uint(void *ptr, struct char_pusher *cp, uintmax_t value,
						 bool include_sign, size_t min_length);
#else
#include <math.h>
void string_flatten_uint(void *ptr, struct char_pusher *cp, uintmax_t value,
						 bool include_sign, size_t min_length) {
	if(include_sign) {
		cp->my_pc(ptr, '+');
	}
	int digits = ((value) ? (floor(log10(value)) + 1) : (1));
	prepadding(ptr, cp, min_length - digits);
	uintmax_t factor = uint_pow(10, digits - 1);
	while(digits--) {
		char c = '0' + ((value / factor) % 10);
		cp->my_pc(ptr, c);
		factor /= 10;
	}
}
#endif
