/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"~/deps/string/globals/hexchars.c",
				"~/deps/string/char_pusher/struct.h",
				"hex1.c",
		}
/*/
#ifndef string_flatten_hex1_c
#define string_flatten_hex1_c
void string_flatten_hex1(void *ptr, struct char_pusher *cp, uintmax_t data,
						 unsigned int n, bool append_prefix,
						 bool insert_spaces);
#else
void string_flatten_hex1(void *ptr, struct char_pusher *cp, uintmax_t data,
						 unsigned int n, bool append_prefix,
						 bool insert_spaces) {
	if(append_prefix) {
		cp->my_mpc(ptr, "0x", 2);
	}
	int i;
	for(i = n - 1; i >= 0; i--) {
		char b = data >> (i * 8);
		cp->my_pc(ptr, (hexchars[0b1111 & (b >> 4)]));
		cp->my_pc(ptr, (hexchars[0b1111 & b]));
		if(i) {
			if(insert_spaces) {
				cp->my_pc(ptr, ' ');
			}
		}
	}
}
#endif
