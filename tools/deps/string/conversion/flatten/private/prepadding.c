/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"~/deps/string/char_pusher/struct.h",
				"prepadding.c"
		}
/*/
#ifndef prepadding_c
#define prepadding_c
void prepadding(void *ptr, struct char_pusher *cp, int padding);
#else
void prepadding(void *ptr, struct char_pusher *cp, int padding) {
	if(padding < 0) {
		padding = 0;
	}
	while(padding--) {
		cp->my_pc(ptr, ' ');
	}
}
#endif
