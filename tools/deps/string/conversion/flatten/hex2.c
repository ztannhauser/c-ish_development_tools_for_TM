/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"~/deps/string/globals/hexchars.c",
		"~/deps/string/char_pusher/struct.h",
		"~/deps/string/char_popper/struct.h",
		"hex2.c",
	}
/*/
#ifndef string_flatten_hex2_c
#define string_flatten_hex2_c
void string_flatten_hex2(void *ptr, struct char_pusher *this,
						 struct char_popper *cp, bool append_prefix,
						 bool insert_spaces);
#else
void string_flatten_hex2(void *ptr, struct char_pusher *this,
						 struct char_popper *cp, bool append_prefix,
						 bool insert_spaces) {
	if(append_prefix) {
		this->my_mpc(ptr, "0x", 2);
	}
	goto inside;
	while(move_forward(cp)) {
	inside : {}
		char b = current_char(cp);
		this->my_pc(ptr, (hexchars[0b1111 & (b >> 4)]));
		this->my_pc(ptr, (hexchars[0b1111 & b]));
		if(insert_spaces) {
			this->my_pc(ptr, ' ');
		}
	}
}
#endif
