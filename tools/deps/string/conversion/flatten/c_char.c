/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/defines.h",
				"~/deps/debug.c",
				"~/deps/char/is_c_printable.c",
				"~/deps/string/char_pusher/struct.h",
				"hex1.c",
				"c_char.c",
		}
/*/
#ifndef string_flatten_c_char_c
#define string_flatten_c_char_c
#include <stdlib.h>
void string_flatten_c_char(void *ptr, struct char_pusher *cp, char c,
						   bool add_literals);
#else
void string_flatten_c_char(void *ptr, struct char_pusher *cp, char c,
						   bool add_literals) {
	if(add_literals) {
		cp->my_pc(ptr, '\'');
	}
	if(is_c_printable(c)) {
		cp->my_pc(ptr, c);
	} else {
		switch(c) {
			case '\t':
				cp->my_mpc(ptr, "\\t", 2);
				break;
			case '\e':
				cp->my_mpc(ptr, "\\e", 2);
				break;
			case '\n':
				cp->my_mpc(ptr, "\\n", 2);
				break;
			case '\r':
				cp->my_mpc(ptr, "\\r", 2);
				break;
			case '\"':
				cp->my_mpc(ptr, "\\\"", 2);
				break;
			case '\'':
				cp->my_mpc(ptr, "\\\'", 2);
				break;
			default: {
				cp->my_mpc(ptr, "\\x", 2);
				string_flatten_hex1(ptr, cp, c, sizeof(char), false, false);
				break;
			}
		}
	}
	if(add_literals) {
		cp->my_pc(ptr, '\'');
	}
}
#endif
