/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/wchar/get_utf8_width.c",
		"~/deps/quack/struct.h",
		"~/deps/string/char_popper/struct.h",
		"~/deps/string/char_popper/mass_pop.c",
		"~/deps/wchar/to_utf8.c",
		"~/deps/misc/assert.h",
		"hex.c",
		"char.c",
	}
/*/
#ifndef string_unflatten_char_c
#define string_unflatten_char_c
unsigned int string_unflatten_char(struct char_popper *cp, char *output,
								   bool accepting_unicode);
#else
#include <stdio.h>
#include <string.h>
#include <wchar.h>
unsigned int string_unflatten_char(struct char_popper *cp, char *output,
								   bool accepting_unicode) {
	char c = current_char(cp);
	if(0b1 & (c >> 7)) {
		if(accepting_unicode && ((0b1 & (c >> 6)))) {
			int width = get_utf8_width(c);
			char_popper_mass_pop(cp, output, width);
			return width;
		} else {
			*output = '?';
			return 1;
		}
	}
	cp->my_cp(cp);
	if(c != '\\') {
		*output = c;
		return 1;
	}
	c = current_char(cp);
	switch(c) {
		case '\\':
			cp->my_cp(cp);
			*output = '\\';
			return 1;
		case '\'':
			cp->my_cp(cp);
			*output = '\'';
			return 1;
		case '"':
			cp->my_cp(cp);
			*output = '"';
			return 1;
		case 'e':
			cp->my_cp(cp);
			*output = '\e';
			return 1;
		case 'r':
			cp->my_cp(cp);
			*output = '\r';
			return 1;
		case 't':
			cp->my_cp(cp);
			*output = '\t';
			return 1;
		case 'n':
			cp->my_cp(cp);
			*output = '\n';
			return 1;
		case 'x': {
			cp->my_cp(cp);
			uintmax_t value = string_unflatten_hex(cp, false);
			assert(value < 256, "C escaping in hex only supports byte sizes");
			*output = value;
			return 1;
		}
		case 'u': {
			cp->my_cp(cp);
			uintmax_t value = string_unflatten_hex(cp, false);
			return wchar_to_utf8(value, output);
		}
		default: {
			assert(0,
				   "Without a 'x' or 'u', the base for the escaped character"
				   " is octal, which is not supported!");
		}
	}
}
#endif
