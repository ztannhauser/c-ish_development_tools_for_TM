/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"~/deps/char/is_hex_digit.c",
		"~/deps/string/char_popper/struct.h",
		"~/deps/misc/assert.h",
		"hex.c",
	}
/*/
#ifndef string_unflatten_hex_c
#define string_unflatten_hex_c
uintmax_t string_unflatten_hex(struct char_popper *cp, bool enforce_prefix);
#else
uintmax_t string_unflatten_hex(struct char_popper *cp, bool enforce_prefix) {
	ENTER;
	if(enforce_prefix) {
		assert(current_char(cp) == '0', "Enforced prefix does not exist!");
		cp->my_cp(cp);
		assert(current_char(cp) == 'x', "Enforced prefix does not exist!");
		cp->my_cp(cp);
	}
	uintmax_t value = 0;
	int digit;
	assert(digit = char_is_hex_digit(current_char(cp)), "Not vaild hex digit!");
	while(digit) {
		verpv(digit);
		value = value * 16 + digit - 1;
		if(!cp->my_cp(cp)) {
			verprintf("hit end of stream\n");
			break;
		}
		digit = char_is_hex_digit(current_char(cp));
	}
	EXIT;
	return value;
}
#endif
