/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/string/struct.h",
		"~/deps/string/char_popper/struct.h",
		"~/deps/misc/assert.h",
		"char.c",
		"c_char.c",
	}
/*/
#ifndef string_unflatten_c_char_c
#define string_unflatten_c_char_c
wchar_t string_unflatten_c_char(struct char_popper *cp, bool accepting_unicode);
#else
wchar_t string_unflatten_c_char(struct char_popper *cp,
								bool accepting_unicode) {
	char temp;
	assert(current_char(cp) == '\'', "C char does not begin with \'");
	cp->my_cp(cp);
	wchar_t returned;
	string_unflatten_char(cp, &returned, accepting_unicode);
	assert(current_char(cp) == '\'',
		   "C char does not end with \', it is instead '%c'", current_char(cp));
	cp->my_cp(cp);
	return returned;
}
#endif
