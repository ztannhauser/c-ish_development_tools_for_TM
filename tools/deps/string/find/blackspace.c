/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/char/is_blackspace.c",
		"~/deps/string/char_popper/struct.h",
		"blackspace.c",
	}
	compiler_flags =
	{
		"-DNO_DEBUG",
	}
/*/
#ifndef find_blackspace_c
#define find_blackspace_c
bool find_blackspace(struct char_popper *cp);
#else
bool find_blackspace(struct char_popper *cp) {
	ENTER;
	char c;
	while(1) {
		c = current_char(cp);
		if(char_is_blackspace(c)) {
			EXIT;
			return true;
		}
		if(!(move_forward(cp))) {
			verprintf("hit end!\n");
			EXIT;
			return false;
		}
	}
}
#endif
