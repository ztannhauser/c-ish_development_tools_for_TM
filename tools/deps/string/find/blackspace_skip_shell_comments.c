/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/defines.h",
				"~/deps/debug.c",
				"~/deps/string/char_popper/struct.h",
				"blackspace.c",
				"blackspace_skip_shell_comments.c",
		}
		compiler_flags =
		{
				"-DNO_DEBUG",
		}
/*/
#ifndef find_blackspace_skip_shell_comments_c
#define find_blackspace_skip_shell_comments_c
bool find_blackspace_skip_shell_comments(struct char_popper *cp);
#else
bool find_blackspace_skip_shell_comments(struct char_popper *cp) {
	ENTER;
	while(find_blackspace(cp)) {
		verpvc(current_char(cp));
		verpv(current_char(cp));
		if(current_char(cp) == '#') {
			while(current_char(cp) != '\n') {
				if(!(move_forward(cp))) {
					EXIT;
					return false;
				}
			}
			if(!(move_forward(cp))) {
				EXIT;
				return false;
			}
			continue;
		} else {
			EXIT;
			return true;
		}
	}
	EXIT;
	return false;
}
#endif
