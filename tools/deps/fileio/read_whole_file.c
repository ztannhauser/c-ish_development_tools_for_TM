/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/string/file_cursor/struct.h",

				"read_whole_file.c",
		}
/*/
#ifndef read_whole_file_c
#define read_whole_file_c
struct file_cursor read_whole_file(const char *pathname, bool append_null);
#else
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
struct file_cursor read_whole_file(const char *pathname, bool append_null) {
	int fd = open(pathname, O_RDONLY);
	if(fd < 0) {
		struct file_cursor returned = {NULL, 0};
		return returned;
	}
	struct stat s;
	stat(pathname, &s);
	off_t filesize = s.st_size;
	char *file_buffer = malloc(filesize + append_null);
	read(fd, file_buffer, filesize);
	if(append_null) {
		file_buffer[filesize] = '\0';
	}
	close(fd);
	struct file_cursor returned = {file_buffer, filesize};
	return returned;
}
#endif
