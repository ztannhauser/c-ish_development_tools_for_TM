/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/string/equals.c",

				"does_file_exist.c",
		}
/*/
#ifndef does_file_exist_c
#define does_file_exist_c
enum dfe_returned { dfe_yes = 0, dfe_dir_does_not, dfe_file_does_not };
enum dfe_returned does_file_exist(const char *parent_dir,
								  const char *child_dir);
#else
#include <dirent.h>
#include <stdio.h>
#include <sys/types.h>
enum dfe_returned does_file_exist(const char *parent_dir,
								  const char *child_dir) {
	DIR *dir = opendir(parent_dir);
	if(dir == NULL) {
		return dfe_dir_does_not;
	}
	struct dirent *r;
	while(r = readdir(dir)) {
		if(equals(r->d_name, child_dir)) {
			closedir(dir);
			return dfe_yes;
		}
	}
	closedir(dir);
	return dfe_file_does_not;
}
#endif
