/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"~/deps/fileio/does_file_exist.c",
				"~/deps/string/struct.h",
				"~/deps/string/path/find_nth_child.c",

				"does_path_exist.c",
		}
/*/
#ifndef does_path_exist_c
#define does_path_exist_c
#include <stdlib.h>
/*
	`path' argument must be editable; it will be rewritten to orignal
   value before return
*/
bool does_path_exist(char *path, size_t strlen_path);
#else
bool does_path_exist(char *path, size_t strlen_path) {
	ENTER;
	struct string str = path_find_filename(path, strlen_path);
	verpv(str.data);
	bool returned;
	if(str.data) {
		*(str.data - 1) = '\0';
		verpvs(path);
		returned = (does_file_exist(path, str.data) == dfe_yes);
		*(str.data - 1) = '/';
	} else {
		returned = (does_file_exist("./", path) == dfe_yes);
	}
	EXIT;
	return returned;
}
#endif
