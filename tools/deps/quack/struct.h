/*/
/*/
#include <stdlib.h>
struct quack {
	char *data;
	size_t n;
	size_t sizeof_element;
	size_t memlength;
	size_t i;
};
#define queue quack
#define quack_index(this, index, type)                                      \
	*((type *) ((this).data + (((this).i + index * (this).sizeof_element) % \
							   (this).memlength)))
