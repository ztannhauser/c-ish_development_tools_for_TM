/*/
	deps =
	{
		"~/deps/typedefs.h",
		"exec.c",
	}
/*/
#ifndef exec_c
#define exec_c
#include <stdlib.h>
pid_t exec(const char **args, bool should_wait, void (*callback)());
#else
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
pid_t exec(const char **args, bool should_wait, void (*callback)()) {
	pid_t child;
	if((child = fork()) == 0) {
		if(callback) {
			callback();
		}
		if(execvp(args[0], args) == -1) {
			fprintf(stderr, "exec(): execvp(): \"%s\"\n", strerror(errno));
			exit(1);
		}
	}
	if(should_wait) {
		int status = 0;
		if(waitpid(child, &status, 0) == -1) {
			fprintf(stderr, "exec(): waitpid(): \"%s\"\n", strerror(errno));
			exit(1);
		}
		if(WIFEXITED(status) && (status = WEXITSTATUS(status))) {
			fprintf(stderr, "exec(): exit code == %i\n", status);
			exit(1);
		}
	}
	return child;
}
#endif
