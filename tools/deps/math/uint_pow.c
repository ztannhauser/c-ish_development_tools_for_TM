/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",

				"uint_pow.c",
		}
/*/
#ifndef uint_pow_c
#define uint_pow_c
uintmax_t uint_pow(uintmax_t base, uintmax_t exp);
#else
uintmax_t uint_pow(uintmax_t base, uintmax_t exp) {
	uintmax_t result = 1;
	while(exp) {
		if(exp & 1) {
			result *= base;
		}
		exp >>= 1;
		base *= base;
	}
	return result;
}
#endif
