/*/
/*/
#define STD_IN 0
#define STD_OUT 1
#define STD_ERROR 2
#define PIPE_READ 0
#define PIPE_WRITE 1
#define true 1
#define false 0
#define yes true
#define no false
#define yup true
#define nope false
#define max_pathname 4096
#define thousand *1000
#define million thousand thousand
#define billion thousand million
#define trillion million million
#define READ_WRITE_FOR_ALL \
	S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH
#define READ_WRITE_EXEC_FOR_ALL READ_WRITE_FOR_ALL | S_IXUSR | S_IXGRP | S_IXOTH
