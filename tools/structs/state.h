/*/
/*/
struct turing_branch {
	byte write;
	signed char ticker_movement;
	size_t next_instruct;
};
struct turing_state {
	struct turing_branch branches[256];
};
