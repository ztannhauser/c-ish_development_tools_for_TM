/*/
/*/
struct func_write {
	size_t instruct_index;
	byte begin, end;
	struct name func;
};
