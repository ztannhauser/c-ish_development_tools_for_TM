/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"~/deps/macros.h",
		"../structs/name.h",
		"names.c",
	}
/*/
#ifndef compare_names_c
#define compare_names_c
int compare_names(struct name* a, struct name* b);
#else
#include <string.h>
int compare_names(struct name* a, struct name* b) {
	// verprintf("strcmp(\"%s\", \"%s\");\n", a->data, b->data);
	if(a->strlen != b->strlen) {
		return eval2(basic_compare, a->strlen, b->strlen);
	}
	return strcmp(a->data, b->data);
}
#endif
