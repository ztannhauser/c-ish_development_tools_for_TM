/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/string/struct.h",
		"~/deps/string/string/mass_push.c",
		"~/deps/string/file_cursor/struct.h",
		"~/deps/string/char_popper/struct.h",
		"~/deps/string/char_popper/new.c",
		"~/deps/string/char_popper/implementations/file_cursor.c",
		"~/deps/string/char_pusher/struct.h",
		"~/deps/string/char_pusher/implementations/string.c",
		"~/deps/string/conversion/flatten/hex2.c",
		"~/deps/terminal/escapes.h",

		"pretty_print_tape.c",
	}
	compiler_flags =
	{
		//"-DNO_DEBUG",
	}
/*/
#ifndef pretty_print_tape_c
#define pretty_print_tape_c
void pretty_print_tape(struct array* tape, size_t loc);
#else
#include <string.h>
#define INCLUDE_SPACES true
void pretty_print_tape(struct array* tape, size_t loc) {
	char buffer[4 * tape->n + 10 + 1];
	struct string str_buff = {.data = buffer, .strlen = 0};
	{
		if(loc) {
			struct file_cursor in = {.data = tape->data, .size = loc};
			struct char_popper cp = char_popper_new(&in, file_cursor_pop, NULL);
			string_flatten_hex2(&str_buff, &string_char_pusher, &cp, false,
								INCLUDE_SPACES);
		}
	}
	{
		char* esc1 = BEGIN_ESCS SET_BACKGROUND_CYAN "m";
		string_mass_push(&str_buff, esc1, strlen(esc1));
	}
	{
		struct file_cursor in =
			(struct file_cursor){.data = tape->data + loc, .size = 1};
		struct char_popper cp = char_popper_new(&in, file_cursor_pop, NULL);
		string_flatten_hex2(&str_buff, &string_char_pusher, &cp, false,
							INCLUDE_SPACES);
	}
	{
		char* esc2 = BEGIN_ESCS SET_BACKGROUND_DEFAULT "m";
		string_mass_push(&str_buff, esc2, strlen(esc2));
	}
	loc++;
	if(tape->n > loc) {
		struct file_cursor in = (struct file_cursor){.data = tape->data + loc,
													 .size = tape->n - loc};
		struct char_popper cp = char_popper_new(&in, file_cursor_pop, NULL);
		string_flatten_hex2(&str_buff, &string_char_pusher, &cp, false,
							INCLUDE_SPACES);
	}
	str_buff.data[0] = '\0';
	printf("\t%s\n", buffer);
}
#endif
