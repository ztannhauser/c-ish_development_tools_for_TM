/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/exec.c",
		"~/deps/string/char_pusher/implementations/uwrite.c",
		"~/deps/string/conversion/flatten/hex1.c",
		"~/deps/misc/write_string.c",
		"latex_print_tape.c",
	}
	compiler_flags =
	{
		"-DNO_DEBUG"
	}
/*/

#ifndef pretty_print_tape_c
#define pretty_print_tape_c

void pretty_print_tape(struct array* tape, size_t loc);

#else

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

size_t image_id = 0;

char* latex_command[] = {"pdflatex", NULL, NULL};

void pretty_print_tape(struct array* tape, size_t loc) {
	set_verbose(true);
	ENTER;
	char path[max_pathname];
	snprintf(path, max_pathname, "./emulator-latex-%0.10i.tex", image_id);
	verpvs(path);
	int fd = open(path, O_WRONLY | O_CREAT | O_TRUNC, READ_WRITE_FOR_ALL);
	verpv(fd);
	write_string(fd,
				 "\\documentclass{article}"
				 "\n"
				 "\\usepackage[latin1]{inputenc}"
				 "\n"
				 "\\usepackage{tikz}"
				 "\n"
				 "\\usepackage{mathtools}"
				 "\n"
				 "\\usepackage{xcolor,colortbl}"
				 "\n"
				 "\\usepackage{array}"
				 "\n"
				 "\\usepackage{booktabs}"
				 "\n"
				 "\\usepackage{xcolor}"
				 "\n"
				 "\\usetikzlibrary{shapes,arrows}"
				 "\n"
				 "\\usetikzlibrary{automata, arrows}"
				 "\n"
				 "\\usepackage{verbatim}"
				 "\n"
				 "\\usepackage[active,tightpage]{preview}"
				 "\n"
				 "\\PreviewEnvironment{tabular}"
				 "\n"
				 "\\setlength"
				 "\n"
				 "\\PreviewBorder{5pt}"
				 "\n"
				 "\\begin{document}"
				 "\n"
				 "\\pagestyle{empty}"
				 "\n");
	bool cut_start, cut_end;
	ssize_t start_index = loc - 5, end_index = loc + 5;
	if(cut_start = 0 > start_index)
		start_index = 0;
	if(cut_end = end_index > tape->n - 1)
		end_index = tape->n - 1;
	verpv(start_index);
	verpv(end_index);
	write_string(fd, "\\begin{tabular}{");
	{
		size_t i, n = end_index - start_index + 1 + !cut_start + !cut_end;
		for(i = 0; i < n; i++) {
			write_string(fd, "c");
			if(i + 1 < n) {
				write_string(fd, " | ");
			}
		}
	}
	write_string(fd, "}\n");
	write_string(fd, "\\hline\n");
	{
		size_t index = start_index;
		if(!cut_start) {
			write_string(fd, "$\\dots$&");
		}
		for(; index <= end_index; index++) {
			if(loc == index) {
				write_string(fd, "\\cellcolor{cyan}\n");
			}
			char c = array_rw_index(*tape, index, char);
			verpvc(c);
			string_flatten_hex1(fd, &(uwrite_char_pusher), c, 1, true, false);
			if(index + 1 <= end_index) {
				write_string(fd, " & \n");
			}
			HERE;
		}
		if(!cut_end) {
			write_string(fd, " & \n");
			write_string(fd, "$\\dots$");
		}
	}
	write_string(fd, "\\\\\n");
	write_string(fd,
				 "\\hline"
				 "\n"
				 "\\end{tabular}"
				 "\n"
				 "\\end{document}"
				 "\n");
	close(fd);
	latex_command[1] = path;
	exec(latex_command, true, NULL);
	image_id++;
	EXIT;
}

#endif
