/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/new.c",
		"~/deps/array/push_n.c",
		"~/deps/string/file_cursor/struct.h",
		"~/deps/fileio/read_whole_file.c",
		"~/deps/terminal/escapes.h",
		"~/deps/misc/assert.c",
		"~/deps/misc/assert.h",
		"../enums/syscalls.h",
		"../structs/exe_header.h",
		"../structs/state.h",
		"pretty_print_tape.c",
		//"latex_print_tape.c",
	}
/*/
#include <string.h>
#include <unistd.h>
int main(int n_args, char** args) {
	set_verbose(false);
	ENTER;
	verpv(n_args);
	bool should_wait = true;
	bool should_print = true;
	{
		size_t i;
		for(i = 2; i < n_args; i++) {
			char* arg = args[i];
			char c;
			while(c = *(arg++)) {
				switch(c) {
					case 'w':
						should_wait = false;
						break;
					case 'v':
						set_verbose(true);
						break;
					case 'p':
						should_print = false;
						break;
					default: { assert(0, "Invaild flag '%c'!", c); }
				}
			}
		}
	}
#ifndef NO_DEBUG
	verbose_prefix = "E";
#endif
	verprintf("Hello, World!\n");
	verpvs(args[1]);
	struct file_cursor exe_file = read_whole_file(args[1], false);
	assert(exe_file.data, "Exe does not exist!");
	assert(exe_file.size, "Exe is empty!");
	struct exe_header* h = exe_file.data;
	struct turing_state* states = exe_file.data + sizeof(struct exe_header);
	verpv(h->starting_index);
	struct array tape = array_new(byte);
	size_t current_state = h->starting_index;
	ssize_t current_ticker_position = 0;
	bool happy_halt;
	char zero = 0;
	array_push_n(&tape, &zero);
	while(1) {
		struct turing_state* state = &(states[current_state]);
		byte byte_read = array_index(tape, current_ticker_position, byte);
		struct turing_branch* branch = &(state->branches[byte_read]);
		array_rw_index(tape, current_ticker_position, byte) = branch->write;
		assert(branch->ticker_movement == -1 || branch->ticker_movement == 0 ||
				   branch->ticker_movement == 1,
			   "Invaild ticker movement %i!", branch->ticker_movement);
		current_ticker_position += branch->ticker_movement;
		assert(current_ticker_position >= 0, "Hit bound!");
		while(current_ticker_position >= tape.n) {
			array_push_n(&tape, &zero);
		}
		if(should_print) {
			pretty_print_tape(&tape, current_ticker_position);
		}
		switch(branch->next_instruct) {
			case sys_accept: {
				pretty_print_tape(&tape, current_ticker_position);
				happy_halt = true;
				goto out_while;
				break;
			}
			case sys_reject: {
				pretty_print_tape(&tape, current_ticker_position);
				happy_halt = false;
				goto out_while;
				break;
			}
			case sys_read: {
				byte t = 0;
				read(STD_IN, &t, sizeof(byte));
				array_rw_index(tape, current_ticker_position, byte) = t;
				current_state = h->syscall_return_state_index;
				break;
			}
			case sys_write: {
				byte t;
				t = array_rw_index(tape, current_ticker_position, byte);
				write(STD_OUT, &t, sizeof(byte));
				current_state = h->syscall_return_state_index;
				break;
			}
			case sys_print_tape: {
				pretty_print_tape(&tape, current_ticker_position);
				current_state = h->syscall_return_state_index;
				break;
			}
			default: {
				current_state = branch->next_instruct;
				break;
			}
		}
		if(should_wait) {
			usleep(10 * 1000);
		}
	}
out_while : {}
	printf("exit(%i);\n", !happy_halt);
	EXIT;
}
