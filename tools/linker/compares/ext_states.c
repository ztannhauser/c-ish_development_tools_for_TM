/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"../../structs/name.h",
		"../../structs/ext_state.h",
		"../../compares/names.c",
		"../structs/my_ext_state.h",
		"ext_states.c",
	}
/*/
#ifndef compare_ext_states_c
#define compare_ext_states_c
int compare_ext_states(struct my_ext_state** a, struct my_ext_state* b);
#else
int compare_ext_states(struct my_ext_state** a, struct my_ext_state* b) {
	return compare_names(&((*a)->state.name), &(b->state.name));
}
#endif
