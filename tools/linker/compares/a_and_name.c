/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"../../structs/name.h",
		"../../compares/names.c",
		"../structs/a.h",
		"a_and_name.c",
	}
/*/
#ifndef compare_a_and_name_c
#define compare_a_and_name_c
int compare_a_and_name(struct a** a, struct name* b);
#else
int compare_a_and_name(struct a** a, struct name* b) {
	return compare_names(&((*a)->name), b);
}
#endif
