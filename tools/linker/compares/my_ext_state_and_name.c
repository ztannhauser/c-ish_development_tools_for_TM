/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"../../structs/name.h",
		"../../structs/ext_state.h",
		"../../compares/names.c",
		"../structs/my_ext_state.h",
		"my_ext_state_and_name.c",
	}
/*/
#ifndef compare_my_ext_state_and_name_c
#define compare_my_ext_state_and_name_c
int compare_my_ext_state_and_name(struct my_ext_state** a, struct name* b);
#else
int compare_my_ext_state_and_name(struct my_ext_state** a, struct name* b) {
	verprintf("strcmp(\"%s\", \"%s\");\n", (*a)->state.name.data, b->data);
	return compare_names(&((*a)->state.name), b);
}
#endif
