/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"~/deps/macros.h",
		"~/deps/memory/equals.c",
		"~/deps/array/struct.h",
		"~/deps/array/bsearch/struct.h",
		"~/deps/array/bsearch/singular.c",
		"~/deps/misc/assert.h",
		"../enums/syscalls.h",
		"../structs/name.h",
		"../structs/state.h",
		"../structs/ext_state.h",
		"../structs/branch_index.h",
		"../structs/undef_sym.h",
		"../structs/exe_header.h",
		"structs/object.h",
		"structs/my_ext_state.h",
		"compares/my_ext_state_and_name.c",
		"resolve_symbols.c",
	}
/*/
#ifndef resolve_symbols_c
#define resolve_symbols_c
struct exe_header resolve_symbols(struct array* objects, struct array* externs,
								  size_t* offsets,
								  struct turing_state* all_states);
#else
struct exe_header resolve_symbols(struct array* objects, struct array* externs,
								  size_t* offsets,
								  struct turing_state* all_states) {
	ENTER;
	size_t get_index(struct name * n) {
		ENTER;
		struct array_search_returned abr =
			array_bsearch_singular(externs, n, compare_my_ext_state_and_name);
		assert(abr.exists, "undefined reference to '%s'!", n->data);
		struct my_ext_state* ext =
			array_index(*externs, abr.index, struct my_ext_state*);
		EXIT;
		return offsets[ext->object_index] + ext->state.instruct_index;
	}
	size_t i, n = objects->n;
	for(i = 0; i < n; i++) {
		verpv(i);
		struct object* ele = array_index(*objects, i, struct object*);
		size_t j, m = ele->undef_syms.n;
		verpv(m);
		struct turing_state* my_objects_states = &(all_states[offsets[i]]);
		for(j = 0; j < m; j++) {
			struct undef_sym* ele2 = arrayp_index(ele->undef_syms, j);
			verpvs(ele2->name.data);
			size_t instruction_index;
			if(memequals(ele2->name.data, "accept", 7)) {
				instruction_index = sys_accept;
			} else if(memequals(ele2->name.data, "reject", 7)) {
				instruction_index = sys_reject;
			} else if(memequals(ele2->name.data, "read", 5)) {
				instruction_index = sys_read;
			} else if(memequals(ele2->name.data, "write", 6)) {
				instruction_index = sys_write;
			} else if(memequals(ele2->name.data, "print_tape", 11)) {
				instruction_index = sys_print_tape;
			} else {
				instruction_index = get_index(&(ele2->name));
			}
			verpv(instruction_index);
			size_t k, o = ele2->indexes.n;
			verpv(o);
			for(k = 0; k < o; k++) {
				struct branch_index* ind = arrayp_index(ele2->indexes, k);
				my_objects_states[ind->instruct]
					.branches[ind->branch]
					.next_instruct = instruction_index;
			}
		}
	}
	struct exe_header h = {
		.starting_index = get_index(varptr(((struct name){"start", 5}))),
		.syscall_return_state_index =
			get_index(varptr(((struct name){"syscall_return_state", 20}))),
	};
	EXIT;
	return h;
}
#endif
