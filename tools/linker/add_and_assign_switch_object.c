/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/bsearch/struct.h",
		"~/deps/array/bsearch/singular.c",
		"~/deps/array/new.c",
		"~/deps/array/push.c",
		"~/deps/array/push_n.c",
		"~/deps/misc/assert.h",
		"../structs/name.h",
		"../structs/undef_sym.h",
		"../structs/state.h",
		"../structs/ext_state.h",
		"../structs/branch_index.h",
		"structs/object.h",
		"structs/a.h",
		"structs/my_ext_state.h",
		"structs/my_func_write.h",
		"compares/ext_states.c",
		"add_and_assign_switch_object.c",
	}
/*/
#ifndef add_and_assign_switch_object_c
#define add_and_assign_switch_object_c
size_t add_and_assign_switch_object(struct array* as, struct array* all_externs,
									struct array* objects);
#else
#include <string.h>
size_t add_and_assign_switch_object(struct array* as, struct array* all_externs,
									struct array* objects) {
	ENTER;
	struct object* switches = malloc(sizeof(struct object));
	switches->undef_syms = array_new(struct undef_sym);
	switches->states = array_new(struct turing_state);
	uint16_t i, n = (as->n) >> 8;
	uint16_t j, m = 0xFF & (as->n);
	verpv(n);
	verpv(m);
	struct turing_branch* branch;
	struct turing_state root_switch;
	struct undef_sym sym;
	memcpy(sym.name.data, "reject", 7);
	sym.name.strlen = 4;
	sym.indexes = array_new(struct branch_index);
	struct branch_index bi = {.instruct = n + 1};
	for(i = n + 1; i < 256; i++) {
		branch = &(root_switch.branches[i]);
		branch->write = '#';
		branch->ticker_movement = 0;
		bi.branch = i;
		array_push_n(&(sym.indexes), &bi);
	}
	verpv(sym.indexes.n);
	HERE;
	for(i = 0; i <= n; i++) {
		branch = &(root_switch.branches[i]);
		branch->write = i;
		branch->ticker_movement = 1;
		branch->next_instruct = i;
		verpv(i);
		struct turing_state newswitch;
		bool last_time = (i == n);
		verpv(last_time);
		uint16_t b = (last_time) ? (m) : (256);
		verpv(b);
		if(last_time) {
			uint16_t v;
			for(v = m; v < 256; v++) {
				newswitch.branches[v].write = '#';
				newswitch.branches[v].ticker_movement = 0;
				bi.instruct = n;
				bi.branch = v;
				array_push_n(&(sym.indexes), &bi);
			}
		}
		verpv(sym.indexes.n);
		for(j = 0; j < b; j++) {
			verpv(j);
			newswitch.branches[j].write = j;
			newswitch.branches[j].ticker_movement = -1;
			struct a* a = array_index(*as, (i << 8 | j), struct a*);
			{
				size_t ii, nn = a->writes.n;
				verpv(nn);
				for(ii = 0; ii < nn; ii++) {
					verpv(a->writes.data);
					struct my_func_write* ele = arrayp_index(a->writes, ii);
					struct array* obj_states = &(
						array_index(*objects, ele->object_index, struct object*)
							->states);
					struct turing_state* referer1 =
						arrayp_index(*obj_states, ele->instruct_index);
					verpv(ele->begin);
					verpv(ele->end);
					uint16_t jj;
					for(jj = ele->begin; jj <= ele->end; jj++) {
						referer1->branches[jj].write = i;
					}
					struct turing_state* referer2 = arrayp_index(
						*obj_states,
						referer1->branches[ele->begin].next_instruct);
					for(jj = 0; jj <= 255; jj++) {
						referer2->branches[jj].write = j;
					}
				}
			}
			struct undef_sym sym2;
			sym2.name = a->name;
			verprintf("instruction '%s' is %x, %x\n", sym2.name.data, i, j);
			verpvs(sym2.name.data);
			sym2.indexes = array_new(struct branch_index);
			bi.instruct = i;
			bi.branch = j;
			array_push_n(&(sym2.indexes), &bi);
			array_push_n(&(switches->undef_syms), &sym2);
		}
		array_push_n(&(switches->states), &newswitch);
	}
	array_push_n(&(switches->undef_syms), &sym);
	{
		struct my_ext_state* my_new = malloc(sizeof(struct my_ext_state));
		my_new->object_index = objects->n;
		memcpy(my_new->state.name.data, "root_switch", 12);
		my_new->state.name.strlen = 11;
		my_new->state.instruct_index = switches->states.n;
		struct array_search_returned abr =
			array_bsearch_singular(all_externs, my_new, compare_ext_states);
		assert(!abr.exists, "duplicate externs!");
		verpv(abr.index);
		array_push(all_externs, abr.index, &my_new);
	}
	array_push_n(&(switches->states), &root_switch);
	array_push_n(objects, &switches);
	EXIT;
	return switches->states.n;
}
#endif
