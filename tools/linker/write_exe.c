/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/misc/assert.h",
		"../structs/state.h",
		"../structs/exe_header.h",
		"write_exe.c",
	}
/*/
#ifndef write_exe_c
#define write_exe_c
void write_exe(const char* pathname, struct exe_header h,
			   struct turing_state* states, size_t n_states);
#else
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
void write_exe(const char* pathname, struct exe_header h,
			   struct turing_state* states, size_t n_states) {
	ENTER;
	verpv(n_states);
	int fd = open(pathname, O_WRONLY | O_CREAT | O_TRUNC, READ_WRITE_FOR_ALL);
	assert(fd > 0, "file failed to open!");
	write(fd, &h, sizeof(struct exe_header));
	write(fd, states, sizeof(struct turing_state) * n_states);
	close(fd);
	EXIT;
}
#endif
