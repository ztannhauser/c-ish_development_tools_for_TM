/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/misc/assert.h",

		"handle_args.c",
	}
/*/
#ifndef handle_args_c
#define handle_args_c
struct parsed_args {
	char output_file[max_pathname];
};
void handle_args(struct parsed_args* output, int n_args, char** args);
#else
#include <string.h>
#include <unistd.h>
void handle_args(struct parsed_args* output, int n_args, char** args) {
	ENTER;
	bool output_given = false;
	char c;
	while((c = getopt(n_args, args, "vo:")) != -1) {
		switch(c) {
			case 'v': {
				set_verbose(true);
				break;
			}
			case 'o': {
				output_given = true;
				strcpy(output->output_file, optarg);
				break;
			}
		}
	}
	assert(output_given, "You must specify an output file!");
	EXIT;
}
#endif
