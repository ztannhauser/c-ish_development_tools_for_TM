/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/new.c",
		"~/deps/array/delete.c",
		"~/deps/misc/assert.h",
		"~/deps/misc/assert.c",
		"../structs/state.h",
		"../structs/exe_header.h",
		"read_object_file.c",
		"concat_objects.c",
		"resolve_symbols.c",
		"add_and_assign_switch_object.c",
		"write_exe.c",
		"handle_args.c",
	}
/*/
#include <unistd.h>
int main(int n_args, char** args) {
	set_verbose(false);
	#ifndef NO_DEBUG
	verbose_prefix = "L";
	#endif
	ENTER;
	struct parsed_args pa;
	handle_args(&pa, n_args, args);
	verprintf("Hello, World!\n");
	struct array as = array_new(struct a*);
	struct array all_externs = array_new(struct my_ext_state*);
	struct array objects = array_new(struct object*);
	size_t i;
	size_t total = 0;
	for(i = optind; i < n_args; i++) {
		verpv(i);
		verpvs(args[i]);
		total += read_object_file(&objects, &all_externs, &as, args[i]);
	}
	total += add_and_assign_switch_object(&as, &all_externs, &objects);
	verpv(total);
	struct turing_state* all_states =
		malloc(sizeof(struct turing_state) * total);
	size_t object_offsets[objects.n];
	assert(concat_objects(all_states, &objects, object_offsets) == total,
		   "incorrect summation!");
	struct exe_header h =
		resolve_symbols(&objects, &all_externs, object_offsets, all_states);
	write_exe(pa.output_file, h, all_states, total);
	array_delete(&objects);
	EXIT;
}
