/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/bsearch/struct.h",
		"~/deps/array/bsearch/singular.c",
		"~/deps/array/new.c",
		"~/deps/array/push.c",
		"~/deps/array/push_n.c",
		"~/deps/array/push_from_fd.c",
		"~/deps/string/file_cursor/struct.h",
		"~/deps/fileio/read_whole_file.c",
		"~/deps/misc/assert.h",
		"../structs/name.h",
		"../structs/func_write.h",
		"../structs/ext_state.h",
		"../structs/undef_sym.h",
		"../structs/state.h",
		"../structs/branch_index.h",
		"structs/object.h",
		"structs/my_func_write.h",
		"structs/a.h",
		"structs/my_ext_state.h",
		"compares/a_and_name.c",
		"compares/ext_states.c",
		"read_object_file.c",
	}
/*/
#ifndef read_object_file_c
#define read_object_file_c
size_t read_object_file(struct array* objects, struct array* all_externs,
						struct array* as, const char* filename);
#else
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#define ERROR "file ended unexpectedly!"
size_t read_object_file(struct array* objects, struct array* all_externs,
						struct array* as, const char* filename) {
	ENTER;
	struct object* new = malloc(sizeof(struct object));
	verpvs(filename);
	int fd = open(filename, O_RDONLY);
	assert(fd > 0, "failed to open '%s'!", filename);
	{
		size_t func_writes_n = 0;
		assert(read(fd, &func_writes_n, sizeof(size_t)) == sizeof(size_t),
			   ERROR);
		verpv(func_writes_n);
		size_t i;
		for(i = 0; i < func_writes_n; i++) {
			verpv(i);
			struct func_write fw;
			read(fd, &fw, sizeof(struct func_write));
			verpvs(fw.func.data);
			{
				struct array_search_returned abr =
					array_bsearch_singular(as, &(fw.func), compare_a_and_name);
				verpv(abr.exists);
				verpv(abr.index);
				if(abr.exists) {
					TODO;
				} else {
					struct a* new = malloc(sizeof(struct a));
					new->name = fw.func;
					new->writes = array_new(struct my_func_write);
					struct my_func_write mfw;
					mfw.object_index = objects->n;
					mfw.instruct_index = fw.instruct_index;
					mfw.begin = fw.begin;
					mfw.end = fw.end;
					verpv(new->writes.n);
					array_push_n(&(new->writes), &mfw);
					verpv(new->writes.data);
					verpv(new->writes.n);
					array_push(as, abr.index, &new);
				}
			}
		}
	}
	{
		size_t ext_states_n = 0;
		assert(read(fd, &ext_states_n, sizeof(size_t)) == sizeof(size_t),
			   ERROR);
		verpv(ext_states_n);
		size_t i;
		for(i = 0; i < ext_states_n; i++) {
			verpv(i);
			struct ext_state new;
			read(fd, &new, sizeof(struct ext_state));
			struct my_ext_state* my_new = malloc(sizeof(struct my_ext_state));
			my_new->object_index = objects->n;
			my_new->state = new;
			struct array_search_returned abr =
				array_bsearch_singular(all_externs, my_new, compare_ext_states);
			assert(!abr.exists, "duplicate externs!");
			verpv(abr.index);
			array_push(all_externs, abr.index, &my_new);
		}
	}
	{
		size_t undef_syms_n;
		assert(read(fd, &undef_syms_n, sizeof(size_t)) == sizeof(size_t),
			   ERROR);
		verpv(undef_syms_n);
		new->undef_syms = array_new(struct undef_sym);
		{
			size_t i;
			for(i = 0; i < undef_syms_n; i++) {
				struct undef_sym newus;
				read(fd, &(newus.name), sizeof(struct name));
				verpvs(newus.name.data);
				verpv(newus.name.strlen);
				size_t indexes_n;
				read(fd, &(indexes_n), sizeof(size_t));
				verpv(indexes_n);
				newus.indexes = array_new_with_inital_n(
					sizeof(struct branch_index), indexes_n);
				size_t j;
				for(j = 0; j < indexes_n; j++) {
					struct branch_index index;
					read(fd, &index, sizeof(struct branch_index));
					array_push_n(&(newus.indexes), &index);
				}
				array_push_n(&(new->undef_syms), &newus);
			}
		}
	}
	size_t states_n;
	assert(read(fd, &states_n, sizeof(size_t)) == sizeof(size_t), ERROR);
	verpv(states_n);
	new->states = array_new(struct turing_state);
	array_push_from_fd(&(new->states), fd, states_n);
	array_push_n(objects, &new);
	close(fd);
	EXIT;
	return states_n;
}
#endif
