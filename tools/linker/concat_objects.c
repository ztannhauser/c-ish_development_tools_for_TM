/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/misc/assert.h",
		"../structs/state.h",
		"structs/object.h",
		"concat_objects.c",
	}
/*/
#ifndef concat_objects_c
#define concat_objects_c
size_t concat_objects(struct turing_state* all_states, struct array* objects,
					  size_t* object_offsets);
#else
#include <string.h>
size_t concat_objects(struct turing_state* all_states, struct array* objects,
					  size_t* object_offsets) {
	ENTER;
	size_t current = 0;
	size_t i, n = objects->n;
	for(i = 0; i < n; i++) {
		size_t old_current = object_offsets[i] = current;
		verpv(object_offsets[i]);
		struct array* states =
			&(array_index(*objects, i, struct object*)->states);
		verpv(states->n);
		size_t j, m = states->n;
		for(j = 0; j < m; j++) {
			all_states[current] = array_index(*states, j, struct turing_state);
			uint16_t k;
			for(k = 0; k < 256; k++) {
				all_states[current].branches[k].next_instruct += old_current;
			}
			current++;
		}
	}
	EXIT;
	return current;
}
#endif
