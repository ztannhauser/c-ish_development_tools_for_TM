/*/
/*/
enum turing_syscalls {
	sys_accept = -1,
	sys_reject = -2,
	sys_read = -3,
	sys_write = -4,
	sys_print_tape = -5,
};
