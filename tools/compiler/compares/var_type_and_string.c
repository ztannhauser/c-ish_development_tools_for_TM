/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"../structs/var_type.h",
				"var_type_and_string.c",
		}
/*/
#ifndef compare_var_type_and_string_c
#define compare_var_type_and_string_c
int compare_var_type_and_string(struct var_type **a, char *b);
#else
#include <string.h>
int compare_var_type_and_string(struct var_type **a, char *b) {
	verprintf("strcmp(\"%s\", \"%s\");\n", (*a)->name, b);
	return strcmp((*a)->name, b);
}
#endif
