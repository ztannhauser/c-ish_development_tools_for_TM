/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"../structs/location.h",
				"locations.c",
		}
/*/
#ifndef compare_locations_c
#define compare_locations_c
int compare_locations(struct location *a, struct location *b);
#else
int compare_locations(struct location *a, struct location *b) {
	ENTER;
	if(a->in_temp > b->in_temp) {
		EXIT;
		return 1;
	} else if(a->in_temp < b->in_temp) {
		EXIT;
		return -1;
	} else if(a->offset > b->offset) {
		EXIT;
		return 1;
	} else if(a->offset < b->offset) {
		EXIT;
		return -1;
	}
	EXIT;
	return 0;
}
#endif
