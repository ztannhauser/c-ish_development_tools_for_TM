/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"~/deps/array/struct.h",
				"../structs/prototype.h",
				"prototype_and_string.c",
		}
/*/
#ifndef compare_prototype_and_string_c
#define compare_prototype_and_string_c
int compare_prototype_and_string(struct prototype **a, char *b);
#else
#include <string.h>
int compare_prototype_and_string(struct prototype **a, char *b) {
	return strcmp((*a)->name, b);
}
#endif
