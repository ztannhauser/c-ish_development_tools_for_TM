/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/push_n.c",
		"../../enums/ekind.h",
		"../../enums/skind.h",
		"../../structs/location.h",
		"../../structs/statement/base.h",
		"../../structs/expression/base.h",
		"../../structs/expression/constant.h",
		"../../globals/working.c",
		"../../globals/types.c",
		"constant_expression.c",
	}
/*/
#ifndef push_new_constant_expression_c
#define push_new_constant_expression_c
struct constant_expression *push_new_constant_expression(byte b, bool add,
														 struct var_type *lexp);
#else
struct constant_expression *push_new_constant_expression(
	byte b, bool add, struct var_type *lexp) {
	ENTER;
	verpv(b);
	struct constant_expression *this =
		malloc(sizeof(struct constant_expression));
	verpv(this);
	as_state(this)->kind = sk_expression;
	as_state(this)->start.in_temp = true;
	as_state(this)->start.offset = 0;
	as_exp(this)->kind = ek_constant;
	as_exp(this)->value_type = byte_type;
	as_exp(this)->lvalue_type = lexp;
	this->value = b;
	this->add_stack_size = add;
	EXIT;
	return this;
}
#endif
