/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/misc/assert.h",
		"../../enums/ekind.h",
		"../../enums/skind.h",
		"../../enums/var_type_type.h",
		"../../structs/location.h",
		"../../structs/var_type.h",
		"../../structs/var_declare.h",
		"../../structs/var_details/base.h",
		"../../structs/var_details/struct.h",
		"../../structs/statement/base.h",
		"../../structs/expression/base.h",
		"../../structs/expression/constant.h",
		"../../globals/types.c",
		"../find_offset_of_field.c",
		"lexpress_field.c",
	}
/*/
#ifndef push_new_lexpress_field_c
#define push_new_lexpress_field_c
struct expression *push_new_lexpress_field(struct expression *lexpress,
										   char field_name[256]);
#else
struct expression *push_new_lexpress_field(struct expression *lexpress,
										   char field_name[256]) {
	ENTER;
	verpvs(field_name);
	assert(lexpress->value_type == byte_type, "!!");
	struct find_offset_of_field_returned foof =
		find_offset_of_field(lexpress->lvalue_type, field_name);
	verpv(foof.offset);
	struct struct_var_details *str = lexpress->lvalue_type->det;
	struct expression *returned;
	if(lexpress->kind == ek_constant) {
		struct constant_expression *spef = lexpress;
		spef->value += foof.offset;
		lexpress->lvalue_type =
			array_index(str->fields, foof.index, struct var_declare).type;
		returned = lexpress;
	} else {
		TODO;
	}
	EXIT;
	return returned;
}
#endif
