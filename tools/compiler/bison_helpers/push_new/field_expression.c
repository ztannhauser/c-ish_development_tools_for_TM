/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/misc/assert.h",
		"../../enums/ekind.h",
		"../../enums/skind.h",
		"../../enums/var_type_type.h",
		"../../structs/location.h",
		"../../structs/statement/base.h",
		"../../structs/expression/base.h",
		"../../structs/var_details/base.h",
		"../../structs/var_details/struct.h",
		"../../structs/var_declare.h",
		"../../structs/var_type.h",
		"../find_offset_of_field.c",
		"memcpy_expression.c",
		"field_expression.c",
	}
/*/
#ifndef push_new_field_expression_c
#define push_new_field_expression_c
struct expression *push_new_field_expression(struct expression *rstruct,
											 char field[256]);
#else
struct expression *push_new_field_expression(struct expression *rstruct,
											 char field[256]) {
	ENTER;
	struct find_offset_of_field_returned foof =
		find_offset_of_field(rstruct->value_type, field);
	verpv(foof.index);
	verpv(foof.offset);
	struct expression *returned;
	struct struct_var_details *str = rstruct->value_type->det;
	// if(foof.offset) // causes bugs if uncommented
	{
		returned = push_new_memcpy_expression(
			(struct location){.in_temp = true, .offset = 0}, true,
			(struct location){.in_temp = true, .offset = foof.offset}, true,
			array_index(str->fields, foof.index, struct var_declare).type,
			rstruct);
	}
#if 0
	 else {
		rstruct->value_type =
			array_index(str->fields, 0, struct var_declare).type;
		returned = rstruct;
	}
#endif
	EXIT;
	return returned;
}
#endif
