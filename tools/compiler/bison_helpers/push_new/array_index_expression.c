/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/bsearch/struct.h",
		"~/deps/array/bsearch/singular.c",
		"~/deps/misc/assert.h",
		"../../enums/skind.h",
		"../../enums/ekind.h",
		"../../enums/var_type_type.h",
		"../../structs/location.h",
		"../../structs/statement/base.h",
		"../../structs/expression/base.h",
		"../../structs/expression/array_index.h",
		"../../structs/var_details/base.h",
		"../../structs/var_details/array.h",
		"../../structs/var_type.h",
		"../../structs/expression_ll.h",
		"../../compares/prototype_and_string.c",
		"../../globals/types.c",
		"../../globals/prototypes.c",
		"../shift_expression.c",
		"constant_expression.c",
		"function_expression.c",
		"array_index_expression.c",
	}
/*/
#ifndef push_new_array_index_expression_c
#define push_new_array_index_expression_c
struct expression* push_new_array_index_expression(struct expression* rarray,
												   struct expression* rindex);
#else
struct expression* push_new_array_index_expression(struct expression* rarray,
												   struct expression* rindex) {
	ENTER;
	assert(rarray->value_type->det->type == vtt_array,
		   "rarray must produce an array!");
	assert(rindex->value_type == byte_type,
		   "rindex must produce a byte address!");
	struct expression* dist_express;
	{
		struct expression* product;
		struct array_var_details* det = rarray->value_type->det;
		{
			verpv(det->element_type->det->size) struct constant_expression* s =
				push_new_constant_expression(
					(0x100 - det->element_type->det->size), false, NULL);
			struct array_search_returned ab = array_bsearch_singular(
				&prototypes, "byte_multiply", compare_prototype_and_string);
			assert(
				ab.exists,
				"implict function call without a prototype! (byte_multiply)");
			struct expression_ll* params1 =
				malloc(sizeof(struct expression_ll));
			struct expression_ll* params2 =
				malloc(sizeof(struct expression_ll));
			params1->express = s;
			params1->next = params2;
			params2->express = rindex;
			params2->next = NULL;
			product = push_new_function_expression(
				array_index(prototypes, ab.index, struct prototype*), params1,
				NULL);
		}
		{
			struct constant_expression* s = push_new_constant_expression(
				(rarray->value_type->det->size + 4 -
						det->element_type->det->size),
				false, NULL);
			struct array_search_returned ab = array_bsearch_singular(
				&prototypes, "byte_add", compare_prototype_and_string);
			assert(ab.exists,
				   "implict function call without a prototype! (byte_add)");
			struct expression_ll* params1 =
				malloc(sizeof(struct expression_ll));
			struct expression_ll* params2 =
				malloc(sizeof(struct expression_ll));
			params1->express = s;
			params1->next = params2;
			params2->express = product;
			params2->next = NULL;
			dist_express = push_new_function_expression(
				array_index(prototypes, ab.index, struct prototype*), params1,
				NULL);
		}
	}
	shift_expression(dist_express, rarray->value_type->det->size);
	struct array_index_expression* this =
		malloc(sizeof(struct array_index_expression));
	as_state(this)->kind = sk_expression;
	as_state(this)->start.in_temp = true;
	as_state(this)->start.offset = rarray->value_type->det->size + 1;
	as_exp(this)->kind = ek_array_index;
	as_exp(this)->value_type =
		((struct array_var_details*) (rarray->value_type->det))->element_type;
	this->rarray = rarray;
	this->rindex = dist_express;
	EXIT;
	return this;
}
#endif
