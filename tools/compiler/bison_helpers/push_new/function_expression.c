/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/misc/assert.h",
		"../../enums/ekind.h",
		"../../enums/skind.h",
		"../../enums/var_type_type.h",
		"../../structs/location.h",
		"../../structs/prototype.h",
		"../../structs/statement/base.h",
		"../../structs/expression/base.h",
		"../../structs/expression/function.h",
		"../../structs/var_details/base.h",
		"../../structs/var_type.h",
		"../../structs/var_declare.h",
		"../../structs/expression_ll.h",
		"../../structs/var_declare_ll.h",
		"../../globals/working.c",
		"../shift_expression.c",
		"function_expression.c",
	}
/*/
#ifndef push_new_function_expression_c
#define push_new_function_expression_c
struct function_expression *push_new_function_expression(
	struct prototype *proto, struct expression_ll *params,
	struct var_type *lexp);
#else
#include <string.h>
struct function_expression *push_new_function_expression(
	struct prototype *proto, struct expression_ll *params,
	struct var_type *lexp) {
	ENTER;
	struct function_expression *this =
		malloc(sizeof(struct function_expression));
	verpv(this);
	this->parent.parent.kind = sk_expression;
	this->parent.kind = ek_function;
	this->params = params;
	as_exp(this)->lvalue_type = lexp;
	strcpy(this->name, proto->name);
	size_t consumed = 0;
	this->parent.value_type = proto->return_type;
	struct var_declare_ll *p = proto->params;
	verpv(p);
	while(p) {
		assert(params, "Arguments too short!");
		struct expression *param = params->express;
		verpv(param);
		verpvs(p->declare.name);
		verpv(p->declare.type->det);
		verpv(param->value_type->det);
		verpv(p->declare.type->det->size);
		verpv(param->value_type->det->size);
		assert(p->declare.type->det == param->value_type->det,
			   "Arguments have incorrect types");
		if(consumed) {
			shift_expression(param, consumed);
		}
		consumed += param->value_type->det->size;
		p = p->next;
		params = params->next;
	}
	assert(params == NULL, "Arguements too long!");
	{
		size_t parents_bytes_written = proto->return_type->det->size;
		if(parents_bytes_written > consumed) {
			consumed = parents_bytes_written;
		}
	}
	this->consumed = consumed;
	this->parent.parent.start.in_temp = true;
	this->parent.parent.start.offset = consumed;
	EXIT;
	return this;
}
#endif
