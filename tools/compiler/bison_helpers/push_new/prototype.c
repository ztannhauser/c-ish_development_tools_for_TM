/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/macros.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/bsearch/struct.h",
		"~/deps/array/bsearch/singular.c",
		"~/deps/array/push.c",
		"~/deps/memory/clone.c",
		"~/deps/misc/assert.h",
		"../../structs/var_type.h",
		"../../structs/prototype.h",
		"../../compares/prototype_and_string.c",
		"../../globals/prototypes.c",
		"prototype.c",
	}
/*/
#ifndef push_new_prototype_c
#define push_new_prototype_c
struct prototype *push_new_prototype(char *name, struct var_type *return_type,
									 struct var_declare_ll *params,
									 bool complain);
#else
#include <string.h>
struct prototype *push_new_prototype(char *name, struct var_type *return_type,
									 struct var_declare_ll *params,
									 bool complain) {
	ENTER;
	struct prototype new;
	strcpy(new.name, name);
	new.return_type = return_type;
	new.params = params;
	struct array_search_returned ab =
		array_bsearch_singular(&prototypes, name, compare_prototype_and_string);
	if(ab.exists) {
		if(complain) {
			assert(0, "prototype '%s' already exists!", name);
		}
	} else {
		verprintf("pushed\n");
		struct prototype *returned = memclone(&new, sizeof(new));
		array_push(&prototypes, ab.index, &returned);
		EXIT;
		return returned;
	}
}
#endif
