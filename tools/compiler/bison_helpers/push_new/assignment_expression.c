/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/misc/assert.h",
		"../../enums/ekind.h",
		"../../enums/skind.h",
		"../../enums/var_type_type.h",
		"../../structs/location.h",
		"../../structs/statement/base.h",
		"../../structs/expression/base.h",
		"../../structs/expression/assignment.h",
		"../../structs/expression/constant.h",
		"../../structs/expression/function.h",
		"../../structs/var_details/base.h",
		"../../structs/var_type.h",
		"../../structs/expression_ll.h",
		"../../globals/types.c",
		"../shift_expression.c",
		"assignment_expression.c",
	}
/*/
#ifndef push_new_assignment_expression_c
#define push_new_assignment_expression_c
struct assignment_expression *push_new_assignment_expression(
	struct expression *lexp, struct expression *rexp);
#else
struct assignment_expression *push_new_assignment_expression(
	struct expression *lexp, struct expression *rexp) {
	ENTER;
	assert(lexp->value_type == byte_type, "lexp must produce a byte address!");
	assert(lexp->lvalue_type == rexp->value_type,
		   "assigned to different types!");
	shift_expression(lexp, rexp->value_type->det->size);
	struct assignment_expression *this =
		malloc(sizeof(struct assignment_expression));
	as_state(this)->kind = sk_expression;
	as_state(this)->start.in_temp = true;
	as_state(this)->start.offset = rexp->value_type->det->size + 1;
	verpv(as_state(this)->start.offset);
	as_exp(this)->kind = ek_assignment;
	as_exp(this)->value_type = rexp->value_type;
	this->left = lexp;
	this->right = rexp;
	EXIT;
	return this;
}
#endif
