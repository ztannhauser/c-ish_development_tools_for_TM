/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/bsearch/struct.h",
		"~/deps/array/bsearch/singular.c",
		"~/deps/misc/assert.h",
		"function_expression.c",
		"constant_expression.c",
		"rexpress_index.c",
	}
/*/
#ifndef push_new_rexpress_index_c
#define push_new_lexpress_index_c
struct expression *push_new_rexpress_index(struct expression *array,
										   struct expression *index);
#else
struct expression *push_new_rexpress_index(struct expression *array,
										   struct expression *index) {
	ENTER;
	assert(index->value_type == byte_type, "!!");
	assert(array->value_type->det->type == vtt_array,
		   "expression begin indexed is not an array!");
	HERE;
	struct array_var_details *avd = array->lvalue_type->det;
	struct expression *returned;
	HERE;
	if((array->kind == ek_constant) && (index->kind == ek_constant)) {
		HERE;
		struct constant_expression *array_spef = array;
		struct constant_expression *index_spef = index;
		verpv(array_spef->value);
		verpv(index_spef->value);
		HERE;
		verpv(avd);
		verpv(avd->element_type);
		HERE;
		returned = push_new_constant_expression(
			verpvin(array_spef->value +
					index_spef->value * (avd->element_type->det->size)),
			false, avd->element_type);
		HERE;
		free(array);
		free(index);
	} else {
		verpv(avd->element_type);
		verpv(avd->element_type->det->size);
		verpvs(avd->element_type->name);
		struct function_expression *product;
		{
			struct expression *const_size = push_new_constant_expression(
				avd->element_type->det->size, false, NULL);
			struct array_search_returned ab = array_bsearch_singular(
				&prototypes, "byte_multiply", compare_prototype_and_string);
			assert(
				ab.exists,
				"implict function call without a prototype! (byte_multiply)");
			struct expression_ll *params1 =
				malloc(sizeof(struct expression_ll));
			struct expression_ll *params2 =
				malloc(sizeof(struct expression_ll));
			params1->express = index;
			params1->next = params2;
			params2->express = const_size;
			params2->next = NULL;
			product = push_new_function_expression(
				array_index(prototypes, ab.index, struct prototype *), params1,
				NULL);
		}
		{
			struct array_search_returned ab = array_bsearch_singular(
				&prototypes, "byte_add", compare_prototype_and_string);
			assert(ab.exists,
				   "implict function call without a prototype! (byte_add)");
			struct expression_ll *params1 =
				malloc(sizeof(struct expression_ll));
			struct expression_ll *params2 =
				malloc(sizeof(struct expression_ll));
			params1->express = array;
			params1->next = params2;
			params2->express = product;
			params2->next = NULL;
			returned = push_new_function_expression(
				array_index(prototypes, ab.index, struct prototype *), params1,
				avd->element_type);
		}
	}
	EXIT;
	return returned;
}
#endif
