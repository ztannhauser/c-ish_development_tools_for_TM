/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/macros.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/new.c",
		"~/deps/array/bsearch/struct.h",
		"~/deps/array/bsearch/singular.c",
		"~/deps/array/push.c",
		"~/deps/array/push_n.c",
		"~/deps/memory/clone.c",
		"~/deps/misc/assert.h",
		"../../enums/var_type_type.h",
		"../../structs/var_details/base.h",
		"../../structs/var_details/struct.h",
		"../../structs/var_type.h",
		"../../structs/var_declare.h",
		"../../structs/var_declare_ll.h",
		"../../compares/var_type_and_string.c",
		"../../globals/structs.c",
		"struct.c",
	}
/*/
#ifndef push_new_struct_c
#define push_new_struct_c
void push_new_struct(char *struct_name, struct var_declare_ll *ll);
#else
#include <string.h>
void push_new_struct(char *struct_name, struct var_declare_ll *ll) {
	ENTER;
	verpvs(struct_name);
	struct struct_var_details new_det;
	new_det.parent.type = vtt_struct;
	new_det.parent.size = 0;
	new_det.fields = array_new(struct var_declare);
	while(ll) {
		new_det.parent.size += ll->declare.type->det->size;
		array_push_n(&(new_det.fields), &(ll->declare));
		struct var_declare_ll *old_ll = ll;
		ll = ll->next;
		free(old_ll);
	}
	struct var_type new;
	new.det = memclone(&new_det, sizeof(new_det));
	strcpy(new.name, struct_name);
	struct array_search_returned ab = array_bsearch_singular(
		&structs, struct_name, compare_var_type_and_string);
	assert(!ab.exists, "struct '%s' already exists!", struct_name);
	array_push(&structs, ab.index, varptr(memclone(&new, sizeof(new))));
	EXIT;
}
#endif
