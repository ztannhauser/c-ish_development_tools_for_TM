/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/macros.h",
		"~/deps/debug.c",
		"~/deps/memory/clone.c",
		"~/deps/array/struct.h",
		"~/deps/array/push_n.c",
		"../../enums/var_type_type.h",
		"../../structs/var_details/base.h",
		"../../structs/var_type.h",
		"../../structs/var_declare.h",
		"../../structs/stack.h",
		"../../globals/working.c",
		"variable.c",
	}
/*/
#ifndef push_new_variable_c
#define push_new_variable_c
void push_new_variable(struct var_declare dec);
#else
void push_new_variable(struct var_declare dec) {
	ENTER;
	current_stack_size += dec.type->det->size;
	if(current_stack_size > max_stack_size) {
		max_stack_size = current_stack_size;
	}
	verpv(wstack.vars.data);
	array_push_n(&wstack, &(dec));
	verpv(wstack.vars.data);
	EXIT;
}
#endif
