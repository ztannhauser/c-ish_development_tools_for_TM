/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/push_n.c",
		"../../enums/skind.h",
		"../../structs/location.h",
		"../../structs/statement/base.h",
		"../../structs/statement/goto.h",
		"../../globals/working.c",
		"goto_statement.c",
	}
/*/
#ifndef push_new_goto_statement_c
#define push_new_goto_statement_c
void push_new_goto_statement(char text[256]);
#else
#include <string.h>
void push_new_goto_statement(char text[256]) {
	ENTER;
	struct goto_statement *this = malloc(sizeof(struct goto_statement));
	array_push_n(&wstatements, &this);
	this->parent.kind = sk_goto;
	this->instruction_index = -1;
	strcpy(this->label, text);
	EXIT;
}
#endif
