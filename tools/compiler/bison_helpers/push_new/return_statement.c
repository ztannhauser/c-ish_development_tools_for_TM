/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/push_n.c",
		"../../enums/skind.h",
		"../../structs/location.h",
		"../../structs/statement/base.h",
		"../../structs/statement/return.h",
		"../../globals/working.c",
		"return_statement.c",
	}
/*/
#ifndef push_new_return_statement_c
#define push_new_return_statement_c
void push_new_return_statement();
#else
void push_new_return_statement() {
	struct return_statement *this = malloc(sizeof(struct return_statement));
	array_push_n(&wstatements, &this);
	this->parent.kind = sk_return;
	this->parent.start.in_temp = false;
	this->parent.start.offset = 0;
}
#endif
