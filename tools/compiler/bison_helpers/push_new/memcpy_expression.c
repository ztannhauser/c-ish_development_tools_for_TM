/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/push_n.c",
		"../../enums/ekind.h",
		"../../enums/skind.h",
		"../../enums/var_type_type.h",
		"../../structs/location.h",
		"../../structs/var_type.h",
		"../../structs/var_details/base.h",
		"../../structs/statement/base.h",
		"../../structs/expression/base.h",
		"../../structs/expression/memcpy.h",
		"../../globals/working.c",
		"../../compares/locations.c",
		"memcpy_expression.c",
	}
/*/
#ifndef push_new_memcpy_expression_c
#define push_new_memcpy_expression_c
struct memcpy_expression *push_new_memcpy_expression(
	struct location dest, bool shift_dest, struct location src, bool shift_src,
	struct var_type *value_type, struct expression *child);
#else
struct memcpy_expression *push_new_memcpy_expression(
	struct location dest, bool shift_dest, struct location src, bool shift_src,
	struct var_type *value_type, struct expression *child) {
	ENTER;
	struct memcpy_expression *this = malloc(sizeof(struct memcpy_expression));
	as_state(this)->kind = sk_expression;
	as_exp(this)->kind = ek_memcpy;
	as_exp(this)->value_type = value_type;
	as_exp(this)->lvalue_type = NULL;
	this->src = src;
	this->shift_src = shift_src;
	this->dest = dest;
	this->shift_dest = shift_dest;
	this->child = child;
	as_state(this)->start =
		(this->src_right = compare_locations(&(this->src), &(this->dest)) > 0)
			? (this->src)
			: (this->dest);
	unsigned int bytes_written = value_type->det->size;
	as_state(this)->start.offset += bytes_written;
	EXIT;
	return this;
}
#endif
