/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/string/char_pusher/implementations/uwrite.c",
		"~/deps/string/conversion/flatten/bool.c",
		"~/deps/string/conversion/flatten/uint.c",
		"~/deps/string/conversion/flatten/c_char.c",
		"~/deps/misc/assert.h",
		"../enums/skind.h",
		"../enums/ekind.h",
		"../enums/var_type_type.h",
		"../structs/location.h",
		"../structs/stack.h",
		"../structs/prototype.h",
		"../structs/statement/base.h",
		"../structs/statement/if.h",
		"../structs/statement/goto.h",
		"../structs/expression/base.h",
		"../structs/expression/array_index.h",
		"../structs/expression/constant.h",
		"../structs/expression/memcpy.h",
		"../structs/expression/function.h",
		"../structs/var_details/base.h",
		"../structs/var_type.h",
		"../globals/working.c",
		"../globals/flags.c",
		"flush_function.c",
	}
/*/
#ifndef flush_function_c
#define flush_function_c
void flush_function(struct prototype *p);
#else
#include <string.h>
#include <unistd.h>
void flush_function(struct prototype *p) {
	assert(wstatements.n, "Oops! No statements!");
#define afd (assembler_fds[PIPE_WRITE])
	write(afd, "extern ", 7);
	size_t strlen_func_name = strlen(p->name);
	size_t pos = 0;
	size_t flatten(struct location loc) {
		size_t goal;
		goal = loc.offset + (loc.in_temp ? max_stack_size : 0);
		return goal;
	}
	void write_name(size_t i, size_t iid, bool type) {
		write(afd, p->name, strlen_func_name);
		if(i || iid || type) {
			write(afd, "_", 1);
			string_flatten_uint(afd, &uwrite_char_pusher, i, false, 0);
			write(afd, "_", 1);
			string_flatten_bool(afd, &uwrite_char_pusher, type);
			write(afd, "_", 1);
			string_flatten_uint(afd, &uwrite_char_pusher, iid, false, 0);
		}
	}
	size_t i = 0;
	bool build_instructions(size_t end) {
		bool bi_returned = true;
		size_t iid;
		for(; i < end; i++) {
			struct statement *sta =
				array_index(wstatements, i, struct statement *);
			iid = 0;
			void move_to(size_t my_i, size_t goal, bool type) {
				if(goal == pos) {
					return;
				}
				if(goal > pos) {
					while(goal > pos) {
						write_name(my_i, iid, type);
						write(afd, ": * => *, R, ", 13);
						iid++;
						pos++;
						write_name(my_i, iid, type);
						write(afd, ";\n", 2);
					}
				} else {
					while(goal < pos) {
						write_name(my_i, iid, type);
						write(afd, ": * => *, L, ", 13);
						iid++;
						pos--;
						write_name(my_i, iid, type);
						write(afd, ";\n", 2);
					}
				}
			}
			void invoke_mass_pickup(char distance, char bytes_to_get) {
				distance += 3;
				write_name(i, iid, true);
				write(afd, ": * => ", 7);
				write_name(i, iid + 3, true);
				write(afd, ", R, ", 5);
				iid++;
				write_name(i, iid, true);
				write(afd, ";\n", 2);
				write_name(i, iid, true);
				write(afd, ": * => ", 7);
				string_flatten_c_char(afd, &uwrite_char_pusher, distance, true);
				write(afd, ", R, ", 5);
				iid++;
				write_name(i, iid, true);
				write(afd, ";\n", 2);
				write_name(i, iid, true);
				write(afd, ": * => ", 7);
				string_flatten_c_char(afd, &uwrite_char_pusher, bytes_to_get,
									  true);
				iid++;
				write(afd, ", L, mass_pickup;\n", 18);
				write(afd, "extern ", 7);
			}
			void invoke_mass_dropoff(char bytes_to_set, char distance) {
				distance += 3;
				write_name(i, iid, true);
				write(afd, ": * => ", 7);
				write_name(i, iid + 3, true);
				write(afd, ", R, ", 5);
				iid++;
				write_name(i, iid, true);
				write(afd, ";\n", 2);
				write_name(i, iid, true);
				write(afd, ": * => ", 7);
				string_flatten_c_char(afd, &uwrite_char_pusher, bytes_to_set,
									  true);
				iid++;
				write(afd, ", R, ", 5);
				write_name(i, iid, true);
				write(afd, ";\n", 2);
				write_name(i, iid, true);
				write(afd, ": * => ", 7);
				string_flatten_c_char(afd, &uwrite_char_pusher, distance, true);
				write(afd, ", L, ", 5);
				write(afd, "mass_dropoff", 12);
				write(afd, ";\n", 2);
				write(afd, "extern ", 7);
				iid++;
			}
			size_t home;
			move_to(i, home = flatten(sta->start), false);
			write_name(i, iid, false);
			write(afd, ": * => *, N, ", 13);
			iid = 0;
			write_name(i, iid, true);
			write(afd, ";\n", 2);
#define BREAKPOINT(LETTER)                                 \
	{                                                      \
		write_name(i, iid, true);                          \
		write(afd, ": * => '" LETTER "', N, halt;\n", 21); \
		iid++;                                             \
	}
			switch((sta->kind)) {
				case sk_expression: {
					struct expression *exp = sta;
					byte bytes_written = exp->value_type->det->size;
					switch((exp->kind)) {
						case ek_constant: {
							HERE;
							struct constant_expression *spef = exp;
							write_name(i, iid, true);
							write(afd, ": * => ", 7);
							if(spef->add_stack_size) {
								spef->value += max_stack_size;
							}
							string_flatten_c_char(afd, &uwrite_char_pusher,
												  spef->value, true);
							write(afd, ", N, ", 5);
							if(i + 1 < end) {
								write_name(i + 1, 0, false);
								write(afd, ";\n", 2);
							}
							break;
						}
						case ek_memcpy: {
							HERE;
							struct memcpy_expression *spef = exp;
							if(spef->src_right) {
								invoke_mass_pickup(0, bytes_written);
								size_t where = flatten(spef->dest);
								invoke_mass_dropoff(
									bytes_written,
									home - (where + bytes_written));
							} else {
								size_t where = flatten(spef->src);
								invoke_mass_pickup(
									home - (where + bytes_written),
									bytes_written);
								invoke_mass_dropoff(bytes_written, 0);
							}
							write_name(i, iid, true);
							write(afd, ": * => *, N, ", 13);
							if(i + 1 < end) {
								write_name(i + 1, 0, false);
								write(afd, ";\n", 2);
							}
							break;
						}
						case ek_array_index: {
							HERE;
							struct array_index_expression *spef = exp;
							write_name(i, iid, true);
							write(afd, ": * => ", 7);
							write_name(i, iid + 1, true);
							write(afd, ", L, ", 5);
							write(afd, "compiler_copier", 15);
							write(afd, ";\n", 2);
							iid++;
							write(afd, "extern ", 7);
							write_name(i, iid, true);
							write(afd, ": * => ", 7);
							write_name(i, iid + 3, true);
							write(afd, ", R, ", 5);
							write_name(i, iid + 1, true);
							write(afd, ";\n", 2);
							iid++;
							write(afd, "extern ", 7);
							write_name(i, iid, true);
							write(afd, ": * => *", 8);
							write(afd, ", R, ", 5);
							write_name(i, iid + 1, true);
							write(afd, ";\n", 2);
							iid++;
							write_name(i, iid, true);
							write(afd, ": * => ", 7);
							string_flatten_c_char(afd, &uwrite_char_pusher,
												  bytes_written, true);
							write(afd, ", L, ", 5);
							// write_name(i, iid + 1, true);
							write(afd, "mass_pickup", 11);
							write(afd, ";\n", 2);
							iid++;
							write(afd, "extern ", 7);
							invoke_mass_dropoff(
								bytes_written,
								spef->rarray->value_type->det->size -
									bytes_written + 1);
							verpv(spef->rarray->value_type->det->size -
								  bytes_written + 1);
							write_name(i, iid, true);
							write(afd, ": * => *, N, ", 13);
							if(i + 1 < end) {
								write_name(i + 1, 0, false);
								write(afd, ";\n", 2);
							}
							break;
						}
						case ek_function: {
							HERE;
							struct function_expression *spef = exp;
							invoke_mass_pickup(0, spef->consumed);
							move_to(i, pos + 7, true);
							write_name(i, iid, true);
							write(afd, ": * => ", 7);
							write_name(i, iid + 1, true);
							write(afd, ", R, ", 5);
							write(afd, spef->name, strlen(spef->name));
							write(afd, ";\n", 2);
							iid++;
							write(afd, "extern ", 7);
							move_to(i, pos - 7, true);
							invoke_mass_dropoff(bytes_written,
												spef->consumed - bytes_written);
							write_name(i, iid, true);
							write(afd, ": * => *, N, ", 13);
							if(i + 1 < end) {
								write_name(i + 1, 0, false);
								write(afd, ";\n", 2);
							}
							break;
						}
						case ek_assignment: {
							HERE;
							struct assignment_expression *spef = exp;
							invoke_mass_pickup(1, bytes_written);
							write_name(i, iid, true);
							write(afd, ": * => ", 7);
							write_name(i, iid + 1, true);
							write(afd, ", L, ", 5);
							write(afd, "compiler_copier", 15);
							write(afd, ";\n", 2);
							iid++;
							write(afd, "extern ", 7);
							write_name(i, iid, true);
							write(afd, ": * => ", 7);
							write_name(i, iid + 2, true);
							write(afd, ", R, ", 5);
							write_name(i, iid + 1, true);
							write(afd, ";\n", 2);
							iid++;
							write_name(i, iid, true);
							write(afd, ": * => ", 7);
							string_flatten_c_char(afd, &uwrite_char_pusher,
												  bytes_written, true);
							write(afd, ", N, ", 5);
							// write(afd, "halt", 4);
							write(afd, "mass_dropoff", 12);
							write(afd, ";\n", 2);
							iid++;
							// write(afd, "extern ", 7); BREAKPOINT("A");
							write(afd, "extern ", 7);
							write_name(i, iid, true);
							write(afd, ": * => *, N, ", 13);
							if(i + 1 < end) {
								write_name(i + 1, 0, false);
								write(afd, ";\n", 2);
							}
							break;
						}
						default: { TODO; }
					}
					break;
				}
				case sk_goto: {
					HERE;
					struct goto_statement *gs = sta;
					write_name(i, iid, true);
					write(afd, ": * => *, N, ", 13);
					assert(gs->instruction_index != -1,
						   "goto label never declared!");
					write_name(gs->instruction_index, 0, true);
					write(afd, ";\n", 2);
					bi_returned = false;
					break;
				}
				case sk_if: {
					HERE;
					struct if_statement *spef = sta;
					bool else_exists =
						spef->false_case_index != spef->end_false_case_index;
					size_t old_i = i;
					i++;
					assert(i < spef->false_case_index,
						   "'true' case in if statement cannot be empty!");
					write_name(old_i, iid, true);
					write(afd, ":\n\t* => *, N, ", 14);
					write_name(i, 0, false);
					write(afd, "\n\t'\\x00' => *, N, ", 18);
					if(else_exists) {
						write_name(spef->false_case_index, 0, false);
					} else {
						write_name(old_i, iid + 1, true);
					}
					write(afd, "\n;\n", 3);
					iid++;
					size_t oldpos = pos;
					verpv(oldpos);
					HERE;
					verpv(spef->false_case_index);
					verpv(spef->end_false_case_index);
					if(build_instructions(spef->false_case_index)) {
						write_name(old_i, -1, true);
						write(afd, ";\n", 2);
					}
					HERE;
					size_t true_case_pos = pos;
					verpv(true_case_pos);
					pos = oldpos;
					HERE;
					if(else_exists) {
						if(build_instructions(spef->end_false_case_index)) {
							if(pos == true_case_pos) {
								write_name(old_i, -1, true);
								write(afd, ";\n", 2);
							} else {
								write_name(old_i, iid, true);
								write(afd, ";\n", 2);
								goto move_to_true_case;
							}
						}
					} else {
					move_to_true_case : {
						move_to(old_i, true_case_pos, true);
						write_name(old_i, iid, true);
						write(afd, ": * => *, N, ", 13);
						write_name(old_i, -1, true);
						write(afd, ";\n", 2);
					}
					}
					assert(pos == true_case_pos, "!!");
					write_name(old_i, -1, true);
					write(afd, ": * => *, N, ", 13);
					if(i < end) {
						write_name(i, 0, false);
						write(afd, ";\n", 2);
					}
					i--;
					break;
				}
				case sk_return: {
					write_name(i, iid, true);
					// write(afd, ": * => 'R', N, halt;\n", 21);
					write(afd, ": * => *, L, switch;\n", 21);
					bi_returned = false;
					break;
				}
				default: { NOPE; }
			}
			free(sta);
		}
		return bi_returned;
	}
	assert(build_instructions(wstatements.n) == false,
		   "The last statement was not 'goto' or 'return'!'");
}
#endif
