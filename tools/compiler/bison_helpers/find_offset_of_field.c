/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"~/deps/array/struct.h",
				"~/deps/misc/assert.h",
				"../enums/var_type_type.h",
				"../structs/var_details/base.h",
				"../structs/var_details/struct.h",
				"../structs/var_declare.h",
				"../structs/var_type.h",
				"find_offset_of_field.c",
		}
/*/
#ifndef find_offset_of_field_c
#define find_offset_of_field_c
struct find_offset_of_field_returned {
	size_t index;
	byte offset;
};
struct find_offset_of_field_returned find_offset_of_field(struct var_type *vt,
														  char field[256]);
#else
#include <string.h>
struct find_offset_of_field_returned find_offset_of_field(struct var_type *vt,
														  char field[256]) {
	ENTER;
	verpv(vt);
	verpv(vt->det);
	assert(vt->det->type == vtt_struct, "!!");
	HERE;
	struct struct_var_details *spef = vt->det;
	verpv(spef);
	HERE;
	struct find_offset_of_field_returned returned = {0, 0};
	size_t offset = 0;
	size_t i, n = spef->fields.n;
	verpv(n);
	for(i = 0; i < n; i++) {
		struct var_declare *ele = arrayp_index(spef->fields, i);
		verpv(ele);
		verpvs(ele->name);
		if(strcmp(ele->name, field)) {
			returned.offset += ele->type->det->size;
		} else {
			returned.index = i;
			goto done;
		}
	}
	assert(0, "struct '%s' does not have field named '%s'!", vt->name, field);
done : {}
	EXIT;
	return returned;
}
#endif
