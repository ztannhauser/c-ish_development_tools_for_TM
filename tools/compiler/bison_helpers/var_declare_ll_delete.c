/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"../structs/var_declare.h",
				"../structs/var_declare_ll.h",
				"var_declare_ll_delete.c",
		}
/*/
#ifndef var_declare_ll_delete_c
#define var_declare_ll_delete_c
void var_declare_ll_delete(struct var_declare_ll *ll);
#else
void var_declare_ll_delete(struct var_declare_ll *ll) {
	ENTER;
	while(ll) {
		struct var_declare_ll *old_ll = ll;
		ll = ll->next;
		free(old_ll);
	}
	EXIT;
}
#endif
