/*/
		deps =
		{
				"../globals/bison_error.c",
				"yyerror.c",
		}
/*/
#ifndef yyerror_c
#define yyerror_c
void yyerror(const char *s);
#else
#include <stdio.h>
void yyerror(const char *s) {
	fprintf(stderr, "Parse error: %s\n", s);
	fprintf(stderr, "Line Number: %lu\n", line_number);
	exit(1);
}
#endif
