/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/defines.h",
				"~/deps/debug.c",
				"~/deps/array/struct.h",
				"~/deps/array/bsearch/struct.h",
				"~/deps/array/bsearch/singular.c",
				"~/deps/misc/assert.h",
				"../enums/ekind.h",
				"../enums/skind.h",
				"../structs/location.h",
				"../structs/prototype.h",
				"../structs/statement/base.h",
				"../structs/expression/base.h",
				"../structs/expression/constant.h",
				"../structs/expression_ll.h",
				"../compares/prototype_and_string.c",
				"../globals/types.c",
				"../globals/prototypes.c",
				"push_new/constant_expression.c",
				"push_new/function_expression.c",
				"add_lexpression_trunk.c",
		}
/*/
#ifndef add_lexpression_trunk_c
#define add_lexpression_trunk_c
struct expression *add_lexpression_trunk(struct expression *lexp);
#else
struct expression *add_lexpression_trunk(struct expression *lexp) {
	ENTER;
	assert(lexp->lvalue_type, "!!");
	assert(lexp->value_type == byte_type, "bytes are the only valid index!");
	struct expression *returned;
	if(lexp->kind == ek_constant) {
		struct constant_expression *spef = lexp;
		// '1' because of the lexpression, '3' because that is the
		// offset of the walker
		spef->value = -(spef->value) + 1 + 3;
		spef->add_stack_size = true;
		returned = lexp;
		NOPE;  // should have been replaced with a memcpy statement
	} else {
		struct array_search_returned ab;
		ab = array_bsearch_singular(&prototypes, "byte_negate",
									compare_prototype_and_string);
		assert(ab.exists,
			   "implict function call without a prototype! (byte_negate)");
		struct expression_ll *params0 = malloc(sizeof(struct expression_ll));
		params0->express = lexp;
		params0->next = NULL;
		struct function_expression *ex1 = push_new_function_expression(
			array_index(prototypes, ab.index, struct prototype *), params0,
			NULL);
		HERE;
		struct constant_expression *const0 =
			push_new_constant_expression(1 + 3, true, NULL);
		ab = array_bsearch_singular(&prototypes, "byte_add",
									compare_prototype_and_string);
		assert(ab.exists,
			   "implict function call without a prototype! (byte_add)");
		struct expression_ll *params1 = malloc(sizeof(struct expression_ll));
		struct expression_ll *params2 = malloc(sizeof(struct expression_ll));
		params1->express = ex1;
		params1->next = params2;
		params2->express = const0;
		params2->next = NULL;
		returned = push_new_function_expression(
			array_index(prototypes, ab.index, struct prototype *), params1,
			lexp->lvalue_type);
	}
	EXIT;
	return returned;
}
#endif
