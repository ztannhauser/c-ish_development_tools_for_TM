/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"~/deps/misc/assert.h",
				"../structs/var_type.h",
				"../structs/var_declare.h",
				"../structs/var_declare_ll.h",
				"../structs/prototype.h",
				"check_prototype_matches.c",
		}
/*/
#ifndef check_prototype_matches_c
#define check_prototype_matches_c
void check_prototype_matches(struct prototype *found_prototype,
							 struct var_declare_ll *b);
#else
void check_prototype_matches(struct prototype *found_prototype,
							 struct var_declare_ll *b) {
#define ERROR "Incorrect number of arguments against prototype!"
	ENTER;
	struct var_declare_ll *a = found_prototype->params;
	verpv(a);
	verpv(b);
	while(a) {
		assert(b, ERROR);
		verpvs(a->declare.name);
		verpvs(b->declare.name);
		verpv(a->declare.type);
		verpv(b->declare.type);
		assert(a->declare.type == b->declare.type, ERROR);
		a = a->next;
		b = b->next;
	}
	assert(b == NULL, ERROR);
	EXIT;
}
#endif
