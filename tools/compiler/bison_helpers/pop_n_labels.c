/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"~/deps/array/struct.h",
				"~/deps/array/mass_pop_n.c",
				"../structs/stack.h",
				"../globals/working.c",
				"pop_n_labels.c",
		}
/*/
#ifndef pop_n_labels_c
#define pop_n_labels_c
void pop_n_labels(unsigned int n);
#else
void pop_n_labels(unsigned int n) {
	array_mass_pop_n(&(wstack.labels), n);
}
#endif
