/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/push_n.c",
		"~/deps/misc/assert.h",
		"../enums/var_type_type.h",
		"../enums/ekind.h",
		"../enums/skind.h",
		"../structs/location.h",
		"../structs/var_details/base.h",
		"../structs/var_type.h",
		"../structs/statement/base.h",
		"../structs/expression/base.h",
		"../structs/expression/constant.h",
		"../structs/expression/assignment.h",
		"../structs/expression/memcpy.h",
		"../structs/expression/function.h",
		"../structs/expression/array_index.h",
		"../structs/expression_ll.h",
		"../globals/working.c",
		"push_whole_tree.c",
	}
	compiler_flags =
	{
		"-DNO_DEBUG"
	}
/*/
#ifndef push_whole_tree_c
#define push_whole_tree_c
void push_whole_tree(struct expression *root);
#else
void push_whole_tree(struct expression *root) {
	ENTER;
	verpv(root);
	verpv(root->kind);
#define state (as_state(this))
	void child(struct expression * this) {
		ENTER;
		verpv(this);
		assert(this, "!!");
		verpv(this->kind);
		switch(this->kind) {
			case ek_constant: {
				HERE;
				break;
			}
			case ek_memcpy: {
				HERE;
				struct memcpy_expression *spef = this;
				if(spef->child) {
					child(spef->child);
				}
				break;
			}
			case ek_function: {
				HERE;
				struct function_expression *spef = this;
				struct expression_ll *params = spef->params;
				verpv(params);
				while(params) {
					verpv(params);
					verpv(params->express);
					assert(params->express, "!!");
					child(params->express);
					params = params->next;
				}
				break;
			}
			case ek_assignment: {
				HERE;
				struct assignment_expression *spef = this;
				child(spef->right);
				child(spef->left);
				break;
			}
			case ek_array_index: {
				HERE;
				struct array_index_expression *spef = this;
				child(spef->rarray);
				child(spef->rindex);
				break;
			}
			default: { TODO; }
		}
		verpv(this->parent.kind);
		verpv(this->kind);
		array_push_n(&wstatements, &this);
		verpv(wstatements.n);
		EXIT;
	}
	child(root);
	EXIT;
}
#endif
