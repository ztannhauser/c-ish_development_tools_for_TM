/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/macros.h",
				"~/deps/debug.c",
				"~/deps/memory/clone.c",
				"~/deps/array/struct.h",
				"~/deps/array/bsearch/singular.c",
				"~/deps/array/bsearch/struct.h",
				"~/deps/array/push.c",
				"~/deps/misc/assert.h",
				"../structs/var_type.h",
				"../structs/indentifier_ll.h",
				"../compares/var_type_and_string.c",
				"../globals/types.c",
				"apply_typedef.c",
		}
/*/
#ifndef apply_typedef_c
#define apply_typedef_c
void apply_typedef(struct var_type *type, struct indentifier_ll *ll);
#else
#include <string.h>
void apply_typedef(struct var_type *type, struct indentifier_ll *ll) {
	ENTER;
	verpv(type);
	struct var_type new = {.det = type->det};
	while(ll) {
		verpvs(ll->ident);
		strcpy(new.name, ll->ident);
		struct array_search_returned ab = array_bsearch_singular(
			&types, new.name, compare_var_type_and_string);
		assert(!ab.exists, "variable type '%s' already exists!", new.name);
		array_push(&types, ab.index, varptr(memclone2(new)));
		ll = ll->next;
	}
	EXIT;
}
#endif
