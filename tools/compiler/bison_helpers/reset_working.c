/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/clear.c",
		"~/deps/misc/assert.h",
		"../structs/stack.h",
		"../globals/working.c",
		"reset_working.c",
	}
/*/
#ifndef reset_working_c
#define reset_working_c
void reset_working();
#else
void reset_working() {
	array_clear(&wstack.vars);
	array_clear(&(wstatements));
	max_stack_size = 0;
	current_stack_size = 0;
}
#endif
