/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"~/deps/misc/assert.h",
		"../enums/skind.h",
		"../enums/ekind.h",
		"../structs/location.h",
		"../structs/statement/base.h",
		"../structs/expression/base.h",
		"../structs/expression/memcpy.h",
		"../structs/expression/function.h",
		"../structs/expression/array_index.h",
		"../structs/expression_ll.h",
		"../globals/working.c",
		"shift_expression.c",
	}
/*/
#ifndef shift_expression_c
#define shift_expression_c
void shift_expression(struct expression *express, unsigned short shift);
#else
void shift_expression(struct expression *express, unsigned short shift) {
	ENTER;
	verpv(shift);
	verpv(express);
	verpv(express->kind);
	express->parent.start.offset += shift;
	switch(express->kind) {
		case ek_memcpy: {
			HERE;
			struct memcpy_expression *spef = express;
			if(spef->shift_src) {
				assert(spef->src.in_temp, "!!!");
				spef->src.offset += shift;
				shift_expression(spef->child, shift);
			}
			if(spef->shift_dest) {
				assert(spef->dest.in_temp, "!!!");
				spef->dest.offset += shift;
			}
			break;
		}
		case ek_constant: {
			HERE;
			struct constant_expression *spef = express;
			break;
		}
		case ek_function: {
			struct function_expression *spef = express;
			verprintf("case function\n");
			struct expression_ll *temp = spef->params;
			while(temp) {
				shift_expression(temp->express, shift);
				temp = temp->next;
			}
			break;
		}
		case ek_assignment: {
			TODO;
			break;
		}
		case ek_array_index: {
			struct array_index_expression *spef = express;
			shift_expression(spef->rarray, shift);
			shift_expression(spef->rindex, shift);
			break;
		}
#if 0
		case constant:
		{
			verprintf("case constant\n");
			express->output.offset += shift;
			if(express->output.offset + 1 > max_temp_size)
			{
				max_temp_size = express->output.offset + 1;
			}
			break;
		}
		case variable:
		{
			verprintf("case variable\n");
			express->output.offset += shift;
			if(express->bytes_written + express->output.offset > max_temp_size)
			{
				max_temp_size = express->bytes_written + express->output.offset;
			}
			break;
		}
		case assignment:
		{
				struct assignment_expression *spef = this;
				struct constant_expression *ce =
					spef->left->params->next->express;
				ce->value += shift;
				child(spef->right);
			break;
		}
		case function_call:
		{
		}
#endif
		default: { TODO; }
	}
	EXIT;
}
#endif
