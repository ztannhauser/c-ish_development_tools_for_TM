/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/defines.h",
				"~/deps/debug.c",
				"~/deps/array/struct.h",
				"~/deps/array/bsearch/struct.h",
				"~/deps/misc/assert.h",
				"../structs/stack.h",
				"../structs/var_declare.h",
				"../globals/working.c",
				"find_var_in_stack.c",
		}
/*/
#ifndef find_var_in_stack_c
#define find_var_in_stack_c
struct array_search_returned find_var_in_stack(char *varname);
#else
#include <string.h>
struct array_search_returned find_var_in_stack(char *varname) {
	size_t i;
	if(wstack.vars.n) {
		for(i = wstack.vars.n - 1; i + 1 >= 1; i--) {
			verpv(i);
			if(strcmp(array_rw_index(wstack.vars, i, struct var_declare).name,
					  varname) == 0) {
				return (struct array_search_returned){.exists = true,
													  .index = i};
			}
			HERE;
		}
	}
	return (struct array_search_returned){.exists = false};
}
#endif
