/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/misc/assert.h",
		"../enums/skind.h",
		"../structs/location.h",
		"../structs/statement/base.h",
		"../structs/statement/goto.h",
		"../structs/stack.h",
		"../structs/label.h",
		"../globals/working.c",
		"assign_gotos.c",
	}
/*/
#ifndef assign_gotos_c
#define assign_gotos_c
void assign_gotos(unsigned int n);
#else
#include <string.h>
void assign_gotos(unsigned int n) {
	ENTER;
	if(wstack.labels.n) {
		size_t index = wstatements.n - 1;
		while(n--) {
			struct goto_statement *gs;
			while((gs = array_index(wstatements, index, struct statement *))
					  ->parent.kind != sk_goto) {
				index--;
			}
			if(gs->instruction_index == -1) {
				size_t i = wstack.labels.n - 1;
				for(; i + 1 >= 1; i--) {
					struct label *l = arrayp_index(wstack.labels, i);
					verpvs(l->name);
					verpv(wstatements.n);
					if(strcmp(gs->label, l->name) == 0) {
						verpv(l->instruction_index);
						gs->instruction_index = l->instruction_index;
#if 1
						gs->parent.start =
							array_index(wstatements, l->instruction_index,
										struct statement *)
								->start;
#endif
						goto out;
					}
				}
				NOPE;
			out:;
			}
			index--;
		}
	}
	EXIT;
}
#endif
