/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/defines.h",
				"~/deps/debug.c",
				"~/deps/array/struct.h",
				"~/deps/misc/assert.h",
				"../enums/var_type_type.h",
				"../structs/stack.h",
				"../structs/location.h",
				"../structs/var_details/base.h",
				"../structs/var_type.h",
				"../structs/var_declare.h",
				"../globals/working.c",
				"get_var_location.c",
		}
/*/
#ifndef get_var_location_c
#define get_var_location_c
struct location get_var_location(size_t var_index);
#else
struct location get_var_location(size_t var_index) {
	ENTER;
	verpv(var_index);
	unsigned int returned = 0;
	while(var_index--) {
		verpv(returned);
		returned += array_index(wstack.vars, var_index, struct var_declare)
						.type->det->size;
	}
	verpv(returned);
	EXIT;
	return (struct location){.in_temp = false, .offset = returned};
}
#endif
