/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/pop_n.c",
		"~/deps/misc/assert.h",
		"../enums/var_type_type.h",
		"../structs/stack.h",
		"../structs/var_details/base.h",
		"../structs/var_type.h",
		"../structs/var_declare.h",
		"../globals/working.c",
		"pop_n_variables.c",
	}
/*/
#ifndef pop_n_variables_c
#define pop_n_variables_c
void pop_n_variables(unsigned int n);
#else
void pop_n_variables(unsigned int n) {
	size_t new_n = wstack.vars.n - n;
	while(n--) {
		current_stack_size -=
			array_index(wstack.vars, new_n + n, struct var_declare)
				.type->det->size;
		array_pop_n(&(wstack.vars));
	}
}
#endif
