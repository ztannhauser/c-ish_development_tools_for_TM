/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"../structs/var_declare.h",
				"../structs/var_declare_ll.h",
				"push_new/variable.c",
				"push_params_onto_stack.c",
		}
/*/
#ifndef push_params_onto_stack_c
#define push_params_onto_stack_c
void push_params_onto_stack(struct var_declare_ll *ll);
#else
void push_params_onto_stack(struct var_declare_ll *ll) {
	ENTER;
	while(ll) {
		verpvs(ll->declare.name);
		push_new_variable(ll->declare);
		ll = ll->next;
	}
	EXIT;
}
#endif
