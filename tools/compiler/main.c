/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/misc/assert.c",
		"~/deps/misc/assert.h",
		"globals/flags.c",
		"bison.c",
		"flex.c",
		"inits/all.c",
	}
/*/
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
int main(int n_args, char **args) {
	set_verbose(false);
	#ifndef NO_DEBUG
	verbose_prefix = "C";
	#endif
	ENTER;
	pid_t assembler_pid;
	yyin = init_all(n_args, args, &assembler_pid);
	do {
		yyparse();
	} while(!feof(yyin));
	fclose(yyin);
	if(assembler_fds[PIPE_WRITE] != STD_OUT) {
		close(assembler_fds[PIPE_WRITE]);
		int status;
		assert(waitpid(assembler_pid, &status, 0) >= 0, "waitpid failed!");
		if(!WIFEXITED(status) && (status = WEXITSTATUS(status))) {
			assert(0, "Assembler did not exit happily!");
		}
	}
	EXIT;
	return 0;
}
