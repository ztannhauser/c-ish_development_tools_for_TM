/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/macros.h",
				"~/deps/defines.h",
				"~/deps/debug.c",
				"~/deps/array/struct.h",
				"~/deps/array/new.c",
				"~/deps/array/push_n.c",
				"~/deps/memory/clone.c",
				"../enums/var_type_type.h",
				"../structs/var_details/base.h",
				"../structs/var_type.h",
				"../globals/types.c",
				"types.c",
		}
/*/
#ifndef init_types_c
#define init_types_c
void init_types();
#else
void init_types() {
	ENTER;
	types = array_new(struct var_type *);
	{
		struct var_details det = {.type = vtt_primitive, .size = 1};
		struct var_type type = {.name = "byte",
								.det = memclone(&det, sizeof(det))};
		array_push_n(&types, varptr(byte_type = memclone2(type)));
	}
	EXIT;
}
#endif
