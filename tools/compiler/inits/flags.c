/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/macros.h",
				"~/deps/defines.h",
				"~/deps/debug.c",
				"~/deps/memory/equals.c",
				"~/deps/exec.c",
				"~/deps/array/struct.h",
				"~/deps/array/new.c",
				"~/deps/array/push_n.c",
				"~/deps/array/clear.c",
				"~/deps/array/delete.c",
				"~/deps/misc/assert.h",
				"../globals/flags.c",
				"flags.c",
		}
/*/
#ifndef init_flags_c
#define init_flags_c
FILE *init_flags(int n_args, char **args, pid_t *assembler_pid);
#else
#include <string.h>
#include <unistd.h>
FILE *init_flags(int n_args, char **args, pid_t *assembler_pid) {
	ENTER;
	verpv(n_args);
	char c;
	int i;
	char souce_file[256];
	char output_file[256];
	bool source_file_defined = false;
	bool output_file_defined = false;
	bool run_assembler = true;
	bool run_assembler_with_verbose = false;
	struct array cpp_command = array_new(char *);
	array_push_n(&cpp_command, varptr((char *) "cpp"));
	array_push_n(&cpp_command, varptr((char *) "-undef"));
	for(i = 1; i < n_args; i++) {
		char *arg = args[i];
		if(memequals(arg, "-v", 3)) {
			set_verbose(true);
		} else if(memequals(arg, "-c", 3)) {
			verprintf("this is a gcc flag: ignoring.\n");
		} else if(memequals(arg, "-Werror=", 8)) {
			verprintf("this is a gcc flag: ignoring.\n");
		} else if(memequals(arg, "-o", 3)) {
			HERE;
			i++;
			assert(i < n_args, "Expected argument with the '-o' flag!");
			strcpy(output_file, args[i]);
			output_file_defined = true;
		} else if(memequals(arg, "-print", 7)) {
			run_assembler = false;
		} else if(memequals(arg, "-av", 4)) {
			run_assembler_with_verbose = true;
		} else if(memequals(arg, "-include", 9)) {
			HERE;
			array_push_n(&cpp_command, &(args[i]));
			i++;
			assert(i < n_args, "Expected argument with the '-include' flag!");
			array_push_n(&cpp_command, &(args[i]));
		} else if(memequals(arg, "-D", 2)) {
			HERE;
			array_push_n(&cpp_command, &(args[i]));
			i++;
			assert(i < n_args, "Expected argument with the '-D' flag!");
			array_push_n(&cpp_command, &(args[i]));
		} else {
			HERE;
			assert(!source_file_defined, "Ony one souce file per compile!");
			strcpy(souce_file, arg);
			source_file_defined = true;
		}
	}
	assert(source_file_defined, "You must at least specify a souce file!");
	verpvs(souce_file);
	array_push_n(&cpp_command, varptr((char *) souce_file));
	array_push_n(&cpp_command, varptr((char *) NULL));
	int cpp_fds[2];
	pipe(cpp_fds);
	HERE;
	verpv(cpp_fds[PIPE_WRITE]);
	exec(cpp_command.data, false, ({
			 void callback() {
				 dup2(cpp_fds[PIPE_WRITE], STD_OUT);
				 close(cpp_fds[PIPE_READ]);
			 }
			 callback;
		 }));
	close(cpp_fds[PIPE_WRITE]);
	FILE *returned = fdopen(cpp_fds[PIPE_READ], "r");
	verpv(returned);
	array_clear(&cpp_command);
#define assembler_command cpp_command
	if(run_assembler) {
		assert(output_file_defined, "You must (also) specify an output file!");
		verpvs(output_file);
		pipe(assembler_fds);
		array_push_n(&assembler_command, varptr((char *) "/tmp/assembler"));
		if(run_assembler_with_verbose) {
			array_push_n(&assembler_command, varptr((char *) "-v"));
		}
		array_push_n(&assembler_command, varptr((char *) "-o"));
		array_push_n(&assembler_command, varptr((char *) output_file));
		array_push_n(&assembler_command, varptr((char *) NULL));
		*assembler_pid = exec(assembler_command.data, false, ({
								  void callback() {
									  dup2(assembler_fds[PIPE_READ], STD_IN);
									  close(assembler_fds[PIPE_WRITE]);
								  }
								  callback;
							  }));
		close(assembler_fds[PIPE_READ]);
	} else {
		assembler_fds[PIPE_WRITE] = STD_OUT;
	}
	array_delete(&assembler_command);
	EXIT;
	return returned;
}
#endif
