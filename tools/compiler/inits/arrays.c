/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/new.c",
		"../structs/var_type.h",
		"../globals/arrays.c",
		"arrays.c",
	}
/*/
#ifndef init_arrays_c
#define init_arrays_c
void init_arrays();
#else
void init_arrays() {
	ENTER;
	arrays = array_new(struct var_type *);
	EXIT;
}
#endif
