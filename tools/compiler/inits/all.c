/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"types.c",
				"structs.c",
				"arrays.c",
				"prototypes.c",
				"working.c",
				"flags.c",
				"bison_error.c",
				"../bison_helpers/reset_working.c",
				"all.c",
		}
/*/
#ifndef init_all_c
#define init_all_c
FILE *init_all(size_t n_args, char **args, pid_t *assembler_pid);
#else
FILE *init_all(size_t n_args, char **args, pid_t *assembler_pid) {
	ENTER;
	init_types();
	init_structs();
	init_arrays();
	init_prototypes();
	init_working();
	reset_working();
	init_bison_error();
	EXIT;
	return init_flags(n_args, args, assembler_pid);
}
#endif
