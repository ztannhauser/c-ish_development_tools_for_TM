/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"~/deps/array/struct.h",
				"~/deps/array/new.c",
				"../globals/prototypes.c",
				"prototypes.c",
		}
/*/
#ifndef init_prototypes_c
#define init_prototypes_c
void init_prototypes();
#else
void init_prototypes() {
	ENTER;
	prototypes = array_new(struct prototype *);
	EXIT;
}
#endif
