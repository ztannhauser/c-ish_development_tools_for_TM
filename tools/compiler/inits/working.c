/*/
		deps =
		{
				"~/deps/typedefs.h",
				"~/deps/debug.c",
				"~/deps/array/struct.h",
				"~/deps/array/new.c",
				"../structs/stack.h",
				"../structs/var_declare.h",
				"../structs/label.h",
				"../globals/working.c",
				"working.c",
		}
/*/
#ifndef init_working_c
#define init_working_c
void init_working();
#else
void init_working() {
	ENTER;
	wstatements = array_new(struct statement *);
	wstack.vars = array_new(struct var_declare);
	wstack.labels = array_new(struct label);
	verpv(wstack.vars.data);
	EXIT;
}
#endif
