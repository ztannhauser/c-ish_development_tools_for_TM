/*/
		deps =
		{
				"../globals/bison_error.c",
				"bison_error.c",
		}
/*/
#ifndef init_bison_error_c
#define init_bison_error_c
void init_bison_error();
#else
void init_bison_error() {
	line_number = 1;
}
#endif
