/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/new.c",
		"../structs/var_type.h",
		"../globals/structs.c",
		"structs.c",
	}
/*/
#ifndef init_structs_c
#define init_structs_c
void init_structs();
#else
void init_structs() {
	ENTER;
	structs = array_new(struct var_type *);
	EXIT;
}
#endif
