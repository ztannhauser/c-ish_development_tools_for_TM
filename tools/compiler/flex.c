/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/bsearch/struct.h",
		"~/deps/array/bsearch/singular.c",
		"~/deps/string/file_cursor/struct.h",
		"~/deps/string/char_popper/struct.h",
		"~/deps/string/char_popper/implementations/file_cursor.c",
		"~/deps/string/char_popper/new.c",
		"~/deps/string/conversion/unflatten/c_char.c",
		"~/deps/string/conversion/unflatten/hex.c",
		"~/deps/misc/assert.h",
		"enums/skind.h",
		"structs/location.h",
		"structs/var_declare.h",
		"structs/prototype.h",
		"structs/statement_data.h",
		"structs/statement/base.h",
		"structs/statement/if.h",
		"structs/if_statement_wtcr.h",
		"globals/bison_error.c",
		"globals/types.c",
		"globals/structs.c",
		"globals/prototypes.c",
		"compares/prototype_and_string.c",
		"compares/var_type_and_string.c",
		"bison_helpers/find_var_in_stack.c",
		"~/tmp/flex.h",
	}
/*/
#include <stdio.h>
extern FILE *yyin;
