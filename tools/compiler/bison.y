%{

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern int yylex();
extern int yyparse();
extern FILE* yyin;

%}

%union
{
	byte constant_value;
	struct statement_data state_data;
	unsigned int gotos_declared;
	struct var_type* found_type;
	struct expression* express;
	char text[256];
	struct var_declare_ll* declare_ll;
	struct indentifier_ll* indent_ll;
	struct var_declare declare;
	struct prototype* found_prototype;
	size_t found_variable;
	struct expression_ll* express_ll;
	struct if_statement_data* if_data;
	struct if_statement_with_true_case_returned ifwtcr;
}

%token K_OPEN_PAREN K_CLOSE_PAREN K_OPEN_BRACKET K_CLOSE_BRACKET K_OPEN_CURLY K_CLOSE_CURLY
%token K_SEMI K_COMMA K_DOT
%token K_STRUCT K_TYPEDEF K_RETURN K_IF K_ELSE K_COLON  K_GOTO

%right K_EQUALS

%token<constant_value> T_CONSTANT
%token<found_type> T_TYPE
%token<text> T_IDENTIFIER
%token<found_prototype> T_PROTOTYPE
%token<found_type> T_STRUCT
%token<found_variable> T_VARIABLE

%type<state_data> statement
%type<state_data> statements
%type<gotos_declared> statements_in_curly
%type<express> rexpression
%type<express> lvalue
%type<express> rvalue
%type<found_prototype> function_declaration
%type<found_type> variable_type
%type<declare> variable_declaration
%type<declare_ll> variable_declarations_comma
%type<declare_ll> variable_declarations_semi
%type<indent_ll> indentifiers
%type<express_ll> subrvalues
%type<if_data> if_statement_header1
%type<ifwtcr> if_statement_header2

%define parse.error verbose

%start global_declarations

%%

variable_type:
	T_TYPE { $$ = $1;}
	| K_STRUCT T_STRUCT { $$ = $2;}
	| variable_type K_OPEN_BRACKET T_CONSTANT K_CLOSE_BRACKET
	{
		assert($1, "!!");
		
		char type_name[256];
		size_t strlen_type_name = sprintf(type_name, "%s[%i]", $1->name, $3);
		
		struct array_search_returned ab =
			array_bsearch_singular(&arrays,
				type_name, compare_var_type_and_string);
		if(ab.exists)
		{
			$$ = array_index(arrays, ab.index, struct var_type*);
		}
		else
		{
			struct var_type* thisthis = $$ =
				malloc(sizeof(struct var_type));
			array_push_n(&arrays, &thisthis);
			struct array_var_details* this =
				malloc(sizeof(struct array_var_details));
			memcpy(thisthis->name, type_name, strlen_type_name + 1);
			thisthis->det = this;
			this->parent.type = vtt_array;
			this->element_type = $1;
			this->parent.size = ($1)->det->size * $3;
		}
	}
;

indentifiers:
	{
		$$ = NULL;
	}
	| T_IDENTIFIER indentifiers
	{
		struct indentifier_ll* this = $$ = malloc(sizeof(struct indentifier_ll));
		strcpy(this->ident, $1);
		this->next = $2;
	}
;

typedef_declaration:
	K_TYPEDEF variable_type indentifiers
	{
		apply_typedef($2, $3);
	}
;

variable_declaration:
	variable_type T_IDENTIFIER
	{
		strcpy($$.name, $2);
		$$.type = $1;
	}
;

variable_declarations_comma:
	{
		$$ = NULL;
	}
	| variable_declaration
	{
		struct var_declare_ll* this = $$ = malloc(sizeof(struct var_declare_ll));
		this->declare = $1;
		this->next = NULL;
	}
	| variable_declaration K_COMMA variable_declarations_comma
	{
		struct var_declare_ll* this = $$ = malloc(sizeof(struct var_declare_ll));
		this->declare = $1;
		this->next = $3;
	}
;

variable_declarations_semi:
	{
		$$ = NULL;
	}
	| variable_declaration K_SEMI variable_declarations_semi
	{
		struct var_declare_ll* this = $$ = malloc(sizeof(struct var_declare_ll));
		this->declare = $1;
		this->next = $3;
	}
;

struct_declaration:
	K_STRUCT T_IDENTIFIER K_OPEN_CURLY variable_declarations_semi K_CLOSE_CURLY
	{
		push_new_struct($2, $4);
	}
;

prototype_declaration:
	variable_type T_IDENTIFIER K_OPEN_PAREN variable_declarations_comma K_CLOSE_PAREN
	{
		push_new_prototype($2, $1, $4, true);
	}
;

function_declaration:
	variable_type T_PROTOTYPE K_OPEN_PAREN variable_declarations_comma K_CLOSE_PAREN
	{
		check_prototype_matches($2, $4);
		push_params_onto_stack($4);
		var_declare_ll_delete($4);
		$$ = $2;
	}
	| variable_type T_IDENTIFIER K_OPEN_PAREN variable_declarations_comma K_CLOSE_PAREN
	{
		push_params_onto_stack($4);
		$$ = push_new_prototype($2, $1, $4, false);
	}
;

subrvalues:
	{
		$$ = NULL;
	}
	| rvalue
	{
		struct expression_ll* this = $$ = malloc(sizeof(struct expression_ll));
		this->express = $1;
		this->next = NULL;
	}
	| rvalue K_COMMA subrvalues
	{
		struct expression_ll* this = $$ = malloc(sizeof(struct expression_ll));
		this->express = $1;
		this->next = $3;
	}
;

rvalue:
	T_CONSTANT
	{
		$$ = push_new_constant_expression($1, false, NULL);
	}
	| T_VARIABLE // variables
	{
		$$ = push_new_memcpy_expression
		(
			(struct location)
			{
				.in_temp = true,
				.offset = 0
			}, true,
			get_var_location($1), false,
			array_index(wstack.vars, $1, struct var_declare).type,
			NULL
		);
	}
	| rvalue K_OPEN_BRACKET rvalue K_CLOSE_BRACKET
	{
		assert($1->value_type->det->type == vtt_array, "not an array!");
		if($3->kind == ek_constant)
		{
			struct array_var_details* avd = $1->value_type->det;
			$$ = push_new_memcpy_expression(
				(struct location)
				{
					.in_temp = true,
					.offset = 0
				},
				true,
				(struct location)
				{
					.in_temp = true,
					.offset =
					(
						((struct constant_expression*) $3)->value *
						avd->element_type->det->size
					)
				},
				true,
				avd->element_type,
				$1
			);
		}
		else
		{
			$$ = push_new_array_index_expression($1, $3);
		}
	}
	| rvalue K_DOT T_IDENTIFIER
	{
		$$ = push_new_field_expression($1, $3);
	}
	| rexpression
	{
		$$ = $1;
	}

rexpression:
	lvalue K_EQUALS rvalue // assignment
	{
		if($1->kind == ek_constant)
		{
			$$ = push_new_memcpy_expression(
				(struct location)
				{
					.in_temp = false,
					.offset = ((struct constant_expression*) $1)->value,
				},
				false,
				(struct location)
				{
					.in_temp = true,
					.offset = 0
				},
				true,
				$3->value_type,
				$3
			);
		}
		else
		{
			$$ = push_new_assignment_expression(add_lexpression_trunk($1), $3);
		}
	}
	| T_PROTOTYPE K_OPEN_PAREN subrvalues K_CLOSE_PAREN // call by name
	{
		$$ = push_new_function_expression($1, $3, NULL);
	}
;

lvalue:
	T_VARIABLE
	{
		verprintf("lexpression reference to '%s'\n",
			array_index(wstack.vars, $1, struct var_declare).name);
		$$ = push_new_constant_expression
		(
			get_var_location($1).offset, false,
			array_index(wstack.vars, $1, struct var_declare).type
		);
	}
	| lvalue K_OPEN_BRACKET rvalue K_CLOSE_BRACKET
	{
		$$ = push_new_lexpress_index($1, $3);
	}
	| lvalue K_DOT T_IDENTIFIER
	{
		$$ = push_new_lexpress_field($1, $3);
	}
;

if_statement_header1:
	K_IF K_OPEN_PAREN rvalue K_CLOSE_PAREN
	{
		assert($3->value_type->det == byte_type->det,
			"If conditionals only accept bytes");
		push_whole_tree($3);
		struct if_statement* this = $$ =
			malloc(sizeof(struct if_statement));
		array_push_n(&wstatements, &this);
		this->parent.kind = sk_if;
		this->parent.start.in_temp = true;
		this->parent.start.offset = 0;
	}
;

if_statement_header2:
	if_statement_header1 statement K_ELSE
	{
		struct if_statement* this = $$.a = $1;
		this->false_case_index = wstatements.n;
		$$.b = $2;
	}
;

statement:
	K_SEMI
	{
		$$ = (struct statement_data) {0, 0, 0};
	}
	| variable_declaration K_SEMI
	{
		push_new_variable($1);
		$$ = (struct statement_data) {.variables_declared = 1, 0};
	}
	| statements_in_curly
	{
		$$ = (struct statement_data) {0, 0, .gotos_declared = $1};
	}
	| rexpression K_SEMI
	{
		push_whole_tree($1);
		$$ = (struct statement_data) {0, 0, 0};
	}
	| K_RETURN rvalue K_SEMI
	{
		if
		(
			($2->kind != ek_memcpy)
			||
			(
				{
					struct location src = 
						((struct memcpy_expression*)($2))
						->
						src;
					src.in_temp || src.offset;
				}
			)
		)
		{
			push_whole_tree(push_new_memcpy_expression
			(
				(struct location)
				{
					.in_temp = false,
					.offset = 0
				},
				false,
				(struct location)
				{
					.in_temp = true,
					.offset = 0
				},
				true,
				($2)->value_type,
				$2
			));
		}
		push_new_return_statement();
		$$ = (struct statement_data) {0, 0, 0};

	}
	| K_RETURN K_SEMI
	{
		push_new_return_statement();
		$$ = (struct statement_data) {0, 0, 0};
	}
	| T_IDENTIFIER K_COLON
	{
		struct label new;
		new.instruction_index = wstatements.n;
		strcpy(new.name, $1);
		array_push_n(&(wstack.labels), &new);
		$$ = (struct statement_data) {0, .labels_declared = 1, 0};
	}
	| K_GOTO T_IDENTIFIER K_SEMI
	{
		push_new_goto_statement($2);
		$$ = (struct statement_data) {0, 0, .gotos_declared = 1};
	}
	| if_statement_header2 statement
	{
		struct if_statement* this = $1.a;
		this->end_false_case_index = wstatements.n;
		$$ = (struct statement_data)
		{
			.variables_declared = $1.b.variables_declared + $2.variables_declared,
			.labels_declared = $1.b.labels_declared + $2.labels_declared,
			.gotos_declared = $1.b.gotos_declared + $2.gotos_declared,
		};
	}
;

statements:
	{
		$$ = (struct statement_data) {0, 0, 0};
	}
	| statement statements
	{
		$$.variables_declared = $1.variables_declared + $2.variables_declared;
		$$.labels_declared = $1.labels_declared + $2.labels_declared;
		$$.gotos_declared = $1.gotos_declared + $2.gotos_declared;
	}
;

statements_in_curly:
	K_OPEN_CURLY statements K_CLOSE_CURLY
	{
		pop_n_variables($2.variables_declared);
		assign_gotos($2.gotos_declared);
		pop_n_labels($2.labels_declared);
		$$ = $2.gotos_declared;
	}
;

function:
	function_declaration statements_in_curly
	{
		flush_function($1);
		reset_working();
	}
;

global_declaration:
	typedef_declaration K_SEMI
	| struct_declaration K_SEMI
    | prototype_declaration K_SEMI
    | function
;

global_declarations: 
	   | global_declarations global_declaration
;

%%











