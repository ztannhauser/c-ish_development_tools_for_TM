%option noyywrap

%{
#include <stdio.h>

#define YY_DECL int yylex()

#include "/tmp/bison.hh"

%}

%%

"#".*\n  ; // ignore comments

\n  {line_number++;}

[ \t\r]	; // ignore all whitespace

";" {return K_SEMI;}
"{" { return K_OPEN_CURLY;}
"}" { return K_CLOSE_CURLY;}
"(" { return K_OPEN_PAREN;}
")" { return K_CLOSE_PAREN;}
"[" { return K_OPEN_BRACKET;}
"]" { return K_CLOSE_BRACKET;}
"," { return K_COMMA;}
"=" { return K_EQUALS;}
"." { return K_DOT;}
":" { return K_COLON;}

"if"      { return K_IF;}
"else"    { return K_ELSE;}
"goto"    { return K_GOTO;}
"struct"  { return K_STRUCT;}
"typedef" { return K_TYPEDEF;}
"return"  { return K_RETURN;}

\'.{1,4}\' {
	verpvs(yytext);
	struct file_cursor fc = {.data = yytext, .size = strlen(yytext)};
	struct char_popper cp = char_popper_new(&fc, file_cursor_pop, NULL);
	yylval.constant_value = string_unflatten_c_char(&cp, false);
	verpv(yylval.constant_value);
	return T_CONSTANT;
}

"0x"[0-9A-F]{1,2} {
	verpvs(yytext);
	struct file_cursor fc = {.data = yytext, .size = strlen(yytext)};
	struct char_popper cp = char_popper_new(&fc, file_cursor_pop, NULL);
	yylval.constant_value = string_unflatten_hex(&cp, true);
	verpv(yylval.constant_value);
	return T_CONSTANT;
}

[0-9]{1,3} {
	verpvs(yytext);
	yylval.constant_value = atoi(yytext);
	return T_CONSTANT;
}

[a-zA-Z_][a-zA-Z0-9_]*  {
	ENTER;
	verpvs(yytext);
	struct array_search_returned ab;
	ab = find_var_in_stack(yytext);
	if(ab.exists)
	{
		yylval.found_variable = ab.index;
		verprintf("varible!\n");
		EXIT;
		return T_VARIABLE;
	}
	ab = array_bsearch_singular(&types, yytext, compare_var_type_and_string);
	if(ab.exists)
	{
		yylval.found_type = array_index(types, ab.index, struct var_type*);
		verprintf("type!\n");
		EXIT;
		return T_TYPE;
	}
	ab = array_bsearch_singular(&structs, yytext, compare_var_type_and_string);
	if(ab.exists)
	{
		yylval.found_type = array_index(structs, ab.index, struct var_type*);
		verprintf("struct!\n");
		EXIT;
		return T_STRUCT;
	}
	ab = array_bsearch_singular(&prototypes, yytext, compare_prototype_and_string);
	if(ab.exists)
	{
		yylval.found_prototype = array_index(prototypes, ab.index, struct prototype*);
		verprintf("prototype!\n");
		EXIT;
		return T_PROTOTYPE;
	}
	strcpy(yylval.text, yytext);
	verprintf("ident!\n");
	EXIT;
	return T_IDENTIFIER;
}

%%
