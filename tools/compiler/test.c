struct bb {
	byte b1;
};
byte func(byte b1, byte b2, byte b3);
byte byte_add(byte b1, byte b2);
byte main() {
	byte b;
	func(3, b, b);
label:
	if(b) {
		func(5, 6, 7);
		goto label;
	} else {
		goto label;
	}
	{ b = 8; }
	{
		struct bb test_struct;
		test_struct.b1 = 4;
		b = test_struct.b1;
	}
	{
		struct bb[2] array;
		array[1].b1 = 6;
		b = array[4].b1;
	}
	{
		struct bb['\x3'][0x2] array;
		array[2][4].b1 = 6;
		b = array[2][4].b1;
	}
	return b;
}
