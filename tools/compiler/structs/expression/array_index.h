/*/
/*/
struct array_index_expression {
	struct expression parent;
	struct expression *rarray;
	struct expression *rindex;
};
