/*/
/*/
struct memcpy_expression {
	struct expression parent;
	struct location src, dest;
	bool src_right;
	bool shift_src, shift_dest;
	struct expression *child;
};
#define as_memexp(this) ((struct memcpy_expression *) (this))
