/*/
/*/
struct expression {
	struct statement parent;
	enum ekind kind;
	struct var_type *value_type;
	struct var_type *lvalue_type;  // is NULL if rexpression
};
#define as_exp(this) ((struct expression *) (this))
