/*/
/*/
struct prototype {
	char name[256];
	struct var_type *return_type;
	struct var_declare_ll *params;
};
