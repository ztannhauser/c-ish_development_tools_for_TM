/*/
/*/
struct statement {
	enum skind kind;
	struct location start;
};
#define as_state(this) ((struct statement *) (this))
