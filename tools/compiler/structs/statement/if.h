/*/
/*/
struct if_statement {
	struct statement parent;
	size_t false_case_index;
	size_t end_false_case_index;
};
