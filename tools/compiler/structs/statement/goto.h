/*/
/*/
struct goto_statement {
	struct statement parent;
	char label[256];
	size_t instruction_index;
};
