/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/array/struct.h",
		"../structs/stack.h",
		"working.c",
	}
/*/
#ifndef working_c
#define working_c
extern struct stack wstack;
extern struct array wstatements;
extern size_t max_stack_size;
extern size_t current_stack_size;
#else
struct stack wstack;
struct array wstatements;
size_t max_stack_size;
size_t current_stack_size;
#endif
