/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/array/struct.h",
		"arrays.c",
	}
/*/
#ifndef arrays_c
#define arrays_c
extern struct array arrays;
#else
struct array arrays;
#endif
