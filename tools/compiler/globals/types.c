/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/array/struct.h",
		"types.c",
	}
/*/
#ifndef types_c
#define types_c
extern struct var_type *byte_type;
extern struct array types;
#else
struct var_type *byte_type;
struct array types;
#endif
