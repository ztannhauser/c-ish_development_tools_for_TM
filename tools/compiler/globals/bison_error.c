/*/
	deps =
	{
		"bison_error.c",
	}
/*/
#ifndef bison_error_c
#define bison_error_c
#include <stdlib.h>
extern size_t line_number;
#else
size_t line_number;
#endif
