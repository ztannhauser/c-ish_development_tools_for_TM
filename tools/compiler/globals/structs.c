/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/array/struct.h",
		"structs.c",
	}
/*/
#ifndef structs_c
#define structs_c
extern struct array structs;
#else
struct array structs;
#endif
