/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/array/struct.h",
		"../../name.h",
		"../../obj_structs.h",
		"turing_states.c",
	}
/*/
#ifndef turing_states_c
#define turing_states_c
extern struct array tstates;
#else
struct array tstates;
#endif
