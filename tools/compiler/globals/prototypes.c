/*/
	deps =
	{
		"~/deps/typedefs.h",
		"~/deps/array/struct.h",
		"prototypes.c",
	}
/*/
#ifndef prototypes_c
#define prototypes_c
extern struct array prototypes;
#else
struct array prototypes;
#endif
