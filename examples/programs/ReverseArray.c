/*/
	deps =
	{
		"~/deps/utils.h",
		"~/deps/add/byte.c",
		"~/deps/subtract/byte.c",
		"~/deps/multiply/byte.c",
		"~/deps/negate/byte.c",
		"~/deps/defines/loops.h",
		"~/deps/syscalls/print_tape.h",
	}
/*/

#define ARRAY_SIZE 8
#define HALF_ARRAY_SIZE 4
#define ARRAY_SIZE_MINUS_ONE 7

byte[ARRAY_SIZE] reverse(byte[ARRAY_SIZE] in) {
	byte i;
	byte j;
	for(i = 0, byte_subtract(i, HALF_ARRAY_SIZE), i = byte_add(i, 1), {
			print_tape();
			j = byte_subtract(ARRAY_SIZE_MINUS_ONE, i);
			byte temp;
			temp = in[i];
			in[i] = in[j];
			in[j] = temp;
		})
		return in;
}
byte main() {
	byte ret;
	ret = 0;
	byte[ARRAY_SIZE] a;
	a[0] = 1;
	a[1] = 2;
	a[2] = 3;
	a[3] = 4;
	a[4] = 5;
	a[5] = 6;
	a[6] = 7;
	a[7] = 8;
	a = reverse(a);
	return ret;
}
