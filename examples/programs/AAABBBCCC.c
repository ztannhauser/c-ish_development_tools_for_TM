/*/
	deps =
	{
		"~/deps/utils.h",
		"~/deps/defines/c.h",
		"~/deps/syscalls/IO.h",
		"~/deps/defines/loops.h",
		"~/deps/add/byte.c",
		"~/deps/subtract/byte.c",
		"~/deps/boolean/not.c",
	}
/*/

byte accepted() {
	write('A');
	write('C');
	write('C');
	write('E');
	write('P');
	write('T');
	write('E');
	write('D');
	write('\n');
	return;
}

byte rejected(char num) {
	write('R');
	write('E');
	write('J');
	write('E');
	write('T');
	write('E');
	write('D');
	write(' ');
	write('(');
	write(num);
	write(')');
	write('\n');
	return;
}

/*
	Only accepts strings that are within the
	language:
	a^i b^i c^i, where i >= 0
	This language was picked because only a turing machine
	could process it
*/

byte main() {
	byte a_counter;
	a_counter = 0;
	byte b_counter;
	b_counter = 0;
	byte c_counter;
	c_counter = 0;
	char buffer;
read_again:
	buffer = read();
	if(boolean_not(byte_subtract(buffer, 'A'))) {
		a_counter = byte_add(a_counter, 1);
		goto read_again;
	} else if(boolean_not(byte_subtract(buffer, 'B'))) {
		b_counter = byte_add(b_counter, 1);
		goto read_again;
	} else if(boolean_not(byte_subtract(buffer, 'C'))) {
		c_counter = byte_add(c_counter, 1);
		goto read_again;
	} else if(boolean_not(byte_subtract(buffer, '\n'))) {
		if(byte_subtract(a_counter, b_counter)) {
			rejected('1');
			return 1;
		} else {
			if(byte_subtract(b_counter, c_counter)) {
				rejected('2');
				return 1;
			} else {
				accepted();
			}
		}
	} else {
		rejected('3');
		return 1;
	}
	return 0;
}
