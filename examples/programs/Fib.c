/*/
	deps =
	{
		"~/deps/utils.h",
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"~/deps/add/int.c",
		"~/deps/add/byte.c",
		"~/deps/subtract/byte.c",
		"~/deps/is_nonzero/int.c",
		"~/deps/defines/loops.h",
	}
/*/
byte main() {
	int a;
	int b;
	int c;
	{
		int one;
		assign_int(one, 0, 0, 0, 1);
		b = one;
		c = one;
	}
	byte i;
	for(i = 0, byte_subtract(i, 5), i = byte_add(i, 1), {
			a = b;
			b = c;
			c = int_add(a, b);
		})
		return;
}
