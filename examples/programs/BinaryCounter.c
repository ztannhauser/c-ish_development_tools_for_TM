/*/
	deps =
	{
		"~/deps/utils.h",
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"~/deps/add/bitwise_byte.c",
		"~/deps/is_nonzero/bitwise_byte.c",
		"~/deps/misc/print_byte.c",
		"~/deps/syscalls/IO.h",
		"~/deps/defines/loops.h",
	}
/*/

byte main() {
	struct bitwise_byte current;
	assign_bitwise_byte(current, 0, 0, 0, 0, 0, 0, 0, 1);
	struct bitwise_byte one;
	assign_bitwise_byte(one, 0, 0, 0, 0, 0, 0, 0, 1);
	while(is_nonzero_bitwise_byte(current), {
		print_byte(current);
		write('\n');
		current = bitwise_byte_add(current, one, 0).sum;
	})
		return 0;
}
