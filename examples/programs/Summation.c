/*/
	deps =
	{
		"~/deps/utils.h",
		"~/deps/defines/c.h",
		"~/deps/add/int.c",
		"~/deps/subtract/int.c",
		"~/deps/is_nonzero/int.c",
		"~/deps/defines/loops.h",
	}
/*/
byte main() {
	int one;
	assign_int(one, 0, 0, 0, 1);
	int n;
	assign_int(n, 0, 0, 0, 6);
	int sum;
	assign_int(sum, 0, 0, 0, 0);
	int i;
	for(assign_int(i, 0, 0, 0, 0), int_is_nonzero(int_subtract(i, n)),
		i = int_add(i, one), { sum = int_add(sum, i); })
		return;
}
