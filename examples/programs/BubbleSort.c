/*/
	deps =
	{
		"~/deps/utils.h",
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"~/deps/add/byte.c",
		"~/deps/subtract/bitwise_byte.c",
		"~/deps/subtract/byte.c",
		"~/deps/multiply/byte.c",
		"~/deps/negate/byte.c",
		"~/deps/conversion/from_byte.h",
		"~/deps/negate/bitwise_byte.c",
		"~/deps/defines/loops.h",
		"~/deps/syscalls/print_tape.h",
	}
/*/

#define ARRAY_SIZE 8
#define ARRAY_SIZE_MINUS_ONE 7

byte[ARRAY_SIZE] sort(byte[ARRAY_SIZE] in) {
	bool run_again;
	run_again = true;
	while(run_again, {
		run_again = false;
		byte i;
		byte j;
		for(i = 0, byte_subtract(i, ARRAY_SIZE_MINUS_ONE), i = j, {
				print_tape();
				j = byte_add(i, 1);
				struct bitwise_byte temp_sum;
				temp_sum = bitwise_byte_subtract(conversion_from_byte(in[i]),
												 conversion_from_byte(in[j]));
				if(temp_sum._7)  // if the difference is negative
				{
					byte temp;
					temp = in[i];
					in[i] = in[j];
					in[j] = temp;
					run_again = true;
				} else {
				}
			})
	})
		return in;
}

byte main() {
	byte ret;
	ret = 0;
	byte[ARRAY_SIZE] a;
	a[0] = 0xD7;
	a[1] = 0xD8;
	a[2] = 0xD6;
	a[3] = 0xD3;
	a[4] = 0xD5;
	a[5] = 0xD4;
	a[6] = 0xD2;
	a[7] = 0xD1;
	a = sort(a);
	return ret;
}
