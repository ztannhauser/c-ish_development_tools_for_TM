/*/
	deps =
	{
		"~/deps/utils.h",
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"~/deps/add/bitwise_byte.c",
		"~/deps/subtract/byte.c",
		"~/deps/subtract/bitwise_byte.c",
		"~/deps/multiply/bitwise_byte.c",
		"~/deps/conversion/from_byte.h",
		"~/deps/misc/print_byte.c",
		"~/deps/syscalls/IO.h",
		"~/deps/defines/loops.h",
	}
/*/

byte main() {
	struct bitwise_byte current;
	assign_bitwise_byte(current, 0, 0, 0, 0, 0, 0, 0, 0);
	struct bitwise_byte ten;
	assign_bitwise_byte(ten, 0, 0, 0, 0, 1, 0, 1, 0);
	struct bitwise_byte char_zero;
	char_zero = conversion_from_byte('0');
	char buffer;
	while(byte_subtract(buffer = read(), '\n'), {
		current = bitwise_byte_multiply(current, ten);
		current = bitwise_byte_add(
			current,
			bitwise_byte_subtract(conversion_from_byte(buffer), char_zero), 0);
	})
		print_byte(current);
	write('\n');
	return 0;
}
