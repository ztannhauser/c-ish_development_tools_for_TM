/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"bitwise_byte.c",
	}
/*/
#ifndef is_nonzero_bitwise_byte_c
#define is_nonzero_bitwise_byte_c
bool is_nonzero_bitwise_byte(struct bitwise_byte x);
#else
bool is_nonzero_bitwise_byte(struct bitwise_byte x) {
	bool ret;
	if(x._0)
		ret = true;
	else if(x._1)
		ret = true;
	else if(x._2)
		ret = true;
	else if(x._3)
		ret = true;
	else if(x._4)
		ret = true;
	else if(x._5)
		ret = true;
	else if(x._6)
		ret = true;
	else if(x._7)
		ret = true;
	else
		ret = false;
	return ret;
}
#endif
