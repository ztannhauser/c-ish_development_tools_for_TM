/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"~/deps/boolean/or.c",
		"short.c",
		"int.c",
	}
/*/
#ifndef int_is_nonzero_c
#define int_is_nonzero_c
bool int_is_nonzero(int x);
#else
bool int_is_nonzero(int x) {
	return boolean_or(short_is_nonzero(x.msig), short_is_nonzero(x.lsig));
}
#endif
