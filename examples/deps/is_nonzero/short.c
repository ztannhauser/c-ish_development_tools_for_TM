/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"~/deps/boolean/or.c",
		"short.c",
	}
/*/
#ifndef short_is_nonzero_c
#define short_is_nonzero_c
bool short_is_nonzero(short x);
#else
bool short_is_nonzero(short x) {
	return boolean_or(x.msig, x.lsig);
}
#endif
