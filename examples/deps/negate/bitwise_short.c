/*/
	deps =
	{
		"~/turing/deps/typedefs/c.h",
		"~/turing/deps/typedefs/bitwise.h",
		"~/turing/deps/add/bitwise_short.c",
		"bitwise_short.c",
	}
/*/
#ifndef bitwise_short_negate_c
#define bitwise_short_negate_c
struct bitwise_short bitwise_short_negate(struct bitwise_short x);
#else
struct bitwise_short bitwise_short_negate(struct bitwise_short x) {
	x = bitwise_short_not(x);
	struct bitwise_short one;
	one.msig._7 = false;
	one.msig._6 = false;
	one.msig._5 = false;
	one.msig._4 = false;
	one.msig._3 = false;
	one.msig._2 = false;
	one.msig._1 = false;
	one.msig._0 = false;
	one.lsig._7 = false;
	one.lsig._6 = false;
	one.lsig._5 = false;
	one.lsig._4 = false;
	one.lsig._3 = false;
	one.lsig._2 = false;
	one.lsig._1 = false;
	one.lsig._0 = true;
	return bitwise_short_add(x, one);
}
#endif
