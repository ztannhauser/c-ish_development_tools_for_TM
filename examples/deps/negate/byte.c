/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"~/deps/conversion/to_byte.c",
		"~/deps/conversion/from_byte.h",
		"bitwise_byte.c",
		"byte.c",
	}
/*/
#ifndef byte_negate_c
#define byte_negate_c
byte byte_negate(byte x);
#else
byte byte_negate(byte x) {
	return conversion_to_byte(bitwise_byte_negate(conversion_from_byte(x)));
}
#endif
