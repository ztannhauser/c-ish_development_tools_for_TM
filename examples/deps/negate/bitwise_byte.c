/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"~/deps/add/bitwise_byte.c",
		"~/deps/bitwise/byte/not.c",
		"bitwise_byte.c",
	}
/*/
#ifndef bitwise_byte_negate_c
#define bitwise_byte_negate_c
struct bitwise_byte bitwise_byte_negate(struct bitwise_byte x);
#else
struct bitwise_byte bitwise_byte_negate(struct bitwise_byte x) {
	x = bitwise_byte_not(x);
	struct bitwise_byte one;
	one._7 = false;
	one._6 = false;
	one._5 = false;
	one._4 = false;
	one._3 = false;
	one._2 = false;
	one._1 = false;
	one._0 = true;
	return bitwise_byte_add(x, one, 0);
}
#endif
