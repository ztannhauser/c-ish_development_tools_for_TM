/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"~/deps/add/bitwise_int.c",
		"~/deps/bitwise/int/not.c",
		"bitwise_int.c",
	}
/*/
#ifndef bitwise_int_negate_c
#define bitwise_int_negate_c
struct bitwise_int bitwise_int_negate(struct bitwise_int x);
#else
struct bitwise_int bitwise_int_negate(struct bitwise_int x) {
	x = bitwise_int_not(x);
	struct bitwise_int one;
	one.msig.msig._7 = false;
	one.msig.msig._6 = false;
	one.msig.msig._5 = false;
	one.msig.msig._4 = false;
	one.msig.msig._3 = false;
	one.msig.msig._2 = false;
	one.msig.msig._1 = false;
	one.msig.msig._0 = false;
	one.msig.lsig._7 = false;
	one.msig.lsig._6 = false;
	one.msig.lsig._5 = false;
	one.msig.lsig._4 = false;
	one.msig.lsig._3 = false;
	one.msig.lsig._2 = false;
	one.msig.lsig._1 = false;
	one.msig.lsig._0 = false;
	one.lsig.msig._7 = false;
	one.lsig.msig._6 = false;
	one.lsig.msig._5 = false;
	one.lsig.msig._4 = false;
	one.lsig.msig._3 = false;
	one.lsig.msig._2 = false;
	one.lsig.msig._1 = false;
	one.lsig.msig._0 = false;
	one.lsig.lsig._7 = false;
	one.lsig.lsig._6 = false;
	one.lsig.lsig._5 = false;
	one.lsig.lsig._4 = false;
	one.lsig.lsig._3 = false;
	one.lsig.lsig._2 = false;
	one.lsig.lsig._1 = false;
	one.lsig.lsig._0 = true;
	return bitwise_int_add(x, one, 0);
}
#endif
