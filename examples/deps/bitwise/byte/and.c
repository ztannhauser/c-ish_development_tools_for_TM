/*/
	deps =
	{
		"~/turing/deps/defines/c.h",
		"~/turing/deps/defines/bitwise.h",
		"~/turing/deps/boolean/and.c",
		"and.c",
	}
/*/
#ifndef bitwise_byte_and_c
#define bitwise_byte_and_c
struct bitwise_byte bitwise_byte_and(struct bitwise_byte b1,
									 struct bitwise_byte b2);
#else
struct bitwise_byte bitwise_byte_and(struct bitwise_byte b1,
									 struct bitwise_byte b2) {
	struct bitwise_byte ret;
	ret._0 = boolean_and(b1._0, b2._0);
	ret._1 = boolean_and(b1._1, b2._1);
	ret._2 = boolean_and(b1._2, b2._2);
	ret._3 = boolean_and(b1._3, b2._3);
	ret._4 = boolean_and(b1._4, b2._4);
	ret._5 = boolean_and(b1._5, b2._5);
	ret._6 = boolean_and(b1._6, b2._6);
	ret._7 = boolean_and(b1._7, b2._7);
	return ret;
}
#endif
