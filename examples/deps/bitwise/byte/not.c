/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"~/deps/boolean/not.c",
		"not.c",
	}
/*/
#ifndef bitwise_byte_not_c
#define bitwise_byte_not_c
struct bitwise_byte bitwise_byte_not(struct bitwise_byte x);
#else
struct bitwise_byte bitwise_byte_not(struct bitwise_byte x) {
	struct bitwise_byte ret;
	ret._0 = boolean_not(x._0);
	ret._1 = boolean_not(x._1);
	ret._2 = boolean_not(x._2);
	ret._3 = boolean_not(x._3);
	ret._4 = boolean_not(x._4);
	ret._5 = boolean_not(x._5);
	ret._6 = boolean_not(x._6);
	ret._7 = boolean_not(x._7);
	return ret;
}
#endif
