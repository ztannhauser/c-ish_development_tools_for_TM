/*/
	deps =
	{
		"~/turing/deps/defines/c.h",
		"~/turing/deps/defines/bitwise.h",
		"../short/or.c",
		"or.c",
	}
/*/
#ifndef bitwise_int_or_c
#define bitwise_int_or_c
struct bitwise_int bitwise_int_or(struct bitwise_int b1, struct bitwise_int b2);
#else
struct bitwise_int bitwise_int_or(struct bitwise_int b1,
								  struct bitwise_int b2) {
	struct bitwise_int ret;
	ret.msig = bitwise_short_or(b1.msig, b2.msig);
	ret.lsig = bitwise_short_or(b1.lsig, b2.lsig);
	return ret;
}
#endif
