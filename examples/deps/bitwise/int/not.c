/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"../short/not.c",
		"not.c",
	}
/*/
#ifndef bitwise_int_not_c
#define bitwise_int_not_c
struct bitwise_int bitwise_int_not(struct bitwise_int x);
#else
struct bitwise_int bitwise_int_not(struct bitwise_int x) {
	struct bitwise_int ret;
	ret.msig = bitwise_short_not(x.msig);
	ret.lsig = bitwise_short_not(x.lsig);
	return ret;
}
#endif
