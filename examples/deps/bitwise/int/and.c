/*/
	deps =
	{
		"~/turing/deps/defines/c.h",
		"~/turing/deps/defines/bitwise.h",
		"../short/and.c",
		"and.c",
	}
/*/
#ifndef bitwise_int_and_c
#define bitwise_int_and_c
struct bitwise_int bitwise_int_and(struct bitwise_int b1,
								   struct bitwise_int b2);
#else
struct bitwise_int bitwise_int_and(struct bitwise_int b1,
								   struct bitwise_int b2) {
	struct bitwise_int ret;
	ret.msig = bitwise_short_and(b1.msig, b2.msig);
	ret.lsig = bitwise_short_and(b1.lsig, b2.lsig);
	return ret;
}
#endif
