/*/
	deps =
	{
		"~/turing/deps/defines/c.h",
		"~/turing/deps/defines/bitwise.h",
		"../byte/and.c",
		"and.c",
	}
/*/
#ifndef bitwise_short_and_c
#define bitwise_short_and_c
struct bitwise_short bitwise_short_and(struct bitwise_short b1,
									   struct bitwise_short b2);
#else
struct bitwise_short bitwise_short_and(struct bitwise_short b1,
									   struct bitwise_short b2) {
	struct bitwise_short ret;
	ret.msig = bitwise_byte_and(b1.msig, b2.msig);
	ret.lsig = bitwise_byte_and(b1.lsig, b2.lsig);
	return ret;
}
#endif
