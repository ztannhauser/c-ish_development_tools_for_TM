/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"../byte/not.c",
		"not.c",
	}
/*/
#ifndef bitwise_short_not_c
#define bitwise_short_not_c
struct bitwise_short bitwise_short_not(struct bitwise_short x);
#else
struct bitwise_short bitwise_short_not(struct bitwise_short x) {
	struct bitwise_short ret;
	ret.msig = bitwise_byte_not(x.msig);
	ret.lsig = bitwise_byte_not(x.lsig);
	return ret;
}
#endif
