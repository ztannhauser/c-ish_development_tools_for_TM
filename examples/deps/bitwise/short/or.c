/*/
	deps =
	{
		"~/turing/deps/defines/c.h",
		"~/turing/deps/defines/bitwise.h",
		"../byte/or.c",
		"or.c",
	}
/*/
#ifndef bitwise_short_or_c
#define bitwise_short_or_c
struct bitwise_short bitwise_short_or(struct bitwise_short b1,
									  struct bitwise_short b2);
#else
struct bitwise_short bitwise_short_or(struct bitwise_short b1,
									  struct bitwise_short b2) {
	struct bitwise_short ret;
	ret.msig = bitwise_byte_or(b1.msig, b2.msig);
	ret.lsig = bitwise_byte_or(b1.lsig, b2.lsig);
	return ret;
}
#endif
