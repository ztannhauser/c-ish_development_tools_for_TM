/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"~/deps/add/bitwise_byte.c",
		"~/deps/negate/bitwise_byte.c",
		"~/deps/conversion/from_byte.h",
		"~/deps/conversion/to_byte.c",
		"byte.c",
	}
/*/
#ifndef byte_subtract_c
#define byte_subtract_c
byte byte_subtract(byte a, byte b);
#else
byte byte_subtract(byte a, byte b) {
	return conversion_to_byte(
		bitwise_byte_add(conversion_from_byte(a),
						 bitwise_byte_negate(conversion_from_byte(b)), 0)
			.sum);
}
#endif
