/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"~/deps/add/bitwise_int.c",
		"~/deps/negate/bitwise_int.c",
		"~/deps/conversion/from_int.c",
		"~/deps/conversion/to_int.c",
		"int.c",
	}
/*/
#ifndef int_subtract_c
#define int_subtract_c
int int_subtract(int a, int b);
#else
int int_subtract(int a, int b) {
	struct bitwise_int_add_returned result;
	result = bitwise_int_add(conversion_from_int(a),
							 bitwise_int_negate(conversion_from_int(b)), 0);
	return conversion_to_int(result.sum);
}
#endif
