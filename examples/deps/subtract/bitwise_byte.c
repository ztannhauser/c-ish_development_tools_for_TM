/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"~/deps/add/bitwise_byte.c",
		"~/deps/negate/bitwise_byte.c",
		"bitwise_byte.c",
	}
/*/
#ifndef bitwise_byte_subtract_c
#define bitwise_byte_subtract_c
struct bitwise_byte bitwise_byte_subtract(struct bitwise_byte a,
										  struct bitwise_byte b);
#else
struct bitwise_byte bitwise_byte_subtract(struct bitwise_byte a,
										  struct bitwise_byte b) {
	return bitwise_byte_add(a, bitwise_byte_negate(b), 0).sum;
}
#endif
