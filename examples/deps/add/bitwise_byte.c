/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"~/deps/boolean/add3.c",
		"bitwise_byte.c",
	}
/*/
#ifndef bitwise_byte_add_c
#define bitwise_byte_add_c
struct bitwise_byte_add_returned {
	struct bitwise_byte sum;
	bool carry;
};
struct bitwise_byte_add_returned bitwise_byte_add(struct bitwise_byte b1,
												  struct bitwise_byte b2,
												  bool first_carry);
#else
struct bitwise_byte_add_returned bitwise_byte_add(struct bitwise_byte b1,
												  struct bitwise_byte b2,
												  bool first_carry) {
	struct bitwise_byte_add_returned ret;
	struct boolean_add3_returned p;
	p.carry = first_carry;
#define add_bit(n)                         \
	p = boolean_add3(b1.n, b2.n, p.carry); \
	ret.sum.n = p.sum
	add_bit(_0);
	add_bit(_1);
	add_bit(_2);
	add_bit(_3);
	add_bit(_4);
	add_bit(_5);
	add_bit(_6);
	add_bit(_7);
	ret.carry = p.carry;
	return ret;
}
#endif
