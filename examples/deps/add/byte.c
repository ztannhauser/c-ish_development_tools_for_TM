/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"~/deps/conversion/from_byte.h",
		"~/deps/conversion/to_byte.c",
		"bitwise_byte.c",
		"byte.c",
	}
/*/
#ifndef byte_add_c
#define byte_add_c
byte byte_add(byte b1, byte b2);
#else
byte byte_add(byte b1, byte b2) {
	return conversion_to_byte(
		bitwise_byte_add(conversion_from_byte(b1), conversion_from_byte(b2), 0)
			.sum);
}
#endif
