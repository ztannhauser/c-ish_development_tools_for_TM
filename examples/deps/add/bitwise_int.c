/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"bitwise_short.c",
		"bitwise_int.c",
	}
/*/
#ifndef bitwise_int_add_c
#define bitwise_int_add_c
struct bitwise_int_add_returned {
	struct bitwise_int sum;
	bool carry;
};
struct bitwise_int_add_returned bitwise_int_add(struct bitwise_int b1,
												struct bitwise_int b2,
												bool first_carry);
#else
struct bitwise_int_add_returned bitwise_int_add(struct bitwise_int b1,
												struct bitwise_int b2,
												bool first_carry) {
	struct bitwise_int_add_returned ret;
	struct bitwise_short_add_returned call;
	call = bitwise_short_add(b1.lsig, b2.lsig, first_carry);
	ret.sum.lsig = call.sum;
	call = bitwise_short_add(b1.msig, b2.msig, call.carry);
	ret.sum.msig = call.sum;
	ret.carry = call.carry;
	return ret;
}
#endif
