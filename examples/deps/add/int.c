/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"~/deps/conversion/from_int.c",
		"~/deps/conversion/to_int.c",
		"bitwise_int.c",
		"int.c",
	}
/*/
#ifndef int_add_c
#define int_add_c
int int_add(int b1, int b2);
#else
int int_add(int b1, int b2) {
	struct bitwise_int_add_returned data;
	data = bitwise_int_add(conversion_from_int(b1), conversion_from_int(b2), 0);
	return conversion_to_int(data.sum);
}
#endif
