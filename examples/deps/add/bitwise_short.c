/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"bitwise_byte.c",
		"bitwise_short.c",
	}
/*/
#ifndef bitwise_short_add_c
#define bitwise_short_add_c
struct bitwise_short_add_returned {
	struct bitwise_short sum;
	bool carry;
};
struct bitwise_short_add_returned bitwise_short_add(struct bitwise_short b1,
													struct bitwise_short b2,
													bool first_carry);
#else
struct bitwise_short_add_returned bitwise_short_add(struct bitwise_short b1,
													struct bitwise_short b2,
													bool first_carry) {
	struct bitwise_short_add_returned ret;
	struct bitwise_byte_add_returned call;
	call = bitwise_byte_add(b1.lsig, b2.lsig, first_carry);
	ret.sum.lsig = call.sum;
	call = bitwise_byte_add(b1.msig, b2.msig, call.carry);
	ret.sum.msig = call.sum;
	ret.carry = call.carry;
	return ret;
}
#endif
