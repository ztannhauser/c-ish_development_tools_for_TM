/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"~/deps/conversion/from_byte.h",
		"~/deps/conversion/to_byte.c",
		"bitwise_byte.c",
		"byte.c"
	}
/*/
#ifndef byte_multiply_c
#define byte_multiply_c
byte byte_multiply(byte a, byte b);
#else
#if 1
byte byte_multiply(byte a, byte b) {
	return conversion_to_byte(bitwise_byte_multiply(conversion_from_byte(a),
													conversion_from_byte(b)));
}
#endif
#endif
