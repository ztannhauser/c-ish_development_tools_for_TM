/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"from_short.c",
		"from_int.c",
	}
/*/
#ifndef conversion_from_int_c
#define conversion_from_int_c
struct bitwise_int conversion_from_int(int x);
#else
struct bitwise_int conversion_from_int(int x) {
	struct bitwise_int ret;
	ret.msig = conversion_from_short(x.msig);
	ret.lsig = conversion_from_short(x.lsig);
	return ret;
}
#endif
