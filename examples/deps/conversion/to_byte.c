/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"to_byte.c",
	}
/*/
#ifndef conversion_to_byte_c
#define conversion_to_byte_c
byte conversion_to_byte(struct bitwise_byte x);
#else
byte conversion_to_byte(struct bitwise_byte x) {
	byte ret;
	if(x._7) {
		if(x._6) {
			if(x._5) {
				if(x._4) {
					if(x._3) {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0xFF;
								} else {
									ret = 0xFE;
								}
							} else {
								if(x._0) {
									ret = 0xFD;
								} else {
									ret = 0xFC;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0xFB;
								} else {
									ret = 0xFA;
								}
							} else {
								if(x._0) {
									ret = 0xF9;
								} else {
									ret = 0xF8;
								}
							}
						}
					} else {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0xF7;
								} else {
									ret = 0xF6;
								}
							} else {
								if(x._0) {
									ret = 0xF5;
								} else {
									ret = 0xF4;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0xF3;
								} else {
									ret = 0xF2;
								}
							} else {
								if(x._0) {
									ret = 0xF1;
								} else {
									ret = 0xF0;
								}
							}
						}
					}
				} else {
					if(x._3) {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0xEF;
								} else {
									ret = 0xEE;
								}
							} else {
								if(x._0) {
									ret = 0xED;
								} else {
									ret = 0xEC;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0xEB;
								} else {
									ret = 0xEA;
								}
							} else {
								if(x._0) {
									ret = 0xE9;
								} else {
									ret = 0xE8;
								}
							}
						}
					} else {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0xE7;
								} else {
									ret = 0xE6;
								}
							} else {
								if(x._0) {
									ret = 0xE5;
								} else {
									ret = 0xE4;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0xE3;
								} else {
									ret = 0xE2;
								}
							} else {
								if(x._0) {
									ret = 0xE1;
								} else {
									ret = 0xE0;
								}
							}
						}
					}
				}
			} else {
				if(x._4) {
					if(x._3) {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0xDF;
								} else {
									ret = 0xDE;
								}
							} else {
								if(x._0) {
									ret = 0xDD;
								} else {
									ret = 0xDC;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0xDB;
								} else {
									ret = 0xDA;
								}
							} else {
								if(x._0) {
									ret = 0xD9;
								} else {
									ret = 0xD8;
								}
							}
						}
					} else {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0xD7;
								} else {
									ret = 0xD6;
								}
							} else {
								if(x._0) {
									ret = 0xD5;
								} else {
									ret = 0xD4;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0xD3;
								} else {
									ret = 0xD2;
								}
							} else {
								if(x._0) {
									ret = 0xD1;
								} else {
									ret = 0xD0;
								}
							}
						}
					}
				} else {
					if(x._3) {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0xCF;
								} else {
									ret = 0xCE;
								}
							} else {
								if(x._0) {
									ret = 0xCD;
								} else {
									ret = 0xCC;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0xCB;
								} else {
									ret = 0xCA;
								}
							} else {
								if(x._0) {
									ret = 0xC9;
								} else {
									ret = 0xC8;
								}
							}
						}
					} else {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0xC7;
								} else {
									ret = 0xC6;
								}
							} else {
								if(x._0) {
									ret = 0xC5;
								} else {
									ret = 0xC4;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0xC3;
								} else {
									ret = 0xC2;
								}
							} else {
								if(x._0) {
									ret = 0xC1;
								} else {
									ret = 0xC0;
								}
							}
						}
					}
				}
			}
		} else {
			if(x._5) {
				if(x._4) {
					if(x._3) {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0xBF;
								} else {
									ret = 0xBE;
								}
							} else {
								if(x._0) {
									ret = 0xBD;
								} else {
									ret = 0xBC;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0xBB;
								} else {
									ret = 0xBA;
								}
							} else {
								if(x._0) {
									ret = 0xB9;
								} else {
									ret = 0xB8;
								}
							}
						}
					} else {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0xB7;
								} else {
									ret = 0xB6;
								}
							} else {
								if(x._0) {
									ret = 0xB5;
								} else {
									ret = 0xB4;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0xB3;
								} else {
									ret = 0xB2;
								}
							} else {
								if(x._0) {
									ret = 0xB1;
								} else {
									ret = 0xB0;
								}
							}
						}
					}
				} else {
					if(x._3) {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0xAF;
								} else {
									ret = 0xAE;
								}
							} else {
								if(x._0) {
									ret = 0xAD;
								} else {
									ret = 0xAC;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0xAB;
								} else {
									ret = 0xAA;
								}
							} else {
								if(x._0) {
									ret = 0xA9;
								} else {
									ret = 0xA8;
								}
							}
						}
					} else {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0xA7;
								} else {
									ret = 0xA6;
								}
							} else {
								if(x._0) {
									ret = 0xA5;
								} else {
									ret = 0xA4;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0xA3;
								} else {
									ret = 0xA2;
								}
							} else {
								if(x._0) {
									ret = 0xA1;
								} else {
									ret = 0xA0;
								}
							}
						}
					}
				}
			} else {
				if(x._4) {
					if(x._3) {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0x9F;
								} else {
									ret = 0x9E;
								}
							} else {
								if(x._0) {
									ret = 0x9D;
								} else {
									ret = 0x9C;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0x9B;
								} else {
									ret = 0x9A;
								}
							} else {
								if(x._0) {
									ret = 0x99;
								} else {
									ret = 0x98;
								}
							}
						}
					} else {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0x97;
								} else {
									ret = 0x96;
								}
							} else {
								if(x._0) {
									ret = 0x95;
								} else {
									ret = 0x94;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0x93;
								} else {
									ret = 0x92;
								}
							} else {
								if(x._0) {
									ret = 0x91;
								} else {
									ret = 0x90;
								}
							}
						}
					}
				} else {
					if(x._3) {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0x8F;
								} else {
									ret = 0x8E;
								}
							} else {
								if(x._0) {
									ret = 0x8D;
								} else {
									ret = 0x8C;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0x8B;
								} else {
									ret = 0x8A;
								}
							} else {
								if(x._0) {
									ret = 0x89;
								} else {
									ret = 0x88;
								}
							}
						}
					} else {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0x87;
								} else {
									ret = 0x86;
								}
							} else {
								if(x._0) {
									ret = 0x85;
								} else {
									ret = 0x84;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0x83;
								} else {
									ret = 0x82;
								}
							} else {
								if(x._0) {
									ret = 0x81;
								} else {
									ret = 0x80;
								}
							}
						}
					}
				}
			}
		}
	} else {
		if(x._6) {
			if(x._5) {
				if(x._4) {
					if(x._3) {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0x7F;
								} else {
									ret = 0x7E;
								}
							} else {
								if(x._0) {
									ret = 0x7D;
								} else {
									ret = 0x7C;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0x7B;
								} else {
									ret = 0x7A;
								}
							} else {
								if(x._0) {
									ret = 0x79;
								} else {
									ret = 0x78;
								}
							}
						}
					} else {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0x77;
								} else {
									ret = 0x76;
								}
							} else {
								if(x._0) {
									ret = 0x75;
								} else {
									ret = 0x74;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0x73;
								} else {
									ret = 0x72;
								}
							} else {
								if(x._0) {
									ret = 0x71;
								} else {
									ret = 0x70;
								}
							}
						}
					}
				} else {
					if(x._3) {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0x6F;
								} else {
									ret = 0x6E;
								}
							} else {
								if(x._0) {
									ret = 0x6D;
								} else {
									ret = 0x6C;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0x6B;
								} else {
									ret = 0x6A;
								}
							} else {
								if(x._0) {
									ret = 0x69;
								} else {
									ret = 0x68;
								}
							}
						}
					} else {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0x67;
								} else {
									ret = 0x66;
								}
							} else {
								if(x._0) {
									ret = 0x65;
								} else {
									ret = 0x64;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0x63;
								} else {
									ret = 0x62;
								}
							} else {
								if(x._0) {
									ret = 0x61;
								} else {
									ret = 0x60;
								}
							}
						}
					}
				}
			} else {
				if(x._4) {
					if(x._3) {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0x5F;
								} else {
									ret = 0x5E;
								}
							} else {
								if(x._0) {
									ret = 0x5D;
								} else {
									ret = 0x5C;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0x5B;
								} else {
									ret = 0x5A;
								}
							} else {
								if(x._0) {
									ret = 0x59;
								} else {
									ret = 0x58;
								}
							}
						}
					} else {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0x57;
								} else {
									ret = 0x56;
								}
							} else {
								if(x._0) {
									ret = 0x55;
								} else {
									ret = 0x54;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0x53;
								} else {
									ret = 0x52;
								}
							} else {
								if(x._0) {
									ret = 0x51;
								} else {
									ret = 0x50;
								}
							}
						}
					}
				} else {
					if(x._3) {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0x4F;
								} else {
									ret = 0x4E;
								}
							} else {
								if(x._0) {
									ret = 0x4D;
								} else {
									ret = 0x4C;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0x4B;
								} else {
									ret = 0x4A;
								}
							} else {
								if(x._0) {
									ret = 0x49;
								} else {
									ret = 0x48;
								}
							}
						}
					} else {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0x47;
								} else {
									ret = 0x46;
								}
							} else {
								if(x._0) {
									ret = 0x45;
								} else {
									ret = 0x44;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0x43;
								} else {
									ret = 0x42;
								}
							} else {
								if(x._0) {
									ret = 0x41;
								} else {
									ret = 0x40;
								}
							}
						}
					}
				}
			}
		} else {
			if(x._5) {
				if(x._4) {
					if(x._3) {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0x3F;
								} else {
									ret = 0x3E;
								}
							} else {
								if(x._0) {
									ret = 0x3D;
								} else {
									ret = 0x3C;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0x3B;
								} else {
									ret = 0x3A;
								}
							} else {
								if(x._0) {
									ret = 0x39;
								} else {
									ret = 0x38;
								}
							}
						}
					} else {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0x37;
								} else {
									ret = 0x36;
								}
							} else {
								if(x._0) {
									ret = 0x35;
								} else {
									ret = 0x34;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0x33;
								} else {
									ret = 0x32;
								}
							} else {
								if(x._0) {
									ret = 0x31;
								} else {
									ret = 0x30;
								}
							}
						}
					}
				} else {
					if(x._3) {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0x2F;
								} else {
									ret = 0x2E;
								}
							} else {
								if(x._0) {
									ret = 0x2D;
								} else {
									ret = 0x2C;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0x2B;
								} else {
									ret = 0x2A;
								}
							} else {
								if(x._0) {
									ret = 0x29;
								} else {
									ret = 0x28;
								}
							}
						}
					} else {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0x27;
								} else {
									ret = 0x26;
								}
							} else {
								if(x._0) {
									ret = 0x25;
								} else {
									ret = 0x24;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0x23;
								} else {
									ret = 0x22;
								}
							} else {
								if(x._0) {
									ret = 0x21;
								} else {
									ret = 0x20;
								}
							}
						}
					}
				}
			} else {
				if(x._4) {
					if(x._3) {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0x1F;
								} else {
									ret = 0x1E;
								}
							} else {
								if(x._0) {
									ret = 0x1D;
								} else {
									ret = 0x1C;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0x1B;
								} else {
									ret = 0x1A;
								}
							} else {
								if(x._0) {
									ret = 0x19;
								} else {
									ret = 0x18;
								}
							}
						}
					} else {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0x17;
								} else {
									ret = 0x16;
								}
							} else {
								if(x._0) {
									ret = 0x15;
								} else {
									ret = 0x14;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0x13;
								} else {
									ret = 0x12;
								}
							} else {
								if(x._0) {
									ret = 0x11;
								} else {
									ret = 0x10;
								}
							}
						}
					}
				} else {
					if(x._3) {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0x0F;
								} else {
									ret = 0x0E;
								}
							} else {
								if(x._0) {
									ret = 0x0D;
								} else {
									ret = 0x0C;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0x0B;
								} else {
									ret = 0x0A;
								}
							} else {
								if(x._0) {
									ret = 0x09;
								} else {
									ret = 0x08;
								}
							}
						}
					} else {
						if(x._2) {
							if(x._1) {
								if(x._0) {
									ret = 0x07;
								} else {
									ret = 0x06;
								}
							} else {
								if(x._0) {
									ret = 0x05;
								} else {
									ret = 0x04;
								}
							}
						} else {
							if(x._1) {
								if(x._0) {
									ret = 0x03;
								} else {
									ret = 0x02;
								}
							} else {
								if(x._0) {
									ret = 0x01;
								} else {
									ret = 0x00;
								}
							}
						}
					}
				}
			}
		}
	}
	return ret;
}
#endif
