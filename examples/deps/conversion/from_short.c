/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"from_byte.h",
		"from_short.c",
	}
/*/
#ifndef conversion_from_short_c
#define conversion_from_short_c
struct bitwise_short conversion_from_short(short x);
#else
struct bitwise_short conversion_from_short(short x) {
	struct bitwise_short ret;
	ret.msig = conversion_from_byte(x.msig);
	ret.lsig = conversion_from_byte(x.lsig);
	return ret;
}
#endif
