/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"to_byte.c",
		"to_short.c",
	}
/*/
#ifndef conversion_to_short_c
#define conversion_to_short_c
short conversion_to_short(struct bitwise_short x);
#else
short conversion_to_short(struct bitwise_short x) {
	short ret;
	ret.msig = conversion_to_byte(x.msig);
	ret.lsig = conversion_to_byte(x.lsig);
	return ret;
}
#endif
