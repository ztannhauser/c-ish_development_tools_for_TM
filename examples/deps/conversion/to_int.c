/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"to_short.c",
		"to_int.c",
	}
/*/
#ifndef conversion_to_int_c
#define conversion_to_int_c
int conversion_to_int(struct bitwise_int x);
#else
int conversion_to_int(struct bitwise_int x) {
	int ret;
	ret.msig = conversion_to_short(x.msig);
	ret.lsig = conversion_to_short(x.lsig);
	return ret;
}
#endif
