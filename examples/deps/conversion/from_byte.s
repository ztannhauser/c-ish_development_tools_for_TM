/*/
	deps = 
	{
		"~/helpers/switch.s",
	}
/*/

#include "../../helpers/all_numbers.hpp"

#define A(x, y) x => x, N, conversion_from_byte_7_##y

extern conversion_from_byte:
	ALL_NUMBERS
;

#define B(y, b7, b6, b5, b4, b3, b2, b1, b0)\
	conversion_from_byte_7_##y: * => b7, R, conversion_from_byte_6_##y;\
	conversion_from_byte_6_##y: * => b6, R, conversion_from_byte_5_##y;\
	conversion_from_byte_5_##y: * => b5, R, conversion_from_byte_4_##y;\
	conversion_from_byte_4_##y: * => b4, R, conversion_from_byte_3_##y;\
	conversion_from_byte_3_##y: * => b3, R, conversion_from_byte_2_##y;\
	conversion_from_byte_2_##y: * => b2, R, conversion_from_byte_1_##y;\
	conversion_from_byte_1_##y: * => b1, R, conversion_from_byte_0_##y;\
	conversion_from_byte_0_##y: * => b0, L, conversion_from_byte_8;

#define C(y, b7, b6, b5, b4)\
	B(0x##y##0, b7, b6, b5, b4, '\x0', '\x0', '\x0', '\x0')\
	B(0x##y##1, b7, b6, b5, b4, '\x0', '\x0', '\x0', '\x1')\
	B(0x##y##2, b7, b6, b5, b4, '\x0', '\x0', '\x1', '\x0')\
	B(0x##y##3, b7, b6, b5, b4, '\x0', '\x0', '\x1', '\x1')\
	B(0x##y##4, b7, b6, b5, b4, '\x0', '\x1', '\x0', '\x0')\
	B(0x##y##5, b7, b6, b5, b4, '\x0', '\x1', '\x0', '\x1')\
	B(0x##y##6, b7, b6, b5, b4, '\x0', '\x1', '\x1', '\x0')\
	B(0x##y##7, b7, b6, b5, b4, '\x0', '\x1', '\x1', '\x1')\
	B(0x##y##8, b7, b6, b5, b4, '\x1', '\x0', '\x0', '\x0')\
	B(0x##y##9, b7, b6, b5, b4, '\x1', '\x0', '\x0', '\x1')\
	B(0x##y##A, b7, b6, b5, b4, '\x1', '\x0', '\x1', '\x0')\
	B(0x##y##B, b7, b6, b5, b4, '\x1', '\x0', '\x1', '\x1')\
	B(0x##y##C, b7, b6, b5, b4, '\x1', '\x1', '\x0', '\x0')\
	B(0x##y##D, b7, b6, b5, b4, '\x1', '\x1', '\x0', '\x1')\
	B(0x##y##E, b7, b6, b5, b4, '\x1', '\x1', '\x1', '\x0')\
	B(0x##y##F, b7, b6, b5, b4, '\x1', '\x1', '\x1', '\x1')

C(0, '\x0', '\x0', '\x0', '\x0')
C(1, '\x0', '\x0', '\x0', '\x1')
C(2, '\x0', '\x0', '\x1', '\x0')
C(3, '\x0', '\x0', '\x1', '\x1')
C(4, '\x0', '\x1', '\x0', '\x0')
C(5, '\x0', '\x1', '\x0', '\x1')
C(6, '\x0', '\x1', '\x1', '\x0')
C(7, '\x0', '\x1', '\x1', '\x1')
C(8, '\x1', '\x0', '\x0', '\x0')
C(9, '\x1', '\x0', '\x0', '\x1')
C(A, '\x1', '\x0', '\x1', '\x0')
C(B, '\x1', '\x0', '\x1', '\x1')
C(C, '\x1', '\x1', '\x0', '\x0')
C(D, '\x1', '\x1', '\x0', '\x1')
C(E, '\x1', '\x1', '\x1', '\x0')
C(F, '\x1', '\x1', '\x1', '\x1')

conversion_from_byte_8: * => *, L, conversion_from_byte_9;
conversion_from_byte_9: * => *, L, conversion_from_byte_A;
conversion_from_byte_A: * => *, L, conversion_from_byte_B;
conversion_from_byte_B: * => *, L, conversion_from_byte_C;
conversion_from_byte_C: * => *, L, conversion_from_byte_D;
conversion_from_byte_D: * => *, L, conversion_from_byte_E;
conversion_from_byte_E: * => *, L, switch;

















