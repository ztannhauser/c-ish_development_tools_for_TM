/*/
	deps =
	{
		"~/helpers/start.s",
		"~/helpers/switch.s",
		"~/helpers/walker/mass_pickup.s",
		"~/helpers/walker/mass_dropoff.s",
		"~/helpers/compiler_copier.s",
		"~/helpers/syscall_return_state.s",
		"~/helpers/exit.s",
	}
/*/
