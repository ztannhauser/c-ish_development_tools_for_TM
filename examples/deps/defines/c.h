/*/
/*/
#define true 1
#define false 0
typedef byte bool;
typedef byte char;
struct _short {
	byte msig;
	byte lsig;
};
typedef struct _short short;
#define assign_short(short, a, b) \
	short.msig = a;               \
	short.lsig = b
struct _int {
	short msig;
	short lsig;
};
typedef struct _int int;
#define assign_int(int, a, b, c, d) \
	assign_short(int.msig, a, b);   \
	assign_short(int.lsig, c, d)
