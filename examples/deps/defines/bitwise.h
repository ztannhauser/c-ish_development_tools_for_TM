/*/
/*/
struct bitwise_byte {
	bool _7;
	bool _6;
	bool _5;
	bool _4;
	bool _3;
	bool _2;
	bool _1;
	bool _0;
};
#define assign_bitwise_byte(var, b7, b6, b5, b4, b3, b2, b1, b0) \
	var._7 = b7;                                                 \
	var._6 = b6;                                                 \
	var._5 = b5;                                                 \
	var._4 = b4;                                                 \
	var._3 = b3;                                                 \
	var._2 = b2;                                                 \
	var._1 = b1;                                                 \
	var._0 = b0;
struct bitwise_short {
	struct bitwise_byte msig;
	struct bitwise_byte lsig;
};
struct bitwise_int {
	struct bitwise_short msig;
	struct bitwise_short lsig;
};
