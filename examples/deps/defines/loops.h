/*/
/*/

#define for(before, conditional, delta, body){ \
	before; goto check; content : body delta;  \
	check : if(conditional){goto content; } else {} }

#define while(condition, body) for(, condition, , body)
