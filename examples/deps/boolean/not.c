/*/
	deps =
	{
		"../defines/c.h",
		"not.c",
	}
/*/
#ifndef boolean_not_c
#define boolean_not_c
bool boolean_not(bool x);
#else
bool boolean_not(bool x) {
	bool ret;
	if(x) {
		ret = false;
	} else {
		ret = true;
	}
	return ret;
}
#endif
