/*/
	deps =
	{
		"../defines/c.h",
		"and.c",
		"xor.c",
		"add.c",
	}
/*/
#ifndef boolean_add_c
#define boolean_add_c
struct boolean_add_returned {
	bool sum;
	bool carry;
};
struct boolean_add_returned boolean_add(bool x, bool y);
#else
struct boolean_add_returned boolean_add(bool x, bool y) {
	struct boolean_add_returned ret;
	ret.sum = boolean_xor(x, y);
	ret.carry = boolean_and(x, y);
	return ret;
}
#endif
