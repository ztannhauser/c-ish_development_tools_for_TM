/*/
	deps =
	{
		"../defines/c.h",
		"or.c",
	}
/*/
#ifndef boolean_or_c
#define boolean_or_c
bool boolean_or(bool b1, bool b2);
#else
bool boolean_or(bool b1, bool b2) {
	bool ret;
	if(b1) {
		ret = true;
	} else {
		if(b2) {
			ret = true;
		} else {
			ret = false;
		}
	}
	return ret;
}
#endif
