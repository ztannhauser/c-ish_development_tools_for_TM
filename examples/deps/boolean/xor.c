/*/
	deps =
	{
		"../defines/c.h",
		"xor.c",
	}
/*/
#ifndef boolean_xor_c
#define boolean_xor_c
bool boolean_xor(bool b1, bool b2);
#else
bool boolean_xor(bool b1, bool b2) {
	bool ret;
	if(b1) {
		if(b2) {
			ret = false;
		} else {
			ret = true;
		}
	} else {
		if(b2) {
			ret = true;
		} else {
			ret = false;
		}
	}
	return ret;
}
#endif
