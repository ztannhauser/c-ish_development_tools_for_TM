/*/
	deps =
	{
		"~/deps/defines/c.h",
		"or.c",
		"add.c",
		"add3.c",
	}
/*/
#ifndef boolean_add3_c
#define boolean_add3_c
struct boolean_add3_returned {
	bool sum;
	bool carry;
};
struct boolean_add3_returned boolean_add3(bool x, bool y, bool z);
#else
struct boolean_add3_returned boolean_add3(bool x, bool y, bool z) {
	struct boolean_add_returned partial_sum1;
	partial_sum1 = boolean_add(x, y);
	struct boolean_add_returned partial_sum2;
	partial_sum2 = boolean_add(partial_sum1.sum, z);
	struct boolean_add3_returned ret;
	ret.sum = partial_sum2.sum;
	// ret.carry = 5;
	// ret.carry = partial_sum2.carry;
	ret.carry = boolean_or(partial_sum1.carry, partial_sum2.carry);
	// ret.carry = boolean_or(0, 0);
	return ret;
}
#endif
