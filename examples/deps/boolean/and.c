/*/
	deps =
	{
		"../defines/c.h",
		"and.c",
	}
/*/
#ifndef boolean_and_c
#define boolean_and_c
bool boolean_and(bool b1, bool b2);
#else
bool boolean_and(bool b1, bool b2) {
	bool ret;
	if(b1) {
		if(b2) {
			ret = true;
		} else {
			ret = false;
		}
	} else {
		ret = false;
	}
	return ret;
}
#endif
