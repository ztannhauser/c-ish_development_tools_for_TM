/*/
	deps =
	{
		"~/deps/defines/c.h",
		"~/deps/defines/bitwise.h",
		"~/deps/syscalls/IO.h",
		"print_byte.c",
	}
/*/

#ifndef print_byte_c
#define print_byte_c

byte print_byte(struct bitwise_byte b);

#else

byte print_bit(bool bit) {
	if(bit)
		write('1');
	else
		write('0');
	return;
}

byte print_byte(struct bitwise_byte b) {
	print_bit(b._7);
	print_bit(b._6);
	print_bit(b._5);
	print_bit(b._4);
	print_bit(b._3);
	print_bit(b._2);
	print_bit(b._1);
	print_bit(b._0);
	return;
}
#endif
