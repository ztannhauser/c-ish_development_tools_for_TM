/*/
	deps =
	{
		"~/turing/deps/utils.h",
		"~/turing/deps/defines/c.h",
		"~/turing/deps/defines/bitwise.h",
		"~/turing/deps/conversion/to_int.c",
	}
/*/
byte main() {
	struct bitwise_int b;
	b.msig.msig._0 = true;
	b.msig.msig._1 = true;
	b.msig.msig._2 = false;
	b.msig.msig._3 = false;
	b.msig.msig._4 = false;
	b.msig.msig._5 = false;
	b.msig.msig._6 = false;
	b.msig.msig._7 = true;
	b.msig.lsig._0 = true;
	b.msig.lsig._1 = true;
	b.msig.lsig._2 = false;
	b.msig.lsig._3 = false;
	b.msig.lsig._4 = false;
	b.msig.lsig._5 = false;
	b.msig.lsig._6 = false;
	b.msig.lsig._7 = false;
	b.lsig.msig._0 = true;
	b.lsig.msig._1 = true;
	b.lsig.msig._2 = false;
	b.lsig.msig._3 = false;
	b.lsig.msig._4 = true;
	b.lsig.msig._5 = false;
	b.lsig.msig._6 = true;
	b.lsig.msig._7 = true;
	b.lsig.lsig._0 = false;
	b.lsig.lsig._1 = true;
	b.lsig.lsig._2 = false;
	b.lsig.lsig._3 = false;
	b.lsig.lsig._4 = true;
	b.lsig.lsig._5 = false;
	b.lsig.lsig._6 = true;
	b.lsig.lsig._7 = true;
	int out;
	out = conversion_to_int(b);
	return;
}
