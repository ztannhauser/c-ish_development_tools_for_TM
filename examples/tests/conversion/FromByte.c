/*/
	deps =
	{
		"~/turing/deps/utils.h",
		"~/turing/deps/defines/c.h",
		"~/turing/deps/defines/bitwise.h",
		"~/turing/deps/conversion/from_byte.h",
	}
/*/
byte main() {
	struct bitwise_byte out;
	out = conversion_from_byte(0x85);
	return;
}
