/*/
	deps =
	{
		"~/turing/deps/utils.h",
		"~/turing/deps/defines/c.h",
		"~/turing/deps/defines/bitwise.h",
		"~/turing/deps/conversion/from_int.c",
	}
/*/
byte main() {
	int input;
	input.msig.msig = 0xDE;
	input.msig.lsig = 0xAD;
	input.lsig.msig = 0xBE;
	input.lsig.lsig = 0xEF;
	struct bitwise_int out;
	out = conversion_from_int(input);
	return;
}
