/*/
	deps =
	{
		"~/turing/deps/utils.h",
		"~/turing/deps/defines/c.h",
		"~/turing/deps/defines/bitwise.h",
		"~/turing/deps/conversion/to_byte.c",
	}
/*/
byte main() {
	struct bitwise_byte b;
	b._0 = true;
	b._1 = true;
	b._2 = false;
	b._3 = false;
	b._4 = true;
	b._5 = false;
	b._6 = false;
	b._7 = false;
	byte out;
	out = conversion_to_byte(b);
	return;
}
