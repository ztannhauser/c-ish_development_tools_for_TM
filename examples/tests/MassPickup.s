/*/
	deps = 
	{
		"~/turing/helpers/walker/mass_pickup.s",
		"~/turing/helpers/syscall_return_state.s",
	}
/*/

extern start   : * => 'A', R, main_0;
main_0 : * => 'B', R, main_1;
main_1 : * => 'C', R, main_2;
main_2 : * => 'D', R, main_3;
main_3 : * => 'E', R, main_4;
main_4 : * => 'F', R, main_5;
main_5 : * => 'G', R, main_6;
main_6 : * => 'H', R, main_7;
main_7 : * => 'I', R, main_8;
main_8 : * => 'J', R, main_9;
main_9 : * => 'K', R, main_A;
main_A : * => 'L', R, main_B;
main_B : * => 'M', R, main_C;
main_C : * => 'N', R, main_D;
main_D : * => 'O', R, main_E;
main_E : * => 'P', R, main_F;
main_F : * => 'Q', R, main_10;
main_10: * => 'R', R, main_11;
main_11: * => 'S', R, main_12;
main_12: * => 'T', R, main_13;
main_13: * => 'U', R, main_14;
main_14: * => 'V', R, main_15;
main_15: * => 'W', R, main_16;
main_16: * => 'X', R, main_17;
main_17: * => 'Y', R, main_18;
main_18: * => 'Z', R, main_19;

main_19 : * => '\xF0', R, main_20;
main_20 : * => '\xF1', R, main_21;
main_21 : * => '\xF2', R, main_22;
main_22 : * => '\xF3', R, main_23;
main_23 : * => '\xF4', R, main_24;
main_24 : * => '\xF5', R, main_25;
main_25 : * => '\xF6', R, main_26;
main_26 : * => '\xF7', R, main_27;
main_27 : * => '\xF8', R, main_28;
main_28 : * => '\xF9', R, main_29;
main_29 : * => '\xFA', R, main_2A;
main_2A : * => '\xFB', R, main_2B;
main_2B : * => '\xFC', R, main_2C;
main_2C : * => '\xFD', R, main_2D;
main_2D : * => '\xFE', R, main_2E;
main_2E : * => '\xFF', R, main_2F;

main_2F : * => '\x0F', R, main_30;
main_30 : * => '\x0E', R, main_31;
main_31 : * => '\x0D', R, main_32;
main_32 : * => '\x0C', R, main_33;
main_33 : * => '\x0B', R, main_34;
main_34 : * => '\x0A', R, main_35;
main_35 : * => '\x09', R, main_36;
main_36 : * => '\x08', R, main_37;
main_37 : * => '\x07', R, main_38;
main_38 : * => '\x06', R, main_39;
main_39 : * => '\x05', R, main_3A;
main_3A : * => '\x04', R, main_3B;
main_3B : * => '\x03', R, main_3C;
main_3C : * => '\x02', R, main_3D;
main_3D : * => '\x01', R, main_3E;
main_3E : * => '\x00', R, main_3F;

main_3F: * => main_42, R, main_40;
main_40: * => '\x34', R, main_41;
main_41: * => '\x3', L, mass_pickup;

extern main_42: * => 'M', N, accept;

// these are here to satisify the walker_switch
extern mass_dropoff_0_3: * => '!', N, reject;
extern mass_dropoff_0_5: * => '!', N, reject;
extern mass_dropoff_0_7: * => '!', N, reject;
extern mass_dropoff_0_B: * => '!', N, reject;
extern mass_dropoff_0_D: * => '!', N, reject;
extern mass_dropoff_1_0: * => '!', N, reject;
extern mass_dropoff_1_3: * => '!', N, reject;
extern mass_dropoff_1_4: * => '!', N, reject;
extern mass_dropoff_2_0: * => '!', N, reject;
extern mass_dropoff_2_1: * => '!', N, reject;
extern mass_dropoff_2_2: * => '!', N, reject;
extern mass_dropoff_3_0: * => '!', N, reject;
extern mass_dropoff_3_1: * => '!', N, reject;
extern mass_dropoff_3_2: * => '!', N, reject;
extern mass_dropoff_3_5: * => '!', N, reject;
extern mass_dropoff_4_0: * => '!', N, reject;
extern mass_dropoff_4_1: * => '!', N, reject;
extern mass_dropoff_4_3: * => '!', N, reject;
extern mass_dropoff_4_5: * => '!', N, reject;
extern mass_dropoff_4_7: * => '!', N, reject;
extern mass_dropoff_4_9: * => '!', N, reject;
extern mass_dropoff_4__E: * => '!', N, reject;
extern mass_dropoff_4_10: * => '!', N, reject;
extern mass_dropoff_4_12: * => '!', N, reject;














