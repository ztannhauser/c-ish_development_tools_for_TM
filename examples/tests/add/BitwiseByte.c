/*/
	deps =
	{
		"~/turing/deps/utils.h",
		"~/turing/deps/defines/c.h",
		"~/turing/deps/defines/bitwise.h",
		"~/turing/deps/add/bitwise_byte.c",
	}
/*/
byte main() {
	struct bitwise_byte b1;
	b1._0 = true;
	b1._1 = true;
	b1._2 = true;
	b1._3 = true;
	b1._4 = true;
	b1._5 = true;
	b1._6 = true;
	b1._7 = true;
	struct bitwise_byte b2;
	b2._0 = true;
	b2._1 = false;
	b2._2 = false;
	b2._3 = false;
	b2._4 = false;
	b2._5 = false;
	b2._6 = false;
	b2._7 = false;
	struct bitwise_byte_add_returned sum;
	sum = bitwise_byte_add(b1, b2, 0);
	return;
}
