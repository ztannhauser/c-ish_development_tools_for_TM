/*/
	deps =
	{
		"~/turing/deps/utils.h",
		"~/turing/deps/defines/c.h",
		"~/turing/deps/defines/bitwise.h",
		"~/turing/deps/add/bitwise_short.c",
	}
/*/
byte main() {
	struct bitwise_short b1;
	b1.lsig._0 = true;
	b1.lsig._1 = true;
	b1.lsig._2 = true;
	b1.lsig._3 = true;
	b1.lsig._4 = true;
	b1.lsig._5 = true;
	b1.lsig._6 = true;
	b1.lsig._7 = true;
	b1.msig._0 = true;
	b1.msig._1 = true;
	b1.msig._2 = true;
	b1.msig._3 = true;
	b1.msig._4 = true;
	b1.msig._5 = true;
	b1.msig._6 = true;
	b1.msig._7 = true;
	struct bitwise_short b2;
	b2.lsig._0 = true;
	b2.lsig._1 = false;
	b2.lsig._2 = false;
	b2.lsig._3 = false;
	b2.lsig._4 = false;
	b2.lsig._5 = false;
	b2.lsig._6 = false;
	b2.lsig._7 = false;
	b2.msig._0 = false;
	b2.msig._1 = false;
	b2.msig._2 = false;
	b2.msig._3 = false;
	b2.msig._4 = false;
	b2.msig._5 = false;
	b2.msig._6 = false;
	b2.msig._7 = false;
	struct bitwise_short_add_returned sum;
	sum = bitwise_short_add(b1, b2, 0);
	return;
}
