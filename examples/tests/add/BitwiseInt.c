/*/
	deps =
	{
		"~/turing/deps/utils.h",
		"~/turing/deps/defines/c.h",
		"~/turing/deps/defines/bitwise.h",
		"~/turing/deps/add/bitwise_int.c",
	}
/*/
byte main() {
	struct bitwise_int b1;
	b1.lsig.lsig._0 = true;
	b1.lsig.lsig._1 = true;
	b1.lsig.lsig._2 = true;
	b1.lsig.lsig._3 = true;
	b1.lsig.lsig._4 = true;
	b1.lsig.lsig._5 = true;
	b1.lsig.lsig._6 = true;
	b1.lsig.lsig._7 = true;
	b1.lsig.msig._0 = true;
	b1.lsig.msig._1 = true;
	b1.lsig.msig._2 = true;
	b1.lsig.msig._3 = true;
	b1.lsig.msig._4 = true;
	b1.lsig.msig._5 = true;
	b1.lsig.msig._6 = true;
	b1.lsig.msig._7 = true;
	b1.msig.lsig._0 = true;
	b1.msig.lsig._1 = true;
	b1.msig.lsig._2 = true;
	b1.msig.lsig._3 = true;
	b1.msig.lsig._4 = true;
	b1.msig.lsig._5 = true;
	b1.msig.lsig._6 = true;
	b1.msig.lsig._7 = true;
	b1.msig.msig._0 = true;
	b1.msig.msig._1 = true;
	b1.msig.msig._2 = true;
	b1.msig.msig._3 = true;
	b1.msig.msig._4 = true;
	b1.msig.msig._5 = true;
	b1.msig.msig._6 = true;
	b1.msig.msig._7 = true;
	struct bitwise_int b2;
	b2.lsig.lsig._0 = true;
	b2.lsig.lsig._1 = false;
	b2.lsig.lsig._2 = false;
	b2.lsig.lsig._3 = false;
	b2.lsig.lsig._4 = false;
	b2.lsig.lsig._5 = false;
	b2.lsig.lsig._6 = false;
	b2.lsig.lsig._7 = false;
	b2.lsig.msig._0 = false;
	b2.lsig.msig._1 = false;
	b2.lsig.msig._2 = false;
	b2.lsig.msig._3 = false;
	b2.lsig.msig._4 = false;
	b2.lsig.msig._5 = false;
	b2.lsig.msig._6 = false;
	b2.lsig.msig._7 = false;
	b2.msig.lsig._0 = false;
	b2.msig.lsig._1 = false;
	b2.msig.lsig._2 = false;
	b2.msig.lsig._3 = false;
	b2.msig.lsig._4 = false;
	b2.msig.lsig._5 = false;
	b2.msig.lsig._6 = false;
	b2.msig.lsig._7 = false;
	b2.msig.msig._0 = false;
	b2.msig.msig._1 = false;
	b2.msig.msig._2 = false;
	b2.msig.msig._3 = false;
	b2.msig.msig._4 = false;
	b2.msig.msig._5 = false;
	b2.msig.msig._6 = false;
	b2.msig.msig._7 = false;
	struct bitwise_int_add_returned sum;
	sum = bitwise_int_add(b1, b2, 0);
	return;
}
