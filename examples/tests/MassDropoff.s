/*/
	deps = 
	{
		"~/turing/helpers/walker/mass_dropoff.s",
		"~/turing/helpers/syscall_return_state.s",
	}
/*/

extern start   : * => 'A', R, main_0;
main_0 : * => 'B', R, main_1;
main_1 : * => 'C', R, main_2;
main_2 : * => 'D', R, main_3;
main_3 : * => 'E', R, main_4;
main_4 : * => 'F', R, main_5;
main_5 : * => 'G', R, main_6;
main_6 : * => 'H', R, main_7;
main_7 : * => 'I', R, main_8;
main_8 : * => 'J', R, main_9;
main_9 : * => 'K', R, main_A;
main_A : * => 'L', R, main_B;
main_B : * => 'M', R, main_C;
main_C : * => 'N', R, main_D;
main_D : * => 'O', R, main_E;
main_E : * => 'P', R, main_F;
main_F : * => 'Q', R, main_10;
main_10: * => 'R', R, main_11;
main_11: * => 'S', R, main_12;
main_12: * => 'T', R, main_13;
main_13: * => 'U', R, main_14;
main_14: * => 'V', R, main_15;
main_15: * => 'W', R, main_16;
main_16: * => 'X', R, main_17;
main_17: * => 'Y', R, main_18;
main_18: * => 'Z', R, main_19;
main_19: * => main_30, R, main_20;
main_20: * => '\x03', R, main_21;
main_21: * => '\x14', R, main_22;

main_22: * => *, R, main_R_0;
main_R_0: * => *, R, main_R_1;
main_R_1: * => *, R, main_R_2;
main_R_2: * => *, R, main_R_3;
main_R_3: * => *, R, main__22;

main__22: * => 'g', R, main__23;
main__23: * => 'h', R, main__24;
main__24: * => 'i', L, main__25;
main__25: * => *, L, main__26;

main__26: * => *, L, main_L_0;
main_L_0: * => *, L, main_L_1;
main_L_1: * => *, L, main_L_2;
main_L_2: * => *, L, main_L_3;
main_L_3: * => *, L, main_26;

main_26: * => *, L, main_27;
main_27: * => *, L, mass_dropoff;

extern main_30: * => 'M', N, accept;

// these are here to satisify the walker_switch
extern mass_pickup_0_3: * => '!', N, reject;
extern mass_pickup_0_5: * => '!', N, reject;
extern mass_pickup_0_7: * => '!', N, reject;
extern mass_pickup_0_B: * => '!', N, reject;
extern mass_pickup_0_D: * => '!', N, reject;
extern mass_pickup_1_0: * => '!', N, reject;
extern mass_pickup_1_3: * => '!', N, reject;
extern mass_pickup_1_4: * => '!', N, reject;
extern mass_pickup_2_0: * => '!', N, reject;
extern mass_pickup_2_1: * => '!', N, reject;
extern mass_pickup_2_2: * => '!', N, reject;
extern mass_pickup_3_0: * => '!', N, reject;
extern mass_pickup_3_1: * => '!', N, reject;
extern mass_pickup_3_2: * => '!', N, reject;
extern mass_pickup_3_5: * => '!', N, reject;
extern mass_pickup_4_0: * => '!', N, reject;
extern mass_pickup_4_1: * => '!', N, reject;
extern mass_pickup_4_3: * => '!', N, reject;
extern mass_pickup_4__5: * => '!', N, reject;
extern mass_pickup_4_7: * => '!', N, reject;
extern mass_pickup_4_9: * => '!', N, reject;
extern mass_pickup_4_E: * => '!', N, reject;
extern mass_pickup_4_10: * => '!', N, reject;
extern mass_pickup_4_12: * => '!', N, reject;













