/*/
	deps =
	{
		"~/turing/deps/utils.h",
	}
/*/
struct return_type {
	byte x1;
	byte x2;
};
struct return_type func(byte b1, byte b2) {
	struct return_type t;
	t.x1 = b2;
	t.x2 = b1;
	return t;
}
byte main() {
	struct return_type b;
	b = func(3, 4);
	return;
}
