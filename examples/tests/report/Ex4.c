/*/
	deps =
	{
		"~/turing/deps/utils.h",
	}
/*/
byte main() {
	byte ret;
	ret = 0;
before_if:
	if(ret) {
		ret = 0;
	} else {
		ret = 1;
		goto before_if;
	}
	return ret;
}
