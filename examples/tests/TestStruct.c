/*/
	deps =
	{
		"~/turing/deps/utils.h",
	}
/*/
byte same(byte bb) {
	return bb;
}
struct same2_ret {
	byte a;
	byte b;
};
struct same2_ret same2(byte AA, byte BB) {
	struct same2_ret ret;
	ret.a = AA;
	ret.b = BB;
	return ret;
}
struct A {
	byte a;
	byte b;
	byte c;
};
byte main() {
	struct A aa;
	aa.a = 1;
	aa.b = 2;
	aa.c = 3;
#if 0
	aa.a = same(aa.a);
	aa.b = same(aa.b);
	aa.c = same(aa.c);
#endif
	struct same2_ret two;
	two = same2(aa.a, aa.c);
	aa.a = two.a;
	aa.c = two.b;
	return 0;
}
