/*/
	deps =
	{
		"~/turing/deps/utils.h",
	}
/*/
struct return_type {
	byte x1;
	byte x2;
	byte x3;
};
struct return_type main() {
	struct return_type t;
	t.x1 = 2;
	t.x2 = 4;
	t.x3 = 8;
	return t;
}
