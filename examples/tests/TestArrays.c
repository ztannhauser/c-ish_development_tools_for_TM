/*/
	deps =
	{
		"~/turing/deps/utils.h",
		"~/turing/deps/defines/c.h",
		"~/turing/deps/add/byte.c",
		"~/turing/deps/negate/byte.c",
		"~/turing/deps/multiply/byte.c",
	}
/*/

byte main() {
	byte i;
	i = 0;
	byte[4] a;
	if(true) {
		a[i] = 1;
		a[1] = 2;
		a[2] = 3;
		a[3] = 4;
		a[0] = a[i];
	} else {
	}
	return 0;
}
