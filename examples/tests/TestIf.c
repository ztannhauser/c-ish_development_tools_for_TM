/*/
	deps =
	{
		"~/turing/deps/utils.h",
	}
/*/

byte is_nonzero(byte b) {
	byte ret;
	if(b) {
		ret = 1;
	} else {
		ret = 0;
	}
	return ret;
}

byte main() {
	byte ret;
	ret = is_nonzero(6);
	return 0;
}
