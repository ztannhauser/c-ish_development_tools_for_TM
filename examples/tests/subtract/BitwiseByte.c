/*/
	deps =
	{
		"~/turing/deps/utils.h",
		"~/turing/deps/defines/c.h",
		"~/turing/deps/defines/bitwise.h",
		"~/turing/deps/subtract/bitwise_byte.c",
	}
/*/
byte main() {
	struct bitwise_byte diff;
	struct bitwise_byte a;  // 128 + 4 + 1 == 133
	a._7 = true;
	a._6 = false;
	a._5 = false;
	a._4 = false;
	a._3 = false;
	a._2 = true;
	a._1 = false;
	a._0 = true;
	struct bitwise_byte b;  // 16 + 4 + 1 == 21
	b._7 = false;
	b._6 = false;
	b._5 = false;
	b._4 = true;
	b._3 = false;
	b._2 = true;
	b._1 = false;
	b._0 = true;
	diff = bitwise_byte_subtract(a, b);  // 133 - 21 == 112
	// 64 + 32 + 16 == 112
	return;
}
