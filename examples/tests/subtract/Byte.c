/*/
	deps =
	{
		"~/turing/deps/utils.h",
		"~/turing/deps/defines/c.h",
		"~/turing/deps/subtract/byte.c",
	}
/*/

byte main() {
	byte ret;
	ret = 0;
	byte diff;
	diff = byte_subtract(5, 2);
	return ret;
}
