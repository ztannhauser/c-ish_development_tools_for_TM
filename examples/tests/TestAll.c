/*/
	deps =
	{
		"~/turing/deps/utils.h",
	}
/*/
byte same(byte bb) {
	return bb;
}
byte foo(byte n, byte m) {
	byte ret;
	if(n) {
		if(m) {
			ret = m;
		} else {
			ret = 0;
		}
	} else {
		ret = 1;
	}
	return same(ret);
}
byte main() {
	byte ret;
	byte old_ret;
	ret = 1;
before_if:
	if(ret) {
		old_ret = ret;
		ret = 0;
		goto before_if;
	} else {
		old_ret = ret;
		ret = 2;
		ret = foo(ret, old_ret);
	}
	return ret;  // expecting ret == 0
}
