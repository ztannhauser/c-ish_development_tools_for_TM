/*/
/*/

extern syscall_return_state:
	* => 'S', N, reject
;

extern
start  : * => '\x00', R, main_0;
main_0 : * => 'A', R, main_1;
main_1 : * => 'B', R, main_2;
main_2 : * => 'A', R, main_3;
main_3 : * => 'A', R, main_4;
main_4 : * => 'B', R, main_5;
main_5 : * => 'A', R, main_6;
main_6 : * => '\x00', L, main_7;

main_7 :
	* => *, L, main_7
	'\x00' => *, R, copy_letter
;

copy_letter:
	'A'    => 'X', R, write_A_0
	'B'    => 'Y', R, write_B_0
	'\x00' => *, N, accept
;

write_A_0:
	*      => *, R, write_A_0
	'\x00' => *, R, write_A_1 
;

write_A_1:
	*      => *, R, write_A_1
	'\x00' => 'A', N, move_back_0
;

write_B_0:
	*      => *, R, write_B_0
	'\x00' => *, R, write_B_1 
;

write_B_1:
	*      => *, R, write_B_1
	'\x00' => 'B', N, move_back_0
;

move_back_0:
	*      => *, L, move_back_0
	'\x00' => *, L, move_back_1
;

move_back_1:
	* => *, L, move_back_1
	'X' => 'A', R, copy_letter
	'Y' => 'B', R, copy_letter
;














