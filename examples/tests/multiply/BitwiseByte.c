/*/
	deps =
	{
		"~/turing/deps/utils.h",
		"~/turing/deps/defines/c.h",
		"~/turing/deps/defines/bitwise.h",
		"~/turing/deps/multiply/bitwise_byte.c",
	}
/*/

byte main() {
	struct bitwise_byte product;
	struct bitwise_byte a;
	assign_bitwise_byte(a, 1, 1, 1, 1, 1, 1, 1, 1);
	struct bitwise_byte b;
	assign_bitwise_byte(b, 1, 1, 1, 1, 1, 1, 1, 1);
	product = bitwise_byte_multiply(a, b);
	return 0;
}
