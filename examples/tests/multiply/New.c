/*/
	deps =
	{
		"~/turing/deps/utils.h",
		"~/turing/deps/defines/c.h",
		"~/turing/deps/defines/bitwise.h",
		"~/turing/deps/add/bitwise_byte.c",
	}
/*/

struct bitwise_byte new_mult(struct bitwise_byte a, struct bitwise_byte b) {
	struct bitwise_byte ret;
	if(a._0) {
		ret = b;
	} else {
		assign_bitwise_byte(ret, 0, 0, 0, 0, 0, 0, 0, 0);
	}
#define CALCULATE_BIT(CHECK_BIT, BS7, BS6, BS5, BS4, BS3, BS2, BS1, BS0)   \
	if(CHECK_BIT) {                                                        \
		struct bitwise_byte temp;                                          \
		assign_bitwise_byte(temp, BS7, BS6, BS5, BS4, BS3, BS2, BS1, BS0); \
		ret = bitwise_byte_add(ret, temp, 0).sum;                          \
	} else
	CALCULATE_BIT(a._1, b._6, b._5, b._4, b._3, b._2, b._1, b._0, 0);
	CALCULATE_BIT(a._2, b._5, b._4, b._3, b._2, b._1, b._0, 0, 0);
	CALCULATE_BIT(a._3, b._4, b._3, b._2, b._1, b._0, 0, 0, 0);
	CALCULATE_BIT(a._4, b._3, b._2, b._1, b._0, 0, 0, 0, 0);
	CALCULATE_BIT(a._5, b._2, b._1, b._0, 0, 0, 0, 0, 0);
	CALCULATE_BIT(a._6, b._1, b._0, 0, 0, 0, 0, 0, 0);
	CALCULATE_BIT(a._7, b._0, 0, 0, 0, 0, 0, 0, 0);
	return ret;
}

byte main() {
	struct bitwise_byte product;
	struct bitwise_byte a;
	assign_bitwise_byte(a, 1, 1, 1, 1, 1, 1, 1, 1);
	struct bitwise_byte b;
	assign_bitwise_byte(b, 1, 1, 1, 1, 1, 1, 1, 1);
	product = new_mult(a, b);
	return;
}
