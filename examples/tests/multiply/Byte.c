/*/
	deps =
	{
		"~/turing/deps/utils.h",
		"~/turing/deps/defines/c.h",
		"~/turing/deps/multiply/byte.c",
	}
/*/
byte main() {
	return byte_multiply(1, 1);
}
