/*/
	deps =
	{
		"~/turing/deps/utils.h",
	}
/*/
byte and_gate(byte b1, byte b2) {
	byte ret;
	if(b1) {
		if(b2) {
			return 1;
		} else {
			return 0;
		}
	} else {
		return 0;
	}
	return 0;
}
byte main() {
	byte ret;
	ret = and_gate(6, 8);
	return 0;
}
