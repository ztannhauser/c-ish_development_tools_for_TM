/*/
	deps =
	{
		"~/turing/deps/utils.h",
		"~/turing/deps/defines/c.h",
		"~/turing/deps/boolean/add.c",
	}
/*/
byte main() {
	struct boolean_add_returned ret;
	ret = boolean_add(1, 1);
	return;
}
