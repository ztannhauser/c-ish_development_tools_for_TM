/*/
	deps =
	{
		"~/turing/deps/utils.h",
		"~/turing/deps/defines/c.h",
		"~/turing/deps/boolean/add3.c",
	}
/*/
byte main() {
	struct boolean_add3_returned ret;
	ret = boolean_add3(1, 1, 1);
	return;
}
