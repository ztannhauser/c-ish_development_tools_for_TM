/*/
	deps = 
	{
		"../copiers/left_four_copy.s",
		"../copiers/H.s",
		"../copiers/G.s",
		"../copiers/C.s",
		"../copiers/D.s",
		"../dec.s",
		"./switch.s",
	}
/*/

// expected to start on decrement

// 0. Four_copy
// 1. Move Feilds
// 2. Decorement
// if nonzero, goto 1

#define left_walker_1_ptr '\x00'
#define left_walker_2_ptr '\x01'
#define left_walker_3_ptr '\x02'
#define left_walker_5_ptr '\x03'
#define left_walker_6_ptr '\x04'
#define left_walker_7_ptr '\x05'

extern left_walker: * => *, R, left_walker_0;
left_walker_0: * => left_walker_1_ptr, L, left_four_copy;
extern left_walker_1: * => left_walker_2_ptr, L, copy_H;
extern left_walker_2: * => left_walker_3_ptr, L, copy_G;
extern left_walker_3: * => *, L, left_walker_4;
left_walker_4: * => left_walker_5_ptr, R, copy_C;
extern left_walker_5: * => left_walker_6_ptr, R, copy_D;
extern left_walker_6: * => left_walker_7_ptr, L, dec;
extern left_walker_7: * => *, L, left_walker_8;
left_walker_8:
	* => *, N, left_walker
	'\x0' => *, R, left_walker_9
;
left_walker_9: * => *, R, left_walker_A;
left_walker_A: * => *, R, walker_switch;



