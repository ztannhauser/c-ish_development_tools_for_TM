/*/
	deps = 
	{
		"../copiers/B.s",
		"../copiers/A.s",
		"../copiers/E.s",
		"../copiers/F.s",
		"../copiers/right_four_copy.s",
		"../dec.s",
		"./switch.s",
	}
/*/

// must start on decrement

// 0. Move fields
// 1. Four Copy
// 2. Decrement
// 3. If nonzero, goto 0

#define right_walker_1_ptr '\x06'
#define right_walker_2_ptr '\x07'
#define right_walker_4_ptr '\x08'
#define right_walker_5_ptr '\x09'
#define right_walker_6_ptr '\x0A'
#define right_walker_7_ptr '\x0B'

extern right_walker: * => *, R, right_walker_0;
right_walker_0: * => right_walker_1_ptr, R, copy_B;
extern right_walker_1: * => right_walker_2_ptr, R, copy_A;
extern right_walker_2: * => *, R, right_walker_3;
right_walker_3: * => right_walker_4_ptr, L, copy_E;
extern right_walker_4: * => right_walker_5_ptr, L, copy_F;
extern right_walker_5: * => right_walker_6_ptr, R, right_four_copy;
extern right_walker_6: * => right_walker_7_ptr, L, dec;
extern right_walker_7: * => *, L, right_walker_8;
right_walker_8:
	* => *, N, right_walker
	'\x0' => *, R, right_walker_9
;
right_walker_9: * => *, R, right_walker_A;
right_walker_A: * => *, R, walker_switch;















