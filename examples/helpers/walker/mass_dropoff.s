/*/
	deps = 
	{
		"../copiers/E.s",
		"../inc.s",
		"../dec.s",
		"../switch.s",
		"right.s",
		"left.s",
		"switch.s",
	}
/*/


#define mass_dropoff_0_3_ptr  '\x0C'
#define mass_dropoff_0_5_ptr  '\x0D'
#define mass_dropoff_0_7_ptr  '\x0E'
#define mass_dropoff_0_B_ptr  '\x0F'
#define mass_dropoff_0_D_ptr  '\x10'
#define mass_dropoff_1_0_ptr  '\x11'
#define mass_dropoff_1_3_ptr  '\x12'
#define mass_dropoff_1_4_ptr  '\x13'
#define mass_dropoff_2_0_ptr  '\x14'
#define mass_dropoff_2_1_ptr  '\x15'
#define mass_dropoff_2_2_ptr  '\x16'
#define mass_dropoff_3_0_ptr  '\x17'
#define mass_dropoff_3_1_ptr  '\x18'
#define mass_dropoff_3_2_ptr  '\x19'
#define mass_dropoff_3_5_ptr  '\x1A'
#define mass_dropoff_4_0_ptr  '\x1B'
#define mass_dropoff_4_1_ptr  '\x1C'
#define mass_dropoff_4_3_ptr  '\x1D'
#define mass_dropoff_4_5_ptr  '\x1E'
#define mass_dropoff_4_7_ptr  '\x1F'
#define mass_dropoff_4_9_ptr  '\x20'
#define mass_dropoff_4__E_ptr '\x21'
#define mass_dropoff_4_10_ptr '\x22'
#define mass_dropoff_4_12_ptr '\x23'

// 1. (byte) Number of bytes to dropoff
// 2. (byte) Number of bytes away

extern mass_dropoff:     * => *,                    R, mass_dropoff_0_0;
mass_dropoff_0_0:        * => *,                    R, mass_dropoff_0_1;
mass_dropoff_0_1:        * => *,                    R, mass_dropoff_0_2;
mass_dropoff_0_2:        * => mass_dropoff_0_3_ptr, L, copy_E;
extern mass_dropoff_0_3: * => *,                    R, mass_dropoff_0_4;
mass_dropoff_0_4:        * => mass_dropoff_0_5_ptr, L, copy_E;
extern mass_dropoff_0_5: * => *,                    R, mass_dropoff_0_6;
mass_dropoff_0_6:        * => mass_dropoff_0_7_ptr, L, copy_E;
extern mass_dropoff_0_7: * => mass_dropoff_1_0_ptr, L, mass_dropoff_0_8;
mass_dropoff_0_8:        * => *,                    L, mass_dropoff_0_9;
mass_dropoff_0_9:        * => *,                    L, mass_dropoff_0_A;
mass_dropoff_0_A:        * => mass_dropoff_0_B_ptr, L, copy_E;
extern mass_dropoff_0_B: * => *,                    R, mass_dropoff_0_C;
mass_dropoff_0_C:        * => mass_dropoff_0_D_ptr, L, copy_E;
extern mass_dropoff_0_D: * => *,                    L, right_walker;

extern mass_dropoff_1_0: * => mass_dropoff_2_0_ptr, L, mass_dropoff_1_1;
mass_dropoff_1_1:        * => *,                    L, mass_dropoff_1_2;
mass_dropoff_1_2:        * => mass_dropoff_1_3_ptr, L, copy_E;
extern mass_dropoff_1_3: * => mass_dropoff_1_4_ptr, L, copy_F;
extern mass_dropoff_1_4: * => *,                    L, left_walker;

extern mass_dropoff_2_0: * => mass_dropoff_2_1_ptr, L, copy_G;
extern mass_dropoff_2_1: * => mass_dropoff_2_2_ptr, L, copy_H;
extern mass_dropoff_2_2: * => mass_dropoff_3_0_ptr, L, mass_dropoff_2_3;
mass_dropoff_2_3:        * => *,                    L, mass_dropoff_2_4;
mass_dropoff_2_4:        * => *,                    L, left_walker;

extern mass_dropoff_3_0: * => mass_dropoff_3_1_ptr, L, copy_G;
extern mass_dropoff_3_1: * => mass_dropoff_3_2_ptr, L, copy_H;
extern mass_dropoff_3_2: * => mass_dropoff_4_0_ptr, L, mass_dropoff_3_3;
mass_dropoff_3_3:        * => *,                    L, mass_dropoff_3_4;
mass_dropoff_3_4:        * => mass_dropoff_3_5_ptr, L, copy_H;
extern mass_dropoff_3_5: * => *,                    L, right_walker;

extern mass_dropoff_4_0: * => mass_dropoff_4_1_ptr,  L, copy_G;
extern mass_dropoff_4_1: * => *,                     L, mass_dropoff_4_2; // on M
mass_dropoff_4_2:        * => mass_dropoff_4_3_ptr,  L, copy_G;
extern mass_dropoff_4_3: * => *,                     L, mass_dropoff_4_4; // on T
mass_dropoff_4_4:        * => mass_dropoff_4_5_ptr,  L, inc;
extern mass_dropoff_4_5: * => *,                     R, mass_dropoff_4_6;
mass_dropoff_4_6:        * => mass_dropoff_4_7_ptr,  L, copy_E;
extern mass_dropoff_4_7: * => *,                     R, mass_dropoff_4_8;
mass_dropoff_4_8:        * => mass_dropoff_4_9_ptr,  L, copy_E;
extern mass_dropoff_4_9: * => mass_dropoff_1_0_ptr,  L, mass_dropoff_4_A;
mass_dropoff_4_A:        * => *,                     L, mass_dropoff_4_B;
mass_dropoff_4_B:        * => *,                     L, mass_dropoff_4_C;
mass_dropoff_4_C:        * => *,                     L, mass_dropoff_4_D;
mass_dropoff_4_D:        * => mass_dropoff_4__E_ptr, L, dec;

extern mass_dropoff_4__E: * => *, L, mass_dropoff_4__F;
mass_dropoff_4__F:
	  *   => *, R, mass_dropoff_4_E
	'\x0' => *, N, mass_dropoff_5_0
;

mass_dropoff_4_E:         * => *,                     R, mass_dropoff_4_F;
mass_dropoff_4_F:         * => mass_dropoff_4_10_ptr, L, copy_E;
extern mass_dropoff_4_10: * => *,                     R, mass_dropoff_4_11;
mass_dropoff_4_11:        * => mass_dropoff_4_12_ptr, L, copy_E;
extern mass_dropoff_4_12: * => *,                     L, right_walker;

mass_dropoff_5_0: * => *, L, switch;


















