/*/
/*/

extern walker_switch:
	'\x00' => *, N, left_walker_1
	'\x01' => *, N, left_walker_2
	'\x02' => *, N, left_walker_3
	'\x03' => *, N, left_walker_5
	'\x04' => *, N, left_walker_6
	'\x05' => *, N, left_walker_7
	'\x06' => *, N, right_walker_1
	'\x07' => *, N, right_walker_2
	'\x08' => *, N, right_walker_4
	'\x09' => *, N, right_walker_5
	'\x0A' => *, N, right_walker_6
	'\x0B' => *, N, right_walker_7
	
	'\x0C' => *, N, mass_dropoff_0_3
	'\x0D' => *, N, mass_dropoff_0_5
	'\x0E' => *, N, mass_dropoff_0_7
	'\x0F' => *, N, mass_dropoff_0_B
	'\x10' => *, N, mass_dropoff_0_D
	'\x11' => *, N, mass_dropoff_1_0
	'\x12' => *, N, mass_dropoff_1_3
	'\x13' => *, N, mass_dropoff_1_4
	'\x14' => *, N, mass_dropoff_2_0
	'\x15' => *, N, mass_dropoff_2_1
	'\x16' => *, N, mass_dropoff_2_2
	'\x17' => *, N, mass_dropoff_3_0
	'\x18' => *, N, mass_dropoff_3_1
	'\x19' => *, N, mass_dropoff_3_2
	'\x1A' => *, N, mass_dropoff_3_5
	'\x1B' => *, N, mass_dropoff_4_0
	'\x1C' => *, N, mass_dropoff_4_1
	'\x1D' => *, N, mass_dropoff_4_3
	'\x1E' => *, N, mass_dropoff_4_5
	'\x1F' => *, N, mass_dropoff_4_7
	'\x20' => *, N, mass_dropoff_4_9
	'\x21' => *, N, mass_dropoff_4__E
	'\x22' => *, N, mass_dropoff_4_10
	'\x23' => *, N, mass_dropoff_4_12

	'\x24' => *, N, mass_pickup_0_3
	'\x25' => *, N, mass_pickup_0_5
	'\x26' => *, N, mass_pickup_0_7
	'\x27' => *, N, mass_pickup_0_B
	'\x28' => *, N, mass_pickup_0_D
	'\x29' => *, N, mass_pickup_1_0
	'\x2A' => *, N, mass_pickup_1_3
	'\x2B' => *, N, mass_pickup_1_4
	'\x2C' => *, N, mass_pickup_2_0
	'\x2D' => *, N, mass_pickup_2_1
	'\x2E' => *, N, mass_pickup_2_2
	'\x2F' => *, N, mass_pickup_3_0
	'\x30' => *, N, mass_pickup_3_1
	'\x31' => *, N, mass_pickup_3_2
	'\x32' => *, N, mass_pickup_3_5
	'\x33' => *, N, mass_pickup_4_0
	'\x34' => *, N, mass_pickup_4_1
	'\x35' => *, N, mass_pickup_4_3
	'\x36' => *, N, mass_pickup_4__5
	'\x37' => *, N, mass_pickup_4_7
	'\x38' => *, N, mass_pickup_4_9
	'\x39' => *, N, mass_pickup_4_E
	'\x3A' => *, N, mass_pickup_4_10
	'\x3B' => *, N, mass_pickup_4_12
;























