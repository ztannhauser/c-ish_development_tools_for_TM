/*/
	deps =
	{
		"../copiers/E.s",
		"../copiers/H.s",
		"../copiers/F.s",
		"../inc.s",
		"../dec.s",
		"../switch.s",
		"left.s",
		"right.s",
		"switch.s",
	}
/*/

#define mass_pickup_0_3_ptr  '\x24'
#define mass_pickup_0_5_ptr  '\x25'
#define mass_pickup_0_7_ptr  '\x26'
#define mass_pickup_0_B_ptr  '\x27'
#define mass_pickup_0_D_ptr  '\x28'
#define mass_pickup_1_0_ptr  '\x29'
#define mass_pickup_1_3_ptr  '\x2A'
#define mass_pickup_1_4_ptr  '\x2B'
#define mass_pickup_2_0_ptr  '\x2C'
#define mass_pickup_2_1_ptr  '\x2D'
#define mass_pickup_2_2_ptr  '\x2E'
#define mass_pickup_3_0_ptr  '\x2F'
#define mass_pickup_3_1_ptr  '\x30'
#define mass_pickup_3_2_ptr  '\x31'
#define mass_pickup_3_5_ptr  '\x32'
#define mass_pickup_4_0_ptr  '\x33'
#define mass_pickup_4_1_ptr  '\x34'
#define mass_pickup_4_3_ptr  '\x35'
#define mass_pickup_4__5_ptr '\x36'
#define mass_pickup_4_7_ptr  '\x37'
#define mass_pickup_4_9_ptr  '\x38'
#define mass_pickup_4_E_ptr  '\x39'
#define mass_pickup_4_10_ptr '\x3A'
#define mass_pickup_4_12_ptr '\x3B'


// 1. (byte) Number of bytes away
// 2. (byte) Number of bytes to pickup

extern mass_pickup:     * => *,                   R, mass_pickup_0_0;
mass_pickup_0_0:        * => *,                   R, mass_pickup_0_1;
mass_pickup_0_1:        * => *,                   R, mass_pickup_0_2;
mass_pickup_0_2:        * => mass_pickup_0_3_ptr, L, copy_E;
extern mass_pickup_0_3: * => *,                   R, mass_pickup_0_4;
mass_pickup_0_4:        * => mass_pickup_0_5_ptr, L, copy_E;
extern mass_pickup_0_5: * => *,                   R, mass_pickup_0_6;
mass_pickup_0_6:        * => mass_pickup_0_7_ptr, L, copy_E;
extern mass_pickup_0_7: * => mass_pickup_1_0_ptr, L, mass_pickup_0_8;
mass_pickup_0_8:        * => *,                   L, mass_pickup_0_9;
mass_pickup_0_9:        * => *,                   L, mass_pickup_0_A;
mass_pickup_0_A:        * => mass_pickup_0_B_ptr, L, copy_E;
extern mass_pickup_0_B: * => *,                   R, mass_pickup_0_C;
mass_pickup_0_C:        * => mass_pickup_0_D_ptr, L, copy_E;
extern mass_pickup_0_D: * => *,                   L, left_walker;

extern mass_pickup_1_0: * => mass_pickup_2_0_ptr, L, mass_pickup_1_1;
mass_pickup_1_1:        * => *,                   L, mass_pickup_1_2;
mass_pickup_1_2:        * => mass_pickup_1_3_ptr, L, copy_E;
extern mass_pickup_1_3: * => mass_pickup_1_4_ptr, L, copy_F;
extern mass_pickup_1_4: * => *,                   L, right_walker;

extern mass_pickup_2_0: * => mass_pickup_2_1_ptr, L, copy_G;
extern mass_pickup_2_1: * => mass_pickup_2_2_ptr, L, copy_H;
extern mass_pickup_2_2: * => mass_pickup_3_0_ptr, L, mass_pickup_2_3;
mass_pickup_2_3:        * => *,                   L, mass_pickup_2_4;
mass_pickup_2_4:        * => *,                   L, right_walker;

extern mass_pickup_3_0: * => mass_pickup_3_1_ptr, L, copy_G;
extern mass_pickup_3_1: * => mass_pickup_3_2_ptr, L, copy_H;
extern mass_pickup_3_2: * => mass_pickup_4_0_ptr, L, mass_pickup_3_3;
mass_pickup_3_3:        * => *,                   L, mass_pickup_3_4;
mass_pickup_3_4:        * => mass_pickup_3_5_ptr, L, copy_H;
extern mass_pickup_3_5: * => *,                   L, left_walker;

extern mass_pickup_4_0:  * => mass_pickup_4_1_ptr,  L, copy_G;
extern mass_pickup_4_1:  * => *,                    L, mass_pickup_4_2;
mass_pickup_4_2:         * => mass_pickup_4_3_ptr,  L, copy_G;
extern mass_pickup_4_3:  * => *,                    L, mass_pickup_4_4;
mass_pickup_4_4:         * => mass_pickup_4__5_ptr, L, dec;
extern mass_pickup_4__5: * => *,                    L, mass_pickup_4__6;

mass_pickup_4__6:
	  *   => *, R, mass_pickup_4_5
	'\x0' => *, R, mass_pickup_5_0
;

mass_pickup_4_5:         * => *,                    R, mass_pickup_4_6;
mass_pickup_4_6:         * => mass_pickup_4_7_ptr,  L, copy_E;
extern mass_pickup_4_7:  * => *,                    R, mass_pickup_4_8;
mass_pickup_4_8:         * => mass_pickup_4_9_ptr,  L, copy_E;
extern mass_pickup_4_9:  * => mass_pickup_1_0_ptr,  L, mass_pickup_4_A;
mass_pickup_4_A:         * => *,                    L, mass_pickup_4_B;
mass_pickup_4_B:         * => *,                    L, mass_pickup_4_C;
mass_pickup_4_C:         * => *,                    L, mass_pickup_4_D;
mass_pickup_4_D:         * => mass_pickup_4_E_ptr,  L, inc;
extern mass_pickup_4_E:  * => *,                    R, mass_pickup_4_F;
mass_pickup_4_F:         * => mass_pickup_4_10_ptr, L, copy_E;
extern mass_pickup_4_10: * => *,                    R, mass_pickup_4_11;
mass_pickup_4_11:        * => mass_pickup_4_12_ptr, L, copy_E;
extern mass_pickup_4_12: * => *,                    L, left_walker;

mass_pickup_5_0: * => *, L, mass_pickup_5_1;
mass_pickup_5_1: * => *, L, mass_pickup_5_2;
mass_pickup_5_2: * => *, L, mass_pickup_5_3;
mass_pickup_5_3: * => *, L, switch;






















