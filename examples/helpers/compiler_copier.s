/*/
/*/

#include "all_numbers.hpp",

extern compiler_copier: * => *, L, compiler_copier_1;

compiler_copier_1:
	#define A(x, y) x => x, R, compiler_copier_2_##y
	ALL_NUMBERS
;

#define A(x, y) compiler_copier_2_##y: * => *, R, compiler_copier_3_##y;
ALL_NUMBERS

#define A(x, y) compiler_copier_3_##y: * => *, R, compiler_copier_4_##y;
ALL_NUMBERS

#define A(x, y) compiler_copier_4_##y: * => x, R, compiler_copier_5_##y;
ALL_NUMBERS

#define A(x, y) compiler_copier_5_##y: * => x, L, compiler_copier_6;
ALL_NUMBERS

compiler_copier_6: * => *, L, switch;

