/*/
/*/

#include "../all_numbers.hpp",

extern copy_D: * => *, R, copy_D_0;
copy_D_0: * => *, R, copy_D_1;

copy_D_1:
	#define A(x, y) x => x, L, copy_D_2_##y
	ALL_NUMBERS
;

#define A(x, y) copy_D_2_##y: * => x, L, copy_D_3;
ALL_NUMBERS

copy_D_3: * => *, L, walker_switch;
