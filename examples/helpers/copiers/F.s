/*/
/*/

#include "../all_numbers.hpp"

extern copy_F: * => *, L, copy_F_0;
copy_F_0: * => *, L, copy_F_1;

copy_F_1:
	#define A(x, y) x => x, R, copy_F_2_##y
	ALL_NUMBERS
;

#define A(x, y) copy_F_2_##y: * => x, R, copy_F_3;
ALL_NUMBERS

copy_F_3: * => *, R, walker_switch;
