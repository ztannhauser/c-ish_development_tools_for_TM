/*/
/*/

#include "../all_numbers.hpp"

extern copy_B: * => *, R, copy_B_1;

copy_B_1:
	#define A(x, y) x => x, R, copy_B_2_##y
	ALL_NUMBERS
;

#define A(x, y) copy_B_2_##y: * => x, L, copy_B_3;
ALL_NUMBERS

copy_B_3: * => *, L, copy_B_4;
copy_B_4: * => *, L, walker_switch;
