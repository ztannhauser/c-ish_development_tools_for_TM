/*/
/*/

#include "../all_numbers.hpp"

extern copy_H: * => *, L, copy_H_0;

copy_H_0:
	#define A(x, y) x => x, L, copy_H_1_##y
	ALL_NUMBERS
;

#define A(x, y) copy_H_1_##y: * => x, R, copy_H_2;
ALL_NUMBERS

copy_H_2: * => *, R, copy_H_3;

copy_H_3: * => *, R, walker_switch;

