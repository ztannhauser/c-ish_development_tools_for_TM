/*/
/*/

#include "../all_numbers.hpp"

extern left_four_copy: * => *, L, left_four_copy_1;

left_four_copy_1: * => *, L, left_four_copy_2;

left_four_copy_2:
	#define A(x, y) x => x, R, left_four_copy_3_##y
	ALL_NUMBERS
;

#define A(x, y) left_four_copy_3_##y : * => *, R, left_four_copy_4_##y;
ALL_NUMBERS

#define A(x, y) left_four_copy_4_##y : * => *, R, left_four_copy_5_##y;
ALL_NUMBERS

#define A(x, y) left_four_copy_5_##y : * => *, R, left_four_copy_6_##y;
ALL_NUMBERS

#define A(x, y) left_four_copy_6_##y : * => *, R, left_four_copy_7_##y;
ALL_NUMBERS

#define A(x, y) left_four_copy_7_##y : * => *, R, left_four_copy_8_##y;
ALL_NUMBERS

#define A(x, y) left_four_copy_8_##y : * => x, L, left_four_copy_9;
ALL_NUMBERS

left_four_copy_9: * => *, L, left_four_copy_A;
left_four_copy_A: * => *, L, walker_switch;
