/*/
/*/

#include "../all_numbers.hpp"

extern right_four_copy: * => *, R, right_four_copy_1;
right_four_copy_1: * => *, R, right_four_copy_2;

right_four_copy_2:
	#define A(x, y) x => *, L, right_four_copy_3_##y
	ALL_NUMBERS
;

#define A(x, y) right_four_copy_3_##y: * => *, L, right_four_copy_4_##y;
ALL_NUMBERS

#define A(x, y) right_four_copy_4_##y: * => *, L, right_four_copy_5_##y;
ALL_NUMBERS

#define A(x, y) right_four_copy_5_##y: * => *, L, right_four_copy_6_##y;
ALL_NUMBERS

#define A(x, y) right_four_copy_6_##y: * => *, L, right_four_copy_7_##y;
ALL_NUMBERS

#define A(x, y) right_four_copy_7_##y: * => *, L, right_four_copy_8_##y;
ALL_NUMBERS

#define A(x, y) right_four_copy_8_##y: * => x, R, right_four_copy_9;
ALL_NUMBERS

right_four_copy_9: * => *, R, right_four_copy_A;
right_four_copy_A: * => *, R, walker_switch;
